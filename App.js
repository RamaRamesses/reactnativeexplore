import { StatusBar } from 'expo-status-bar';
import React, {useState} from 'react';
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font'
import { useFonts } from '@use-expo/font';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity } from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import SignIn from './pages/SignIn';
import OnBoardingLogin from './pages/OnBoardingLogin';
import SignUp from './pages/SignUp';
import Home from './pages/Home';
import SplashScreen from './pages/SplashScreen';
import Profile from './pages/Profile';
import { Provider } from 'react-redux';
import { store } from './store';
import EditProfile from './pages/EditProfile';
import PilihTesLaboratorium from './pages/PilihTesLaboratorium';
import PilihJenisProgram from './pages/PilihJenisProgram';
import PilihProgramPAP from './pages/PilihProgramPAP';
import Notifikasi from './pages/Notifikasi';
import Bantuan from './pages/Bantuan';
import TestLabStatusPilihLaboratorium from './pages/TestLabStatusPilihLaboratorium';
import TestLabKodeRujukan from './pages/TestLabKodeRujukan';
import TestLabPermintaanDiterima from './pages/TestLabPermintaanDiterima';
import TestLabDataRujukan from './pages/TestLabDataRujukan';
import TestLabUnggahHasilTes from './pages/TestLabUnggahHasilTes';
import TestLabSelesaiMenuBar from './pages/TestLabSelesaiMenuBar';
import TestLabStatusUnggahHasilTes from './pages/TestLabStatusUnggahHasilTes';
import { Canvas } from './pages/Canvas';
import TestLabStatusSelesai from './pages/TestLabStatusSelesai';
import PapStatusSetujuiDokumen from './pages/PapStatusSetujuiDokumen';
import PapPersetujuanDokumen from './pages/PapPersetujuanDokumen';
import { SignatureDrawer } from './pages/SignatureDrawer';
import { PapKonfirmasiDokter } from './pages/PapKonfirmasiDokter';
import PapUnggahResepDokter from './pages/PapUnggahResepDokter';
import ChangePassword from './pages/ChangePassword';
import IsiArtikel from './pages/IsiArtikel';
import ArticleMenu from './pages/ArticleMenu';
import PindaiQR from './pages/PindaiQR';
import PasienPerpanjang from './pages/PasienPerpanjang';
import PasienPerpanjangSukses from './pages/PasienPerpanjangSukses';
import PasienPerpanjangBerhenti from './pages/PasienPerpanjangBerhenti';
const {width, height} = Dimensions.get("window")

const fetchFonts = () => {
  return Font.loadAsync({
  'avenir-next-demibold': require('./assets/fonts/avenir_next_demibold.ttf'),
  'avenir_heavy': require('./assets/fonts/avenir_heavy.ttf'),
  'avenir_black': require('./assets/fonts/avenir_black.ttf'),
  });
};

const Stack = createNativeStackNavigator();

function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator
        screenOptions={{
          headerShown: false
        }}>
          <Stack.Screen name="SplashScreen" component={SplashScreen} />
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="OnBoardingLogin" component={OnBoardingLogin} />
          <Stack.Screen name="SignIn" component={SignIn} />
          <Stack.Screen name="SignUp" component={SignUp} />
          <Stack.Screen name="Profile" component={Profile} />
          <Stack.Screen name="EditProfile" component={EditProfile} />
          <Stack.Screen name="PilihTesLaboratorium" component={PilihTesLaboratorium} />
          <Stack.Screen name="PilihJenisProgram" component={PilihJenisProgram} />
          <Stack.Screen name="PilihProgramPAP" component={PilihProgramPAP} />
          <Stack.Screen name="Notifikasi" component={Notifikasi} />
          <Stack.Screen name="Bantuan" component={Bantuan} />
          <Stack.Screen name="TestLabStatusPilihLaboratorium" component={TestLabStatusPilihLaboratorium} />
          <Stack.Screen name="TestLabStatusUnggahHasilTes" component={TestLabStatusUnggahHasilTes} />
          <Stack.Screen name="TestLabKodeRujukan" component={TestLabKodeRujukan} />
          <Stack.Screen name="TestLabPermintaanDiterima" component={TestLabPermintaanDiterima} />
          <Stack.Screen name="TestLabDataRujukan" component={TestLabDataRujukan} />
          <Stack.Screen name="TestLabUnggahHasilTes" component={TestLabUnggahHasilTes} />
          <Stack.Screen name="TestLabSelesaiMenuBar" component={TestLabSelesaiMenuBar} />
          <Stack.Screen name="TestLabStatusSelesai" component={TestLabStatusSelesai} />
          <Stack.Screen name="PapStatusSetujuiDokumen" component={PapStatusSetujuiDokumen} />
          <Stack.Screen name="PapPersetujuanDokumen" component={PapPersetujuanDokumen} />
          <Stack.Screen name="SignatureDrawer" component={SignatureDrawer} />
          <Stack.Screen name="PapKonfirmasiDokter" component={PapKonfirmasiDokter} />
          <Stack.Screen name="PapUnggahResepDokter" component={PapUnggahResepDokter} />
          <Stack.Screen name="ChangePassword" component={ChangePassword} />
          <Stack.Screen name="IsiArtikel" component={IsiArtikel} />
          <Stack.Screen name="ArticleMenu" component={ArticleMenu} />
          <Stack.Screen name="PindaiQR" component={PindaiQR} />
          <Stack.Screen name="PasienPerpanjang" component={PasienPerpanjang} />
          <Stack.Screen name="PasienPerpanjangSukses" component={PasienPerpanjangSukses} />
          <Stack.Screen name="PasienPerpanjangBerhenti" component={PasienPerpanjangBerhenti} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  )
}

export default App;
