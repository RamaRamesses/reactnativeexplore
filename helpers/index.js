import AsyncStorage from '@react-native-async-storage/async-storage'
import { getOne } from '../services/api/testLab';
import { getOne as getOneProgram } from '../services/api/program';
import { getById } from '../services/api/patient';
import moment from 'moment';

export const checkProgramCheckPoint = async () => {
    const programId = await AsyncStorage.getItem('@UUID_PROGRAM')
    const token = await AsyncStorage.getItem('@token')
    const res = await getOneProgram(token, programId)
    if (res.data) {
      const body = res.data
      if (body.drugsTakenDate != null) {
        await AsyncStorage.setItem("@enroll_date", body.drugsTakenDate.toString())
      }
      if (body.programType) {
        await AsyncStorage.setItem('@NAMA_PROGRAM_TYPE', body.programType.name)
      }
      if (body.checkPoint != null) {
        if (body.checkPoint < 3) {
          console.log("masuk sini kh")
          return { page: "Home", fragment: "dokumen_lengkap" }
        } else if (body.checkPoint == 3) {
          return { page: "Home", fragment: "unggah_resep_dokter" }
        } else if (body.checkPoint == 4) {
          return { page: "Home", fragment: "tunggu_verifikasi" }
        } else if (body.checkPoint == 5 && body.isApproved) {
          const drugsTaken = body.isDrugsTaken
          if (drugsTaken && drugsTaken != null) {
            const reminderDate = body.reminderDate
            const enrollDate = body.drugsTakenDate
            const today = new Date()
            if (reminderDate != null && today > moment(reminderDate).subtract(6, "days")) {
              return { page: "Home", fragment: "perpanjang_pap" }
            } else {
              return { page: "Home", fragment: "minum_obat" }
            }
          } else {
          return { page: "Home", fragment: "pindai_qr" }
          }
        } else {
          return { page: "Home", fragment: "dokumen_lengkap" }
        }
      } else {
          return { page: "Home", fragment: "dokumen_lengkap" }
      }
    } else {
      console.log(res, "err")
          return { page: "OnBoardingLogin", fragment: "" }
    }
}

export const checkTestLabCheckPoint = async () => {
  const test_lab = await AsyncStorage.getItem('@TES_LAB_ID')
  const programId = await AsyncStorage.getItem('@UUID_PROGRAM')
  console.log(programId, "programid")
  const token = await AsyncStorage.getItem('@token')
  console.log(test_lab, "tes lav")
  if (test_lab != null && test_lab != "") {
    const res = await getOne(token, test_lab)
    if (res.data) {
      const body = res.data
      if (body.testLabEvidences != null && body.testLabEvidences.length > 0) {
        if (programId != null && programId != "" && programId != "no program id") {
          // console.log("cek program")
          const intent = await checkProgramCheckPoint()
          return intent
        } else {
          return { page: "Home", fragment: "dokumen_lengkap" }
        }
      } else {
        return { page: "Home", fragment: "tunjukan_data_rujukan" }
      }
    } else {
      console.log(res, "cek lab")
      return { page: "Home", fragment: "pilih_tes_pasien" }
    }
  } else {
    console.log("masuk tes pasien")
    return { page: "Home", fragment: "pilih_tes_pasien" }
  }
}