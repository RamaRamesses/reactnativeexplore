import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font'
import { useFonts } from '@use-expo/font';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Svg, { Path,
  Rect,
  Circle,
  Mask,
  Defs,
  LinearGradient,
  Stop, } from "react-native-svg"
import { useNavigation } from '@react-navigation/native';
import {LinearGradient as ExpoLinearGradient} from 'expo-linear-gradient';
import { marginTop, style } from 'styled-system';
import BarHeader from '../components/BarHeader';
const {width, height} = Dimensions.get("window")

export default function PilihProgramPAP() {

  const navigation = useNavigation()

  useEffect(() => {
    saveCategory()
  }, [])

  const saveCategory = async () => {
    try {
      await AsyncStorage.setItem("@program_category", "pulih")
    } catch(e) {
      console.log(e)
    }
  }

  const navigateProgram = async (programId, programName) => {
    try {
      await AsyncStorage.setItem("@UUID_PROGRAM_TYPE", programId)
      await AsyncStorage.setItem("@NAMA_PROGRAM_TYPE", programName)
      navigation.navigate("PapStatusSetujuiDokumen", { dokumen: "", teslab: "", doctor: "", receipt: "", verify: "" })
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <>
      <BarHeader header="PILIH PROGRAM PAP" route="PilihJenisProgram" />
      <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
        <View style={styles.layout}>
          <View style={styles.row}>
            <View style={styles.imageWrapper}>
              <Image
                style={styles.boardImage}
                resizeMode="contain"
                source={require('../assets/obat_1.png')}
              />
           </View>
            <View style={styles.labelsLayout}>
              <Text style={styles.cardTitle}>TAGRISSO </Text>
              <Text style={styles.cardDescription}>Tagrisso digunakan untuk pengobatan pada orang dewasa yang menderita kanker paru bukan sel kecil stadium lanjut. </Text>
            </View>
          </View>
          <View style={styles.buttonWrapper}>
              <TouchableOpacity onPress={() => {navigateProgram("3a1845a1-28df-403d-a40d-e8173f511fc9", "TAGRISSO")}}>
              <ExpoLinearGradient style={styles.buttonLogin} colors={['#F0AB00', '#C76A1E']}  start={{ x: -1, y: 0 }}
end={{ x: 1, y: 0 }}>
                <Text style={styles.buttonLoginLabel}>AJUKAN PROGRAM PULIH</Text>
              </ExpoLinearGradient>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.layout}>
          <View style={styles.row}>
            <View style={styles.imageWrapper}>
              <Image
                style={styles.boardImage}
                resizeMode="contain"
                source={require('../assets/obat_1.png')}
              />
            </View>
            <View style={styles.labelsLayout}>
              <Text style={styles.cardTitle}>FASLODEX </Text>
              <Text style={styles.cardDescription}>Faslodex digunakan untuk pengobatan pada orang dewasa yang menderita kanker payudara dengan hormonal reseptor positif</Text>
            </View>
          </View>
          <View style={styles.buttonWrapper}>
              <TouchableOpacity onPress={() => {navigateProgram("f20ef325-c767-444c-927d-71d4c3600f67", "FASLODEX")}}>
              <ExpoLinearGradient style={styles.buttonLogin} colors={['#F0AB00', '#C76A1E']} start={{ x: -1, y: 0 }}
end={{ x: 1, y: 0 }}>
                <Text style={styles.buttonLoginLabel}>AJUKAN PROGRAM PULIH</Text>
              </ExpoLinearGradient>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.layout}>
          <View style={styles.row}>
          <View style={styles.imageWrapper}>
              <Image
                style={styles.boardImage}
                resizeMode="contain"
                source={require('../assets/obat_1.png')}
              />
            </View>
            <View style={styles.labelsLayout}>
              <Text style={styles.cardTitle}>LYNPARZA </Text>
              <Text style={styles.cardDescription}>Lynparza digunakan untuk pengobatan kanker ovarium dan kanker payudara dengan jenis tertentu</Text>
            </View>
          </View>
          <View style={styles.buttonWrapper}>
              <TouchableOpacity onPress={() => {navigateProgram("579d64ca-8ba8-458b-9534-47207e86a6a6", "LYNPARZA")}}>
              <ExpoLinearGradient style={styles.buttonLogin} start={{ x: -1, y: 0 }}
end={{ x: 1, y: 0 }} colors={['#F0AB00', '#C76A1E']}>
                <Text style={styles.buttonLoginLabel}>AJUKAN PROGRAM PULIH</Text>
              </ExpoLinearGradient>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f1f5f7'
  },
  layout: {
    marginHorizontal: 20,
    flexDirection: 'column',
    marginTop: 10,
    backgroundColor: 'white',
    marginBottom: 20,
    borderRadius: 15,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
    padding: 10,
  },
  buttonWrapper: {
    width: "100%",
  },
  buttonLogin: {
    height: 50,
    fontSize: 14,
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
  },
  row: {
    alignSelf: 'center',
    marginVertical: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    // background color must be set
  },
  labelsLayout: {
    flex: 3,
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  cardTitle: {
    fontFamily: 'avenir_bold',
    fontSize: 20,
    color: "black",
    flex: 1,
  },
  cardDescription: {
    fontFamily: 'avenir_next_demibold',
    color: "gray",
    lineHeight: 20,
    marginTop: 7,
    marginRight: 25,
    fontSize: 12,
    flex: 2,
  },
  imageWrapper: {
    marginVertical: 0,
    marginLeft: 11,
    flex: 2
  },
  boardImage: {
    width: 130,
    height: 110,
  },
});
