import React, {useState} from 'react';
import AppLoading from 'expo-app-loading';
import { useFonts } from '@use-expo/font';
import { StyleSheet, SafeAreaView, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity } from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';
import { Input, Icon } from 'react-native-elements';
const {width, height} = Dimensions.get("window")
import { Appbar } from 'react-native-paper';
import { login } from '../services/api/auth';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';

export default function SignIn() {
  const navigation = useNavigation();

  const [isLoaded] = useFonts({
    'avenir_next_bold': require('../assets/fonts/avenir_next_bold.ttf'),
    'avenir_next_medium': require('../assets/fonts/avenir_next_medium.ttf'),
  });

  const [password, setPassword] = useState(true);
  const [buttonStyle, setButtonStyle] = useState(['#F0AB00', '#C76A1E']);
  const [buttonDisabled, setButtonDisabled] = useState(false);
  const [passwordErrorMsg, setPasswordErrorMsg] = useState("");
  const [emailErrorMsg, setEmailErrorMsg] = useState("");
  const [user, setUser] = useState({
    email: '',
    password: '',
    role: 'patient',
  })

  const onChangeEmail = (val) => {
    setUser({...user, email: val})
  }
  const onChangePassword = (val) => {
    setUser({...user, password: val})
  }

  const saveActiveTestLab = async (patient) => {
    console.log(patient, "patient")
    for (const i in patient.testLabs) {
      if (patient.testLabs[i].status == 1) {
        try {
          await AsyncStorage.setItem("@TES_LAB_ID", patient.testLabs[i].id.toString())
          if (patient.testLabs[i].doctor) await AsyncStorage.setItem("@UUID_DOKTER", patient.testLabs[i].doctor.id)
          if (patient.testLabs[i].laboratorium) await AsyncStorage.setItem("@UUID_LABORATORIUM",patient.testLabs[i].laboratorium.id)
          if (patient.testLabs[i].testLabType) await AsyncStorage.setItem("@UUID_TES_LAB_TYPE", patient.testLabs[i].testLabType.id)
          if (patient.testLabs[i].testLabType) await AsyncStorage.setItem("@TES_LAB_NAME", patient.testLabs[i].testLabType.name)
        } catch (err) {
          console.log(err)
        }
      }
    }

    for (const i in patient.programs) {
      const program = patient.programs[i]
      if (patient.programs[i].status == 1) {
        await AsyncStorage.setItem("@UUID_PROGRAM", program.id)
        if (program.doctor) await AsyncStorage.setItem("@PROGRAM_UUID_DOCTOR", program.doctor.id)
        if (program.doctor) await AsyncStorage.setItem("@PROGRAM_NAMA_DOKTER", program.doctor.fullname)
        if (program.programType) await AsyncStorage.setItem("@NAMA_PROGRAM_TYPE", program.programType.name)
        if (program.programType) await AsyncStorage.setItem("@UUID_PROGRAM_TYPE", program.programType.id)
      }
    }
  }

  const saveToken = async (token, id, code, name) => {
    try {
      await AsyncStorage.setItem("@token", token)
      await AsyncStorage.setItem("@UUID_PATIENT", id)
      await AsyncStorage.setItem("@CODE_PATIENT", code)
      await AsyncStorage.setItem("@NAME_PATIENT", name)
    } catch (e) {
      console.log(e, "err")
    }
  }

  const submitData = async () => {
    // setButtonStyle(['gray', 'gray'])
    setButtonDisabled(true)
    const res = await login(user)
    // setButtonStyle('#F0AB00', '#C76A1E')
    setButtonDisabled(false)
    if (res.data) {
      try {
        const jsonValue = JSON.stringify(res.data)
        await AsyncStorage.setItem('@patient', jsonValue)
        saveToken(res.headers.authorization, res.data.id, res.data.code, res.data.fullname)
        saveActiveTestLab(res.data)
        navigation.navigate('SplashScreen')
      } catch (e) {
        console.log(e)
      }
    } else {
      console.log(res, "err")
      if (res === 'invalid email') {
        setEmailErrorMsg(res)
        setPasswordErrorMsg('')
      } else {
        setPasswordErrorMsg(res)
        setEmailErrorMsg("")
      }
    }
  }

  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return (
      <SafeAreaView>
        <ScrollView>
          <Appbar.BackAction style={styles.toolbar} onPress={() => navigation.navigate('OnBoardingLogin')} />
          <View style={styles.contentWrapper}>
            <Text
            style={styles.text}>
              Selamat Datang {"\n"} Kembali
            </Text>
            <View style={styles.inputEmail}>
            <Input
              onChangeText={(val) => onChangeEmail(val)}
              placeholder='Email'
              errorStyle={{ color: 'red' }}
              errorMessage={emailErrorMsg}
              style={{paddingLeft: 8}}
            />
            </View>
            <View style={styles.inputPassword}>
              <Input
              onChangeText={(val) => onChangePassword(val)}
              placeholder="Kata sandi"
              errorStyle={{ color: 'red' }}
              errorMessage={passwordErrorMsg}
              style={{paddingLeft: 8}}
              secureTextEntry={password}
              rightIcon={
                <Icon
                onPress={() => { 
                  setPassword(!password)
                 }}
                  name={password ? 'eye' : 'eye-off'}
                  size={24}
                  type='ionicon'
                  color='black'
                />
              } />
            </View>
            <Text style={styles.forgotPasswordLabel}>Lupa kata sandi?</Text>
            <View style={styles.registerLabels}>
              <Text style={styles.registerText}>Pengguna Baru?</Text>
              <Text style={styles.registerLink}>Daftar Disini</Text>
            </View>
            <View style={styles.buttonWrapper}>
              <TouchableOpacity disabled={buttonDisabled} onPress={() => submitData()}>
              <LinearGradient style={styles.buttonLogin} colors={buttonStyle}>
                <Text style={styles.buttonLoginLabel}>MASUK</Text>
              </LinearGradient>
            </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    width: width,
    justifyContent: 'center',
  },
  toolbar: {
    marginLeft: 10,
  },
  buttonLogin: {
    padding: 20,
    width: width - 44,
    fontSize: 16,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 18,
    color: 'white',
  },
  inputEmail: {
    height: 40,
    width: width - 44,
    margin: 12,
    fontSize: 28,
    padding: 10,
  },
  inputPassword: {
    height: 40,
    width: width - 44,
    marginBottom: 50,
    marginTop: 70,
    fontSize: 28,
    padding: 10,
  },
  forgotPasswordLabel: {
    textAlign: 'right',
    width: width - 44,
    marginRight: 20,
    marginBottom: 120,
    color: '#800152'
  },
  registerLabels: {
    flexDirection: 'row',
    marginBottom: 15,
  },
  registerText: {
    textAlign: 'center',
    fontFamily: 'avenir_next_medium',
    color: 'gray'
  },
  registerLink: {
    textAlign: 'center',
    fontFamily: 'avenir_next_medium',
    color: '#800152',
    marginLeft: 5,
  },
  text: {
    fontSize: 46,
    color: '#800152',
    textAlign: 'center',
    fontFamily: 'avenir_next_bold',
    bottom: 30,
  },
  contentWrapper: {
    paddingTop: 80,
    alignItems: 'center',
  },
  buttonWrapper: {
    width: width,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  }
});
