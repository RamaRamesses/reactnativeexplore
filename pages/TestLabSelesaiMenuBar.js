import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity, SafeAreaView } from 'react-native';
import Svg, { SvgProps, Path } from "react-native-svg"
import BarHeader from '../components/BarHeader';
import { useFonts } from '@use-expo/font';
import RNPickerSelect from 'react-native-picker-select';
import { Input, NativeBaseProvider } from 'native-base';
import { Icon } from 'react-native-elements';
import {LinearGradient} from 'expo-linear-gradient';
import AppLoading from 'expo-app-loading';
import { useNavigation } from '@react-navigation/native';
import BarFooter from '../components/BarFooter';
import { checkTestLabCheckPoint } from '../helpers';
const {width, height} = Dimensions.get("window")

export default function TestLabSelesaiMenuBar() {

  const onChangeSelect = (val) => {
    console.log(val)
  }

  const navigation = useNavigation()
  const onBackPressed = async () => {
    const route = await checkTestLabCheckPoint()
    navigation.navigate(route.page, { fragment: route.fragment })
  }

  const nextButton = async () => {
    navigation.navigate("PilihJenisProgram")
  }
  const [isLoaded] = useFonts({
    'avenir_next_bold': require('../assets/fonts/avenir_next_bold.ttf'),
    'avenir_next_medium': require('../assets/fonts/avenir_next_medium.ttf'),
  });
  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return (
        <NativeBaseProvider style={styles.container}>
          <ScrollView>
            <View>
              <View style={styles.contentWrapper}>
                <Image
                  style={styles.boardImage}
                  resizeMode="contain"
                  source={require('../assets/dokumen_lengkap.png')}
                />
                <Text style={styles.sectionTitle}> Selamat, Dokumen {"\n"} telah lengkap</Text>
                <Text style={styles.sectionDescription}> Seluruh Dokumen tes Anda telah diunggah, apakah Anda ingin mengajukan program PAP sekarang.  </Text>
              </View>
            </View>
          </ScrollView>
          <View style={styles.buttonWrapper}>
              <TouchableOpacity onPress={()=> nextButton()}>
              <LinearGradient style={styles.button} colors={['#F0AB00', '#C76A1E']}>
                <Text style={styles.buttonYes}>IYA</Text>
              </LinearGradient>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => onBackPressed()}>
              <LinearGradient style={styles.button} colors={['#dfdfdf', '#dfdfdf']}>
                <Text style={styles.buttonNo}>TIDAK</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </NativeBaseProvider>
    );
  }
}

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 18,
    paddingVertical: 12,
    borderWidth: 1,
    borderRadius: 13,
    borderColor: 'gray',
    width: 140,
    marginBottom: 20,
    marginHorizontal: 5,
    paddingHorizontal: 10,
    color: 'black',
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});

const laboratoriumPickerStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 18,
    paddingVertical: 12,
    borderWidth: 1,
    borderRadius: 13,
    borderColor: 'gray',
    width: "84%",
    marginHorizontal: 15,
    alignSelf: 'center',
    paddingHorizontal: 10,
    height: 40,
    color: 'black',
  },
})

const styles = StyleSheet.create({
  contentWrapper: {
    alignSelf: 'center',
    flexDirection: 'column',
    width: width -30,
    marginTop: 22,
    alignSelf: 'center',
    marginTop: 55,
    paddingVertical: 21,
    paddingHorizontal: 17,
    // background color must be set
  },
  boardImage: {
    width: 360,
    height: 236
  },
  arrowImage: {
    flex: 1,
    width: 40,
    height: 38,
  },
  input: {
    alignSelf: 'center',
    borderRadius: 12,
    borderColor: "#979797",
    marginBottom: 80,
  },
  row: {
    flexDirection: "row",
    justifyContent: 'center'
  },
  container: {
    width,
    height,
    backgroundColor: "#ffffff",
  },
  buttonWrapper: {
    width: width - 20,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-around',
    bottom: 20,
  },
  button: {
    height: 65,
    width: 182,
    fontSize: 14,
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonYes: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 18,
    color: 'white',
  },
  buttonNo: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 18,
    color: 'gray',
  },
  sectionTitle: {
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_bold",
    fontSize: 38,
    lineHeight: 50,
    letterSpacing: 0.13,
    marginTop: 21,
  },
  sectionDescription: {
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_next_medium",
    fontSize: 18,
    marginTop: 38,
    fontWeight: "500",
    marginBottom: 25,
    lineHeight: 30,
  },
  bottomText: {
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_bold",
    fontSize: 18,
    fontWeight: "500",
    lineHeight: 25,
    color: "#830051",
  },
});
