import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity, SafeAreaView, Alert } from 'react-native';
import Svg, { SvgProps, Path } from "react-native-svg"
import BarHeader from '../components/BarHeader';
import { useFonts } from '@use-expo/font';
import { Input, NativeBaseProvider } from 'native-base';
import { Icon } from 'react-native-elements';
import {LinearGradient} from 'expo-linear-gradient';
import AppLoading from 'expo-app-loading';
import { useIsFocused, useNavigation, useRoute } from '@react-navigation/native';
import { baseUrl } from '../config';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { getAll , getAllTags, getByTag } from '../services/api/article';
import moment from 'moment';
import { backgroundColor } from 'styled-system';
const {width, height} = Dimensions.get("window")

export default function ArticleMenu() {
  
  const navigation = useNavigation()
  const route = useRoute()
  const isFocused = useIsFocused()

  const [isLoaded] = useFonts({
    'avenir_next_bold': require('../assets/fonts/avenir_next_bold.ttf'),
    'avenir_next_medium': require('../assets/fonts/avenir_next_medium.ttf'),
  });

  
  const [imageSize, setImageSize] = useState({
    width: 0,
    height: 0,
  })
  const [articleBody, setArticleBody] = useState("")
  const [articles, setArticles] = useState([])
  const [tags, setTags] = useState([])
  const [uniqueIndex, setUniqueIndex] = useState(0)
  const [selectedTag, setSelectedTag] = useState(-1)
  
  useEffect(() => {
    getImage()
    loadTags()
    loadArticle()
    setSelectedTag(-1)
  }, [isFocused])

  const getImage = async () => {
    Image.getSize(baseUrl + "/uploads/news/NEWS-1631868675191.jpg", (width, height) => {
      setImageSize({
        width,
        height
      })
    })
  }

  const getAllByTags = async (tag, tagIndex) => {
    try {
      const token = await AsyncStorage.getItem('@token')
      const res = await getByTag(token, tag)
      if (res.data) {
        if (tagIndex !== selectedTag) {
          setArticles(res.data)
          setSelectedTag(tagIndex)
          let newTags = []
          for (const i in tags) {
            tags[i].active = false
            newTags.push(tags[i])
          }
          newTags[tagIndex].active = true
          console.log(newTags, "tags")
          setTags(newTags)
          setUniqueIndex(uniqueIndex + 1)
        } else {
          setSelectedTag(-1)
          let newTags = []
          for (const i in tags) {
            tags[i].active = false
            newTags.push(tags[i])
          }
          setTags(newTags)
          loadArticle()
        }
      } else {
        Alert.alert(e)
      }
    } catch (e) {
      console.log(e)
    }
  }

  const loadTags = async () => {
    try {
      const res = await getAllTags()
      if (res.data) {
        for (const i in res.data) {
          res.data[i].active = false
        }
        setTags(res.data)
      } else {
        Alert.alert(e)
      }
    } catch (e) {
      console.log(e)
    }
  }

  const loadArticle = async () => {
    try {
      const token = await AsyncStorage.getItem('@token')
      const res = await getAll(token)
      setUniqueIndex(uniqueIndex + 1)
      if (res.data) {
        setArticles(res.data)
      } else {
        console.log(res)
        Alert.alert(res)
      }
    } catch (e) {
      console.log(e)
    }
  }

  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return (
        <NativeBaseProvider style={styles.container}>
          <BarHeader header={"ARTIKEL"} />
          <ScrollView>
            <View style={styles.tagsWrapper}>
              <ScrollView key={uniqueIndex} showsHorizontalScrollIndicator={false} horizontal={true}>
                {
                  tags.map((tag, i) => (
                    <TouchableOpacity key={i} onPress={() => getAllByTags(tag.tag, i)}>
                      <View style={tag.active ? styles.tagSelected : styles.tag}>
                          <Text style={tag.active ? styles.tagLabelSelected : styles.tagLabel}>{tag.tag}</Text>
                      </View>
                    </TouchableOpacity>
                  ))
                }
                {/* <LinearGradient style={styles.tag} colors={['#830051', '#1d2671']} start={{ x: 1, y: 0 }}>
                  <Text style={styles.tagLabel}>Kanker</Text>
                </LinearGradient> */}
              </ScrollView>
            </View>
            <View style={styles.listWrapper}>
              {
                articles.map((article, i) => (
                  <TouchableOpacity onPress={() => navigation.navigate("IsiArtikel", { articleId: article.id })} key={i} style={styles.card}>
                    <View style={styles.imageWrapper}>
                    <Image
                      style={{
                        height: "100%",
                      }}
                      resizeMode="stretch"
                      source={{
                        uri: baseUrl + article.thumbnailImage
                      }}
                    />
                    </View>
                    <View style={styles.textWrapper}>
                      <Text style={styles.title}  numberOfLines={2}>{article.title}</Text>
                      <Text style={styles.description} numberOfLines={3} >{article.body}</Text>
                    </View>
                  </TouchableOpacity>
                ))
              }
              {/* <View style={styles.card}>
                <View style={styles.imageWrapper}>
                </View>
                <View style={styles.textWrapper}>
                  <Text style={styles.title}  numberOfLines={2}>Antibiotik untuk Batuk, Temukan</Text>
                  <Text style={styles.description} numberOfLines={3} >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel quam et enim tempus semper. Maecenas lobortis risus at mauris malesuada, vitae fermentum ipsum fermentum. Sed mattis mi dignissim feugiat dictum.</Text>
                </View>
              </View> */}
            </View>
          </ScrollView>
        </NativeBaseProvider>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    flex: 1,
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    height: 140,
    width: width - 20,
    backgroundColor: "#FFF",
    marginVertical: 10,
    borderRadius: 6,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
    // background color must be set
  },
  imageWrapper: {
    borderRadius: 6,
    flex: 1,
    backgroundColor: "#390752"
  },
  textWrapper: {
    flex: 2,
    height: "100%",
    flexDirection: 'column',
    alignSelf: 'center',
    justifyContent: 'center'
  },
  listWrapper: {
    width,
  },
  container: {
    width,
    height,
    backgroundColor: "#ffffff",
  },
  tag: {
    borderRadius: 10,
    marginTop: 10,
    marginLeft: 10,
    backgroundColor: "#830051"
  },
  tagSelected: {
    borderColor: "#830051",
    borderWidth: 1,
    backgroundColor: "#FFF",
    marginTop: 10,
    marginLeft: 10,
    borderRadius: 10,
  },
  tagsWrapper: {
    marginBottom: 10,
    marginTop: 10,
  },
  tagLabel: {
    paddingVertical: 5,
    paddingHorizontal: 25,
    fontFamily: 'avenir_bold',
    color: "#FFFFFF"
  },
  tagLabelSelected: {
    paddingVertical: 5,
    paddingHorizontal: 25,
    fontFamily: 'avenir_bold',
    color: "#830051"
  },
  contentWrapper: {
    marginHorizontal: 24,
    marginVertical: 30,
  },
  boardImage: {
    height: 436
  },
  row: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  date: {
    fontFamily: "avenir_next_demibold",
    fontSize: 10,
    color: "#3e3e3e"
  },
  body: {
    flex: 1,
    fontSize: 12,
    color: "#000000",
    fontFamily: 'avenir_next_medium',
    textAlign: 'justify',
    marginTop: 25,
  },
  author: {
    fontSize: 10,
    color: "#800152",
    fontFamily: "avenir_next_demibold",
  },
  title: {
    fontFamily: "avenir_bold",
    fontSize: 18,
    paddingTop: 15,
    flex: 1,
    marginHorizontal: 15,
    color: "#000",
  },
  description: {
    fontFamily: "avenir_next_medium",
    fontSize: 14,
    flex: 1.5,
    marginHorizontal: 15,
    alignSelf: 'center',
    color: "#000",
  },
});
