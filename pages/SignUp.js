import React, {useState, useEffect, useRef} from 'react';
import AppLoading from 'expo-app-loading';
import { useFonts } from '@use-expo/font';
import { StyleSheet, SafeAreaView, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity, CheckBox, PickerIOSBase, Alert } from 'react-native';
import { Checkbox, Center, NativeBaseProvider } from "native-base"
import {LinearGradient} from 'expo-linear-gradient';
import { Input, Icon, Overlay } from 'react-native-elements';
const {width, height} = Dimensions.get("window")
import { Appbar } from 'react-native-paper';
import { getAll as getAllCity } from '../services/api/city'
import { register } from '../services/api/auth'
import { useNavigation } from '@react-navigation/native';
import RNPickerSelect from 'react-native-picker-select';
import Toast, {DURATION} from 'react-native-easy-toast'

export default function SignUp() {
  const navigation = useNavigation();
  const ref = useRef();
  const [isLoaded] = useFonts({
    'avenir_next_bold': require('../assets/fonts/avenir_next_bold.ttf'),
    'avenir_next_medium': require('../assets/fonts/avenir_next_medium.ttf'),
  });


  const [password, setPassword] = useState(true);
  const [passwordErrorMsg, setPasswordErrorMsg] = useState("");
  const [emailErrorMsg, setEmailErrorMsg] = useState("");
  const [cities, setCities] = useState([]);
  const [cityValue, setCityValue] = useState("");
  const [buttonDisabled, setButtonDisabled] = useState(false);
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    getCityData();
  }, []);
  const toggleOverlay = () => {
    setVisible(!visible);
  };
  const [user, setUser] = useState({
    status: 1,
    email: '',
    password: '',
    city: '',
  })

  const onChangeEmail = (val) => {
    setUser({...user, email: val})
  }
  const onChangePassword = (val) => {
    setUser({...user, password: val})
  }
  const onChangeCity = (val) => {
    setUser({...user, city: val})
  }

  const getCityData = async () => {
    const res = await getAllCity()
    let items = []
    for (const i in res.data) {
      items.push({ label: res.data[i].name, value: res.data[i].name })
    }
    setCities(items)
  }


  const submitData = async () => {
    setButtonDisabled(true)
    const res = await register(user)
    console.log(res.data)
    if (!res.data) {
      if (res === "email telah terdaftar") {
        setPasswordErrorMsg("")
        setEmailErrorMsg(res)
      } else {
        setEmailErrorMsg("")
        setPasswordErrorMsg(res)
      }
      console.log(res, "login response")
    } else {
      ref.current.show(res.data.data);
      setTimeout(function(){ navigation.navigate("SignIn") }, 2000);
      console.log(res.data, "login response")
    }
    setButtonDisabled(false)
  }

  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return (
      <NativeBaseProvider>
      <SafeAreaView>
        <ScrollView>
          <Appbar.BackAction style={styles.toolbar} onPress={() => navigation.navigate('OnBoardingLogin')} />
          <View style={styles.contentWrapper}>
            <Text
            style={styles.text}>
              DAFTAR
            </Text>
            <View style={styles.inputEmail}>
            <Input
              onChangeText={(val) => onChangeEmail(val)}
              placeholder='Email'
              errorStyle={{ color: 'red' }}
              errorMessage={emailErrorMsg}
              style={{paddingLeft: 8}}
            />
            </View>
            <View style={styles.inputPassword}>
              <Input
              onChangeText={(val) => onChangePassword(val)}
              placeholder="Kata sandi"
              errorStyle={{ color: 'red' }}
              errorMessage={passwordErrorMsg}
              style={{paddingLeft: 8}}
              secureTextEntry={password}
              rightIcon={
                <Icon
                onPress={() => { 
                  setPassword(!password)
                 }}
                  name={password ? 'eye' : 'eye-off'}
                  size={24}
                  type='ionicon'
                  color='black'
                />
              } />
            </View>
            {/* <Overlay style={styles.overlay} isVisible={visible}>
              <ScrollView style={{maxHeight: height - 100}}>
              {
                cities.map((city, i) => (
                  <Checkbox.Group value={cityValue} key={i} accessibilityLabel="choose numbers">
                    <Checkbox onChange={(val) => { 
                      setCityValue(city.name)
                      setVisible(false)
                    }} value="true" accessibilityLabel="This is a dummy checkbox" style={{ marginVertical: 5 }} > {city.name} </Checkbox>
                  </Checkbox.Group>
                ))}
              </ScrollView>
            </Overlay> */}
            <RNPickerSelect
              placeholder={{
                label: "Pilih Kota",
              }}
              useNativeAndroidPickerStyle={false}
                onValueChange={(value) => onChangeCity(value)}
                style={pickerSelectStyles}
                items={cities}
            />
            <View style={styles.buttonWrapper}>
              <TouchableOpacity disabled={buttonDisabled} onPress={() => submitData()}>
              <LinearGradient style={styles.buttonLogin} colors={['#F0AB00', '#C76A1E']}>
                <Text style={styles.buttonLoginLabel}>MASUK</Text>
              </LinearGradient>
            </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
        <Toast ref={ref}/>
      </SafeAreaView>
      </NativeBaseProvider>
    );
  }
}

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 18,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderBottomWidth: 1,
    borderColor: 'gray',
    marginHorizontal: 38,
    marginBottom: 30,
    marginTop: 30,
    color: 'black',
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    width: width,
    height: height,
    justifyContent: 'center',
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 20,
  },
  checkbox: {
    alignSelf: "center",
  },
  overlay: {
  },
  toolbar: {
    marginLeft: 10,
  },
  buttonLogin: {
    padding: 20,
    width: width - 44,
    fontSize: 16,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 18,
    color: 'white',
  },
  inputEmail: {
    height: 40,
    width: width - 44,
    margin: 12,
    fontSize: 28,
    padding: 10,
  },
  inputKota: {
    width: width - 80,
    margin: 12,
    borderBottomWidth: 1,
    marginTop: 40,
    fontSize: 28,
    borderColor: 'gray',
    padding: 10,
  },
  inputPassword: {
    height: 40,
    width: width - 44,
    marginBottom: 50,
    marginTop: 50,
    fontSize: 28,
    padding: 10,
  },
  forgotPasswordLabel: {
    textAlign: 'right',
    width: width - 44,
    marginRight: 20,
    color: '#800152'
  },
  registerLabels: {
    flexDirection: 'row',
    marginBottom: 15,
  },
  registerText: {
    textAlign: 'center',
    fontFamily: 'avenir_next_medium',
    color: 'gray'
  },
  registerLink: {
    textAlign: 'center',
    fontFamily: 'avenir_next_medium',
    color: '#800152',
    marginLeft: 5,
  },
  text: {
    fontSize: 46,
    color: '#800152',
    textAlign: 'center',
    fontFamily: 'avenir_next_bold',
    bottom: 30,
  },
  contentWrapper: {
    paddingTop: 80,
    alignItems: 'center',
    height: height-80,
  },
  buttonWrapper: {
    width: width,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
  }
});
