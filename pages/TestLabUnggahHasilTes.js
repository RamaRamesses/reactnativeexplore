import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity } from 'react-native';
import Svg, { SvgProps, Path, G } from "react-native-svg"
import BarHeader from '../components/BarHeader';
import { useFonts } from '@use-expo/font';
import RNPickerSelect from 'react-native-picker-select';
import { Box, Input, NativeBaseProvider, Progress, Spinner } from 'native-base';
import { Icon, Overlay } from 'react-native-elements';
import {LinearGradient} from 'expo-linear-gradient';
import AppLoading from 'expo-app-loading';
import { useNavigation } from '@react-navigation/native';
import { alignSelf, background, backgroundColor } from 'styled-system';
import * as ImagePicker from 'expo-image-picker';
import { upload, uploadTestLabEvidence } from '../services/api/fileUpload';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { create } from '../services/api/testLabEvidence';
const {width, height} = Dimensions.get("window")


export default function TestLabUnggahHasilTes() {
  const [progress, setProgress] = useState(0)
  const [fileName, setFileName] = useState("")
  const [imageList, setImageList] = useState([])
  const [isImageLoading, setIsImageLoading] = useState(false);
  const [pickedImagePath, setPickedImagePath] = useState('');
  const [uniqueIndex, setUniqueIndex] = useState(1)
  const [savedImageList, setSavedImageList] = useState([])
  const [visibleOverlay, setVisibleOverlay] = useState(false)

  const onChangeSelect = (val) => {
    console.log(val)
  }

  const submitData = async () => {
    setVisibleOverlay(true)
    try {
      const testLabId = await AsyncStorage.getItem('@TES_LAB_ID')
      const res = await create({
        urls: savedImageList,
        testLabId
      })
      if (res.data) {
        setVisibleOverlay(false)
        console.log(res.data)
        const isDocumentReady = await AsyncStorage.getItem('@document_ready')
        const consent = await AsyncStorage.getItem('@consent_url')
        if (isDocumentReady == "true") {
          // TODO
        }  else {
          navigation.navigate("TestLabSelesaiMenuBar")
        }
      } else {
        console.log(res)
        setVisibleOverlay(false)
      }
    } catch (e) {
      console.log(e)
      setVisibleOverlay(false)
    }
  }

  const removeImage = (index) => {
    const images = imageList
    const savedImages = savedImageList
    console.log(index)
    setUniqueIndex(uniqueIndex + 1)
    for(var i = 0; i < images.length; i++){ 
      console.log(index, i)
      if (i == index) {
        images.splice(i, 1);
        savedImages.splice(i, 1)
      }
    }
    setImageList(images)
    setSavedImageList(savedImages)
    console.log(imageList, "imagelist")
    console.log(savedImages, "imagelist")
  }

  const openCamera = async () => {
    // Ask the user for the permission to access the camera
    setIsImageLoading(true)
    const permissionResult = await ImagePicker.requestCameraPermissionsAsync();
 
    if (permissionResult.granted === false) {
      alert("You've refused to allow this appp to access your camera!");
      return;
    }
 
    const result = await ImagePicker.launchCameraAsync();

    let filename = result.uri.split('/').pop();
      setFileName(filename)
      const images = imageList
      const newImage = {
        filename,
        status: 'Sedang diunggah...'
      }
      images.push(newImage)
      setImageList(images)
 
    // Explore the result
    console.log(result);
 
    if (!result.cancelled) {
     setPickedImagePath(result.uri);
     let filename = result.uri.split('/').pop();
 
     let match = /\.(\w+)$/.exec(filename);
     let type = match ? `image/${match[1]}` : `image`;
 
      // Upload the image using the fetch and FormData APIs
      let formData = new FormData();
      // Assume "photo" is the name of the form field the server expects
      formData.append('upload', { uri: result.uri, name: filename, type });
      console.log(result.uri);
      
      const res = await uploadTestLabEvidence(formData, (event) => {
        setProgress(Math.round((100 * event.loaded) / event.total))
      });
      console.log(res.data, "racse")
      console.log(filename, "racse")
      images[images.length - 1].status = "Berhasil diunggah."
      setImageList(images)
      const imageLinks = savedImageList
      imageLinks.push(res.data.data)
      setSavedImageList(imageLinks)
      // if (selectedImage === "idPicture") setUser({...user, idPicture: res.data.data})
      // else setUser({...user, selfiePicture: res.data.data})
      setIsImageLoading(false)
      setProgress(0)
    }
  }

  const showImagePicker = async () => {
    setIsImageLoading(true)
    // Ask the user for the permission to access the media library 
    const permissionResult = await ImagePicker.requestMediaLibraryPermissionsAsync();
    
    if (permissionResult.granted === false) {
      alert("You've refused to allow this appp to access your photos!");
      return;
    }
    
    const result = await ImagePicker.launchImageLibraryAsync();

    let filename = result.uri.split('/').pop();
      setFileName(filename)
      const images = imageList
      const newImage = {
        filename,
        status: 'Sedang diunggah...'
      }
      images.push(newImage)
      setImageList(images)
    
    // Explore the result
    console.log(result);
    
    if (!result.cancelled) {
      setPickedImagePath(result.uri);
      let filename = result.uri.split('/').pop();
      // setFileName(filename)
      // const images = imageList
      // const newImage = {
      //   filename,
      //   status: 'Sedang diunggah...'
      // }
      // images.push(newImage)
      // setImageList(images)
  
      let match = /\.(\w+)$/.exec(filename);
      let type = match ? `image/${match[1]}` : `image`;
  
      // Upload the image using the fetch and FormData APIs
      let formData = new FormData();
      // Assume "photo" is the name of the form field the server expects
      formData.append('upload', { uri: result.uri, name: filename, type });
      console.log(result.uri);
      
      const res = await uploadTestLabEvidence(formData, (event) => {
        setProgress(Math.round((100 * event.loaded) / event.total))
      });
      console.log(res.data, "racse")
      console.log(filename, "racse")
      images[images.length - 1].status = "Berhasil diunggah."
      setImageList(images)
      const imageLinks = savedImageList
      imageLinks.push(res.data.data)
      setSavedImageList(imageLinks)
      // if (selectedImage === "idPicture") setUser({...user, idPicture: res.data.data})
      // else setUser({...user, selfiePicture: res.data.data})
      setIsImageLoading(false)
      setProgress(0)
     }
  }

  const navigation = useNavigation()
  const [isLoaded] = useFonts({
    'avenir_next_bold': require('../assets/fonts/avenir_next_bold.ttf'),
    'avenir_next_medium': require('../assets/fonts/avenir_next_medium.ttf'),
  });
  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return (
      <NativeBaseProvider>
        <View style={styles.container}>
          <BarHeader header="UNGGAH HASAIL TES" />
          <View style={styles.header}>
            <View style={styles.dashedLayout}>
              <Svg onPress={() => openCamera()} style={styles.camera} xmlns="http://www.w3.org/2000/svg" width={61} height={52}>
                <G
                  stroke="#454545"
                  fill="none"
                  fillRule="evenodd"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                >
                  <Path d="M60 11.572C60 8.497 57.57 6 54.579 6H6.42C3.43 6 1 8.497 1 11.572v33.856C1 48.503 3.43 51 6.421 51H54.58c2.99 0 5.42-2.497 5.42-5.572V11.572zM22.459 1l-3.918 5" />
                  <Path d="M22 1h15.481L41 6M30.5 18C37.399 18 43 23.601 43 30.5S37.399 43 30.5 43 18 37.399 18 30.5 23.601 18 30.5 18zM50 15a3.001 3.001 0 010 6 3.001 3.001 0 010-6z" />
                </G>
              </Svg>
              <Text style={styles.dashedLayoutLabel}> Atau </Text>
              <Svg onPress={() => showImagePicker()} style={styles.camera} xmlns="http://www.w3.org/2000/svg" width={61} height={52} >
                <G fill="none" fillRule="evenodd">
                  <Path
                    stroke="#454545"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M53 19.5C53 13.705 48.338 9 42.596 9H11.404C5.662 9 1 13.705 1 19.5v21C1 46.295 5.662 51 11.404 51h31.192C48.338 51 53 46.295 53 40.5v-21z"
                  />
                  <Path
                    stroke="#454545"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M54.232 41c5.608 0 4.716-3.004 4.716-8.752v-20.83c0-5.748-4.553-10.415-10.162-10.415H18.321C12.713 1.003 11 .68 11 6.428"
                  />
                  <Path
                    stroke="#454545"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M4 44.768l13.143-12.644L24.573 49 37.36 25 52 40.795"
                  />
                  <Path
                    fill="#454545"
                    d="M14 17c-2.76 0-5 2.24-5 5s2.24 5 5 5 5-2.24 5-5-2.24-5-5-5zm0 1.034A3.968 3.968 0 0117.966 22c0 2.188-1.777 3.966-3.966 3.966S10.034 24.188 10.034 22A3.968 3.968 0 0114 18.034z"
                  />
                </G>
              </Svg>
            </View>
          </View>
          <Overlay isVisible={visibleOverlay}>
                <View style={{margin: 5}}>
                    <Spinner color="cyan.500" />
                </View>
              </Overlay>
          <View key={uniqueIndex} style={styles.contentWrapper}>
            {
              imageList.map((image, i) => (
                <View key={i} style={styles.col}>
                  <Text style={styles.itemsLabel}>{image.filename}</Text>
                  <View style={styles.layoutItemsSize}>
                    <Text style={styles.itemsSize}>{image.status}</Text>
                    <Icon
                        onPress={() => removeImage(i)}
                        name="close-circle"
                        size={16}
                        type='ionicon'
                        color='gray'
                    />
                  </View>
                  <Box w="100%" style={{marginTop: 4}}>
                    <Progress size="xs" colorScheme="emerald" value={image.status === 'Sedang diunggah...' ? progress : 100} />
                  </Box>
                </View>
              ))
            }
            
            {/* <View style={styles.col}>
              <Text style={styles.itemsLabel}>hasil_tes_2.jpg </Text>
              <View style={styles.layoutItemsSize}>
                <Text style={styles.itemsSize}>Berhasil diunggah</Text>
                <Icon
                    name="close-circle"
                    size={16}
                    type='ionicon'
                    color='gray'
                />
              </View>
              <Box w="100%" style={{marginTop: 4}}>
                <Progress size="xs" colorScheme="emerald" value={100} />
              </Box>
            </View>
            <View style={styles.col}>
              <Text style={styles.itemsLabel}>hasil_tes_3.jpg </Text>
              <View style={styles.layoutItemsSize}>
                <Text style={styles.itemsSize}>Berhasil diunggah</Text>
                <Icon
                    name="close-circle"
                    size={16}
                    type='ionicon'
                    color='gray'
                />
              </View>
              <Box w="100%" style={{marginTop: 4}}>
                <Progress size="xs" colorScheme="emerald" value={100} />
              </Box>
            </View> */}
          </View>
          <View style={styles.buttonWrapper}>
              <TouchableOpacity onPress={() => submitData()}>
              <LinearGradient style={styles.buttonLogin} colors={['#F0AB00', '#C76A1E']}>
                <Text style={styles.buttonLoginLabel}>KIRIM</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </View>
      </NativeBaseProvider>
    );
  }
}

const styles = StyleSheet.create({
  contentWrapper: {
    alignSelf: 'center',
    flexDirection: 'column',
    width: width - 80,
    height: 450,
    paddingVertical: 21,
    marginTop: 15,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.1,
    // background color must be set
  },
  camera: {
    alignSelf: 'center'
  },
  dashedLayoutLabel: {
    color: "#9b9b9b",
    fontSize: 18,
    alignSelf: 'center',
    marginHorizontal: 20,
    fontFamily: "avenir_next_medium"
  },
  dashedLayout: {
    width: width - 80,
    flexDirection: 'row',
    height: 146,
    padding: 10,
    marginTop: 25,
    backgroundColor: '#E8E8E8',
    borderStyle: 'dotted',
    alignSelf: 'center',
    borderWidth: 2,
    borderColor: '#A8A8A8',
    borderRadius: 7,
    justifyContent: 'center',
  },
  imageKTP: {
    width: "100%",
    height: "100%",
    resizeMode: "stretch"
  },
  layoutItemsSize: {
    width: "100%",
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 4,
  },
  textHeader: {
    height: 76,
    backgroundColor: "#FFF",
    width: width -30,
    alignSelf: 'center',
    marginTop: 22,
    alignItems: 'center',
    justifyContent: 'center'
  },
  arrowImage: {
    flex: 1,
    width: 40,
    height: 38,
  },
  input: {
    alignSelf: 'center',
    borderRadius: 12,
    borderColor: "#979797",
    marginBottom: 80,
  },
  itemsLabel: {
    alignSelf: 'flex-start',
    textAlign: 'left',
    fontFamily: "avenir_next_medium",
    fontSize: 14,
  },
  itemsSize: {
    alignSelf: 'flex-start',
    textAlign: 'left',
    color: "gray",
    fontFamily: "avenir_next_medium",
    fontSize: 11,
  },
  col: {
    flexDirection: "column",
    justifyContent: 'flex-start',
    textAlign: 'left',
    alignItems: 'center',
    height: 60,
    alignSelf: 'center',
    width: "100%"
  },
  container: {
    width,
    height,
    backgroundColor: "#FFF",
  },
  buttonWrapper: {
    bottom: 0,
    position: 'absolute',
    width: width - 20,
    marginBottom: 20,
    alignSelf: 'center'
  },
  buttonLogin: {
    height: 55,
    width: "100%",
    fontSize: 14,
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
  },
  sectionTitle: {
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_bold",
    letterSpacing: 1,
    fontSize: 30,
  },
  sectionDescription: {
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_next_medium",
    fontSize: 18,
    marginTop: 21,
    fontWeight: "500",
    marginBottom: 35,
    lineHeight: 20,
  },
  bottomText: {
    alignSelf: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_next_demibold",
    textAlign: 'center',
    fontSize: 20,
    bottom: 120,
    position: 'absolute'
  },
  warningIcon: {
    alignSelf: 'center',
    width: 65,
    height: 65,
    position: 'absolute',
    bottom: 180,
  },
  sectionLayout: {
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#3c1053',
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  statusTree: {
    flex: 2,
    alignSelf: 'center',
    marginLeft: 44,
  },
  circleLineGreen: {
    height: 20,
    width: 2,
    alignSelf: 'center',
    backgroundColor: "#C4D600"
  },
  circleLineGray: {
    height: 20,
    width: 2,
    alignSelf: 'center',
    backgroundColor: "#DFDFDF"
  },
  circleLineWhite: {
    height: 20,
    width: 2,
    alignSelf: 'center',
    backgroundColor: "#FFF"
  },
  startTree: {
    alignSelf: 'center',
    flexDirection: 'row',
  },
  treeLabel: {
    textAlign: 'left',
    fontSize: 18,
    alignSelf: 'center',
    color: '#191b26',
    flex: 3,
    fontFamily: "avenir_next_demibold"
  }
});
