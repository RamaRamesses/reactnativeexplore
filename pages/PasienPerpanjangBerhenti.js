import React, {useState, useEffect, useRef} from 'react';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity, SafeAreaView, Alert } from 'react-native';
import Svg, { SvgProps, Path } from "react-native-svg"
import BarHeader from '../components/BarHeader';
import { useFonts } from '@use-expo/font';
import RNPickerSelect from 'react-native-picker-select';
import { Input, NativeBaseProvider, Spinner } from 'native-base';
import { Icon, Overlay } from 'react-native-elements';
import {LinearGradient} from 'expo-linear-gradient';
import AppLoading from 'expo-app-loading';
import { useNavigation } from '@react-navigation/native';
import BarFooter from '../components/BarFooter';
import { getOne } from '../services/api/testLab';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { checkTestLabCheckPoint } from '../helpers';
import PDFReader from 'rn-pdf-reader-js';
import Signature from 'react-native-signature-canvas';
import PDFLib, { PDFDocument, PDFPage } from 'react-native-pdf-lib';
import { continueProgram, terminateProgram } from '../services/api/program';
import { alignSelf } from 'styled-system';
const {width, height} = Dimensions.get("window")

export default function PasienPerpanjangBerhenti() {

  const [description, setDescription] = useState("")
  const [visibleOverlay, setVisibleOverlay] = useState(false)
  const [deceased, setDeceased] = useState(false)
  const [progression, setProgression] = useState(false)
  const [financial, setFinancial] = useState(false)
  const [other, setOther] = useState(false)
  const [message, setMessage] = useState("")

  useEffect(() => {
    getTestLab()
  }, [])

  const navigation = useNavigation()
  const ref = useRef();

  const navigateHome = async () => {
    const route = await checkTestLabCheckPoint()
    navigation.navigate(route.page, { fragment: route.fragment })
    
  }

  const getTestLab = async () => {
    try {
      const token = await AsyncStorage.getItem('@token')
      const testLabId = await AsyncStorage.getItem('@TES_LAB_ID')
      const res = await getOne(token, testLabId)
      if (res.data) {
        if (res.data.laboratorium) {
          setDescription(res.data.laboratorium.address)
        } else {
          console.log(res.data)
        }
      } else {
        console.log(res)
      }
    } catch (e) {
      console.log(e)
    }
  }

  const selection = (choice) => {
    switch(choice) {
      case "deceased":
        setDeceased(!deceased)
        setMessage("Meninggal dunia")
        setProgression(false)
        setFinancial(false)
        setOther(false)
      break;
      case "progression":
        setDeceased(false)
        setProgression(!progression)
        setMessage("Progresi penyakit")
        setFinancial(false)
        setOther(false)
      break;
      case "financial":
        setDeceased(false)
        setProgression(false)
        setMessage("Masalah biaya/finansial")
        setFinancial(!financial)
        setOther(false)
      break;
      case "other":
        setDeceased(false)
        setProgression(false)
        setFinancial(false)
        setMessage("Lain-lain")
        setOther(!other)
        console.log(message)
      break;
    }
  }

  const terminate = async () => {
    try {
      setVisibleOverlay(true)
      const token = await AsyncStorage.getItem('@token')
      const programId = await AsyncStorage.getItem('@UUID_PROGRAM')
      if (token != null) {
        const res = await terminateProgram(token, {
          id: programId,
          message
        })
        if (res.data) {
          console.log(res.data, "berhenti")
          await AsyncStorage.setItem("@UUID_PROGRAM", res.data.id)
          setVisibleOverlay(false)
          navigateHome()
        } else {
          console.log(res)
          Alert.alert(res)
          setVisibleOverlay(false)
        }
      }
    } catch (e) {
      setVisibleOverlay(false)
      console.log(e)
    }
  }

  const [isLoaded] = useFonts({
    'avenir_next_bold': require('../assets/fonts/avenir_next_bold.ttf'),
    'avenir_next_medium': require('../assets/fonts/avenir_next_medium.ttf'),
  });
  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return (
        <NativeBaseProvider style={styles.container}>
          <ScrollView>
              <View style={styles.contentWrapper}>
                <Image
                  style={styles.boardImage}
                  resizeMode="contain"
                  source={require('../assets/pasien_berhenti_pap.png')}
                />
                <Text style={styles.sectionTitle}> Program Anda telah diberhentikan. </Text>
                <Text style={styles.sectionDescription}> Apa alasan Anda berhenti dari program ini? </Text>
                <Text style={styles.bottomText}> {description} </Text>
                <View style={styles.column}>
                  <View style={styles.row}>
                    <TouchableOpacity onPress={() => { selection("deceased") }} style={ deceased ? styles.optionsSelected : styles.options}>
                      <Text style={deceased ? styles.optionLabelSelected : styles.optionLabel }>MENINGGAL DUNIA</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { selection("progression") }} style={ progression ? styles.optionsSelected : styles.options}>
                      <Text style={progression ? styles.optionLabelSelected : styles.optionLabel }>PROGRESI PENYAKIT</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.row}>
                    <TouchableOpacity onPress={() => { selection("financial") }} style={ financial ? styles.optionsSelected : styles.options}>
                      <Text style={financial ? styles.optionLabelSelected : styles.optionLabel }>MASALAH BIAYA</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { selection("other") }} style={ other ? styles.optionsSelected : styles.options}>
                      <Text style={other ? styles.optionLabelSelected : styles.optionLabel }>LAIN-LAIN</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
          <Overlay isVisible={visibleOverlay}>
                <View style={{margin: 5}}>
                    <Spinner color="cyan.500" />
                </View>
              </Overlay>
          <View style={styles.buttonWrapper}>
            <TouchableOpacity onPress={() => terminate()}>
              <LinearGradient style={styles.buttonLogin} colors={['#F0AB00', '#C76A1E']}>
                <Text style={styles.buttonLoginLabel}>KIRIM TANGGAPAN</Text>
              </LinearGradient>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => terminate()}>
              <LinearGradient style={styles.buttonLogin} colors={['#DFDFDF', '#DFDFDF']}>
                <Text style={styles.buttonLoginLabelSecondary}>TUTUP</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
          </ScrollView>
        </NativeBaseProvider>
    );
  }
}

const styles = StyleSheet.create({
  pdf: {
    flex:1,
    width:Dimensions.get('window').width,
    height:Dimensions.get('window').height,
  },
  contentWrapper: {
    alignSelf: 'center',
    flexDirection: 'column',
    width: width -30,
    marginTop: 22,
    alignSelf: 'center',
    marginTop: 55,
    paddingVertical: 21,
    paddingHorizontal: 17,
    // background color must be set
  },
  column: {
    flexDirection: 'column',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  options: {
    flex: 1,
    alignSelf: 'center',
    borderWidth: 2,
    borderColor: "#DFDFDF",
    marginHorizontal: 5,
    padding: 20,
    marginBottom: 10,
  },
  optionsSelected: {
    flex: 1,
    alignSelf: 'center',
    borderWidth: 2,
    borderColor: "#DFDFDF",
    marginHorizontal: 5,
    backgroundColor: "#830051",
    padding: 20,
    marginBottom: 10,
  },
  optionLabel: {
    textAlign: 'center',
    fontFamily: "avenir_next_demibold",
    color: "#6d7278",
    fontSize: 14,
  },
  optionLabelSelected: {
    textAlign: 'center',
    fontFamily: "avenir_next_demibold",
    color: "#FFFFFF",
    fontSize: 14,
  },
  boardImage: {
    width: 360,
    height: 236
  },
  arrowImage: {
    flex: 1,
    width: 40,
    height: 38,
  },
  input: {
    alignSelf: 'center',
    borderRadius: 12,
    borderColor: "#979797",
    marginBottom: 80,
  },
  row: {
    flexDirection: "row",
    justifyContent: 'center'
  },
  container: {
    width,
    height,
    backgroundColor: "#FFF",
  },
  buttonWrapper: {
    width: width - 20,
    alignSelf: 'center',
    bottom: 10,
  },
  buttonLogin: {
    height: 55,
    marginVertical: 5,
    width: "100%",
    fontSize: 14,
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
  },
  buttonLoginLabelSecondary: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 16,
    color: '#727272',
  },
  sectionTitle: {
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_bold",
    fontSize: 38,
    lineHeight: 50,
    letterSpacing: 0.13,
    marginTop: 21,
  },
  sectionDescription: {
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_next_medium",
    fontSize: 18,
    marginTop: 21,
    fontWeight: "700",
    marginBottom: 25,
  },
  bottomText: {
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_bold",
    fontSize: 18,
    fontWeight: "500",
    lineHeight: 25,
    color: "#830051",
  },
});
