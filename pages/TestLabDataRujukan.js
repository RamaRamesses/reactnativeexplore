import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity } from 'react-native';
import Svg, { SvgProps, Path } from "react-native-svg"
import BarHeader from '../components/BarHeader';
import { useFonts } from '@use-expo/font';
import RNPickerSelect from 'react-native-picker-select';
import { Input, NativeBaseProvider } from 'native-base';
import { Icon } from 'react-native-elements';
import {LinearGradient} from 'expo-linear-gradient';
import AppLoading from 'expo-app-loading';
import { useNavigation } from '@react-navigation/native';
import { background, backgroundColor } from 'styled-system';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { getOne } from '../services/api/testLab';
const {width, height} = Dimensions.get("window")

export default function TestLabDataRujukan() {

  useEffect(() => {
    getTestLab()
  }, [])

  const [form, setForm] = useState({
    doctor: '',
    code: '',
    voucher: '',
    laboratorium: '',
    address: ''
  })


  const onChangeSelect = (val) => {
    console.log(val)
  }

  const getTestLab = async () => {
    try {
      const token = await AsyncStorage.getItem('@token')
      const testLabId = await AsyncStorage.getItem('@TES_LAB_ID')
      const res = await getOne(token, testLabId)
      if (res.data) {
        let object = {};
        console.log(res.data, "Res data")
        if (res.data.doctor) {
          console.log(res.data.doctor, "doctor")
          object.doctor = res.data.doctor.fullname
          object.code = res.data.doctor.code
        }
        if (res.data.laboratorium) {
          object.laboratorium = res.data.laboratorium.name
          object.address = res.data.laboratorium.address
        }
        if (res.data.voucher) {
          object.voucher = res.data.voucher.code
        }
        setForm(object)
      } else {
        console.log(res)
      }
    } catch (e) {
      console.log(e)
    }
  }

  const navigation = useNavigation()
  const [isLoaded] = useFonts({
    'avenir_next_bold': require('../assets/fonts/avenir_next_bold.ttf'),
    'avenir_next_medium': require('../assets/fonts/avenir_next_medium.ttf'),
  });
  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return (
      <NativeBaseProvider>
        <View style={styles.container}>
          <BarHeader header="TES LABORATORIUM" />
          <View style={styles.textHeader}>
            <Text style={styles.sectionTitle}> Data Pengajuan Tes Lab</Text>
          </View>
          <View style={styles.contentWrapper}>
            <View style={styles.row}>
              <Text style={styles.itemsLabel}>Nama dokter </Text>
              <Text style={{flex: 1}}>: </Text>
              <Text style={styles.itemsText}>{form.doctor}</Text>
            </View>
            <View style={styles.row}>
              <Text style={styles.itemsLabel}>Kode dokter </Text>
              <Text style={{flex: 1}}>: </Text>
              <Text style={styles.itemsText}>{form.code}</Text>
            </View>
            <View style={styles.row}>
              <Text style={styles.itemsLabel}> Voucher </Text>
              <Text style={{flex: 1}}>: </Text>
              <Text style={styles.itemsText}>{form.voucher}</Text>
            </View>
            <View style={styles.row}>
              <Text style={styles.itemsLabel}> Laboratorium </Text>
              <Text style={{flex: 1}}>: </Text>
              <Text style={styles.itemsText}>{form.laboratorium}, {form.address}</Text>
            </View>
          </View>
          <View style={styles.buttonWrapper}>
              <TouchableOpacity onPress={() => { navigation.navigate("TestLabUnggahHasilTes") }}>
              <LinearGradient style={styles.buttonLogin} colors={['#F0AB00', '#C76A1E']}>
                <Text style={styles.buttonLoginLabel}>UNGGAH HASIL TES</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </View>
      </NativeBaseProvider>
    );
  }
}

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 18,
    paddingVertical: 12,
    borderWidth: 1,
    borderRadius: 13,
    borderColor: 'gray',
    width: 140,
    marginBottom: 20,
    marginHorizontal: 5,
    paddingHorizontal: 10,
    color: 'black',
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});

const laboratoriumPickerStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 18,
    paddingVertical: 12,
    borderWidth: 1,
    borderRadius: 13,
    borderColor: 'gray',
    width: "84%",
    marginHorizontal: 15,
    alignSelf: 'center',
    paddingHorizontal: 10,
    height: 40,
    color: 'black',
  },
})

const styles = StyleSheet.create({
  contentWrapper: {
    alignSelf: 'center',
    flexDirection: 'column',
    width: width -30,
    height: 450,
    paddingVertical: 21,
    paddingHorizontal: 17,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.1,
    // background color must be set
  },
  textHeader: {
    height: 76,
    backgroundColor: "#FFF",
    width: width -30,
    alignSelf: 'center',
    marginTop: 22,
    alignItems: 'center',
    justifyContent: 'center'
  },
  arrowImage: {
    flex: 1,
    width: 40,
    height: 38,
  },
  input: {
    alignSelf: 'center',
    borderRadius: 12,
    borderColor: "#979797",
    marginBottom: 80,
  },
  itemsLabel: {
    flex: 8,
    alignSelf: 'center',
    textAlign: 'center'
  },
  itemsText: {
    flex: 14,
    paddingHorizontal: 5
  },
  row: {
    flexDirection: "row",
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#FFF",
    height: 60,
    width: width - 30,
    alignSelf: 'center',
    marginBottom: 1
  },
  container: {
    width,
    height,
    backgroundColor: "#f1f5f7"
  },
  buttonWrapper: {
    bottom: 0,
    position: 'absolute',
    width: width - 20,
    marginBottom: 20,
    alignSelf: 'center'
  },
  buttonLogin: {
    height: 55,
    width: "100%",
    fontSize: 14,
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
  },
  sectionTitle: {
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_bold",
    letterSpacing: 1,
    fontSize: 30,
  },
  sectionDescription: {
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_next_medium",
    fontSize: 18,
    marginTop: 21,
    fontWeight: "500",
    marginBottom: 35,
    lineHeight: 20,
  },
  bottomText: {
    alignSelf: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_next_demibold",
    textAlign: 'center',
    fontSize: 20,
    bottom: 120,
    position: 'absolute'
  },
  warningIcon: {
    alignSelf: 'center',
    width: 65,
    height: 65,
    position: 'absolute',
    bottom: 180,
  },
  sectionLayout: {
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#3c1053',
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  statusTree: {
    flex: 2,
    alignSelf: 'center',
    marginLeft: 44,
  },
  circleLineGreen: {
    height: 20,
    width: 2,
    alignSelf: 'center',
    backgroundColor: "#C4D600"
  },
  circleLineGray: {
    height: 20,
    width: 2,
    alignSelf: 'center',
    backgroundColor: "#DFDFDF"
  },
  circleLineWhite: {
    height: 20,
    width: 2,
    alignSelf: 'center',
    backgroundColor: "#FFF"
  },
  startTree: {
    alignSelf: 'center',
    flexDirection: 'row',
  },
  treeLabel: {
    textAlign: 'left',
    fontSize: 18,
    alignSelf: 'center',
    color: '#191b26',
    flex: 3,
    fontFamily: "avenir_next_demibold"
  }
});
