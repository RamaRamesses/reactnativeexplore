import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font'
import { useFonts } from '@use-expo/font';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Svg, { Path,
  Rect,
  Circle,
  Mask,
  Defs,
  LinearGradient,
  Stop, } from "react-native-svg"
import { useNavigation } from '@react-navigation/native';
import {LinearGradient as ExpoLinearGradient} from 'expo-linear-gradient';
import { marginTop, style } from 'styled-system';
import BarHeader from '../components/BarHeader';
const {width, height} = Dimensions.get("window")

export default function PilihJenisProgram() {
  const navigation = useNavigation()
  return (
    <>
      <BarHeader header="PILIH PROGRAM" />
      <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
        <View style={styles.layout}>
          <View style={styles.row}>
            <View style={styles.imageWrapper}>
            <Svg
              style={styles.boardImage}
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <Path
                d="M12.682 35c7.5-6 23.166-7 30-5 17 4 15.5-2.5 33.5-6 14.4-2.8 22.5-2.5 26 3.5 3.791 6.5 3.6 17.7-8 20.5-11.6 2.8-24.167 16.167-29 22.5-4.334 6-11.5 18.969-35 17.5-16-1-26-17.5-27-26.5-.731-6.579.42-19.236 9.5-26.5z"
                fill="url(#prefix__paint0_linear)"
                fillOpacity={0.75}
              />
              <Rect x={27.682} y={13} width={64} height={64} rx={10} fill="#800152" />
              <Circle cx={20.682} cy={74} r={4} fill="#fff" />
              <Path
                d="M5.983 28.174c2.8 0 2.5-1.5 4.5-4 1.5-1.5 4.671-2.678 6-6 1-2.5-.353-5.338-3-6-2.647-.662-10.1.4-12.5 6-3 7 1.5 10 5 10z"
                fill="#FA9770"
              />
              <Circle cx={96.5} cy={4.5} r={4.5} fill="#FA9770" />
              <Circle cx={43} cy={32.676} r={4} stroke="#fff" strokeWidth={2} />
              <Path
                d="M70.895 47.787c.128-.076.24-.137.337-.186l.023.075H72a8 8 0 11-8 8c0-.192.07-.505.274-.948.199-.431.496-.922.875-1.452.758-1.058 1.79-2.192 2.85-3.192 1.065-1.004 2.115-1.83 2.896-2.297zm.758-.353a.054.054 0 01.008 0h-.008z"
                stroke="#fff"
                strokeWidth={2}
              />
              <Circle cx={54.5} cy={28.176} r={2.5} fill="#fff" />
              <Circle cx={38.5} cy={44.176} r={2.5} fill="#fff" />
              <Rect
                x={70.324}
                y={22.414}
                width={18}
                height={50.784}
                rx={9}
                transform="rotate(45 70.324 22.414)"
                stroke="#fff"
                strokeWidth={2}
              />
              <Path d="M52 41l12.5 12.5M64.5 56.5h15" stroke="#fff" strokeWidth={2} />
              <Defs>
                <LinearGradient
                  id="prefix__paint0_linear"
                  x1={67.374}
                  y1={45.388}
                  x2={-9.13}
                  y2={45.304}
                  gradientUnits="userSpaceOnUse"
                >
                  <Stop stopColor="#F0AB00" />
                  <Stop offset={0.839} stopColor="#830051" />
                </LinearGradient>
              </Defs>
            </Svg>
            </View>
            <View style={styles.labelsLayout}>
              <Text style={styles.cardTitle}>PULIH PAP </Text>
              <Text style={styles.cardDescription}>Patient Access and Affordability Program atau PAP merupakan program kerja sama AZI dan YKI untuk menyediakan akses pengobatan kanker kepada pasien. </Text>
            </View>
          </View>
          <View style={styles.buttonWrapper}>
              <TouchableOpacity onPress={() => { navigation.navigate("PilihProgramPAP") }}>
              <ExpoLinearGradient style={styles.buttonLogin} colors={['#830051','#830051', '#F0AB00']}  start={{ x: -1, y: 0 }}
                end={{ x: 1, y: 0 }}>
                <Text style={styles.buttonLoginLabel}>AJUKAN PROGRAM PAP</Text>
              </ExpoLinearGradient>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.layout}>
          <View style={styles.row}>
            <View style={styles.imageWrapper}>
          <Svg
              style={styles.boardImage}
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <Path
                d="M9.682 36c7.5-6 23.166-7 30-5 17 4 15.5-2.5 33.5-6 14.4-2.8 22.5-2.5 26 3.5 3.791 6.5 3.6 17.7-8 20.5-11.6 2.8-24.167 16.167-29 22.5-4.334 6-11.5 18.969-35 17.5-16-1-26-17.5-27-26.5-.731-6.579.42-19.236 9.5-26.5z"
                fill="url(#prefix__paint0_linear)"
              />
              <Rect x={24.682} y={14} width={64} height={64} rx={10} fill="#800152" />
              <Circle cx={17.682} cy={75} r={4} fill="#fff" />
              <Path
                d="M6.983 30.174c2.8 0 2.5-1.5 4.5-4 1.5-1.5 4.671-2.678 6-6 1-2.5-.353-5.338-3-6-2.647-.662-10.1.4-12.5 6-3 7 1.5 10 5 10z"
                fill="#357FBA"
                fillOpacity={0.75}
              />
              <Circle cx={96.5} cy={3.5} r={3.5} fill="#357FBA" fillOpacity={0.75} />
              <Rect
                x={46}
                y={26}
                width={29}
                height={39}
                rx={1}
                stroke="#fff"
                strokeWidth={2}
              />
              <Rect
                width={21}
                height={4}
                rx={0.5}
                transform="matrix(1 0 0 -1 50 35)"
                fill="#fff"
              />
              <Rect x={57} y={38} width={14} height={2} rx={0.5} fill="#fff" />
              <Rect x={62} y={44} width={9} height={2} rx={0.5} fill="#fff" />
              <Rect x={62} y={50} width={9} height={2} rx={0.5} fill="#fff" />
              <Rect x={62} y={56} width={9} height={2} rx={0.5} fill="#fff" />
              <Mask id="prefix__a" fill="#fff">
                <Path d="M32 42.833a1.167 1.167 0 011.167-1.166c4.142 0 8.179-1.467 12.133-4.434a1.167 1.167 0 011.4 0c3.954 2.967 7.99 4.434 12.133 4.434A1.167 1.167 0 0160 42.833V51c0 7.78-4.601 13.496-13.572 17.03-.275.109-.58.109-.856 0C36.602 64.496 32 58.778 32 51v-8.167z" />
              </Mask>
              <Path
                d="M32 42.833a1.167 1.167 0 011.167-1.166c4.142 0 8.179-1.467 12.133-4.434a1.167 1.167 0 011.4 0c3.954 2.967 7.99 4.434 12.133 4.434A1.167 1.167 0 0160 42.833V51c0 7.78-4.601 13.496-13.572 17.03-.275.109-.58.109-.856 0C36.602 64.496 32 58.778 32 51v-8.167z"
                fill="#fff"
                stroke="#fff"
                strokeWidth={2}
                mask="url(#prefix__a)"
              />
              <Path
                d="M46 43a1.25 1.25 0 011.25 1.25v7.5h7.5a1.25 1.25 0 010 2.5h-7.5v7.5a1.25 1.25 0 01-2.5 0v-7.5h-7.5a1.25 1.25 0 010-2.5h7.5v-7.5A1.25 1.25 0 0146 43z"
                fill="#800152"
              />
              <Defs>
                <LinearGradient
                  id="prefix__paint0_linear"
                  x1={0}
                  y1={83}
                  x2={108.5}
                  y2={34}
                  gradientUnits="userSpaceOnUse"
                >
                  <Stop offset={0.027} stopColor="#800152" />
                  <Stop offset={0.574} stopColor="#81FE9D" />
                  <Stop offset={1} stopColor="#357FBA" />
                </LinearGradient>
              </Defs>
            </Svg>
            </View>
            <View style={styles.labelsLayout}>
              <Text style={styles.cardTitle}>AURORA </Text>
              <Text style={styles.cardDescription}>Program bantuan terapi untuk
                menyediakan akses pengobatan
                kanker bagi pasien kanker yang
                memiliki asuransi sesuai dengan
                kriteria tertentu.</Text>
            </View>
          </View>
          <View style={styles.buttonWrapper}>
              <TouchableOpacity>
              <ExpoLinearGradient style={styles.buttonLogin} colors={['#800152', '#800152', "#357FBA"]} start={{ x: -1, y: 0 }}
end={{ x: 1, y: 0 }}>
                <Text style={styles.buttonLoginLabel}>AJUKAN PROGRAM AURORA</Text>
              </ExpoLinearGradient>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.layout}>
          <View style={styles.row}>
            <View style={styles.imageWrapper}>
            <Svg
              style={styles.boardImage}
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <Path
                d="M11.682 35c7.5-6 23.166-7 30-5 17 4 15.5-2.5 33.5-6 14.4-2.8 22.5-2.5 26 3.5 3.791 6.5 3.6 17.7-8 20.5-11.6 2.8-24.167 16.167-29 22.5-4.334 6-11.5 18.969-35 17.5-16-1-26-17.5-27-26.5-.731-6.579.42-19.236 9.5-26.5z"
                fill="url(#prefix__paint0_linear)"
                fillOpacity={0.75}
              />
              <Rect x={26.682} y={13} width={64} height={64} rx={10} fill="#800152" />
              <Circle cx={19.682} cy={74} r={4} fill="#fff" />
              <Path
                d="M5.88 28.213c2.694.76 2.813-.765 5.416-2.629 1.85-1.036 5.223-1.31 7.403-4.146 1.641-2.135 1.11-5.234-1.259-6.59-2.368-1.354-9.83-2.355-13.66 2.384-4.786 5.923-1.27 10.031 2.1 10.981z"
                fill="#D12121"
                fillOpacity={0.55}
              />
              <Circle cx={95.5} cy={4.5} r={4.5} fill="#D12121" fillOpacity={0.55} />
              <Path
                d="M55.743 34.78c-.085 1.1-.952 3.701-1.375 4.865-1.375 4.866 3.173 18.511-.846 21.684-4.02 3.174-14.704 2.328-15.973 2.328-1.27 0-2.327-3.068-2.539-5.5-.211-2.434 2.856-19.781 4.443-23.378 1.587-3.596 8.145-8.356 12.482-7.722 4.337.635 3.914 6.347 3.808 7.722zM62.257 34.837c.085 1.1.952 3.702 1.375 4.865 1.375 4.866-3.173 18.512.846 21.685 4.02 3.173 14.704 2.327 15.973 2.327 1.27 0 2.327-3.068 2.539-5.5.211-2.433-2.856-19.781-4.443-23.377-1.587-3.597-8.145-8.357-12.482-7.722-4.337.635-3.914 6.347-3.808 7.722z"
                stroke="#fff"
                strokeWidth={2}
              />
              <Path
                d="M64.31 38.904c-.93-.084-3.38-2.362-4.121-3.49h-1.167v2.221c1.692 1.834 6.03 4.76 7.616 7.616 1.638 2.949 1.375 4.549 1.163 4.972-.211.423-2.961 1.798-2.22 2.01.74.211 1.903-.847 2.538-1.27.508-.338.705-1.904.74-2.644.74 1.586-.423 6.452-1.057 7.404-.508.762-1.34 1.375-1.693 1.587-.317.388-.55.93 1.058 0 1.608-.931 2.362-3.773 2.538-5.078.53.424-.528 4.549-.528 4.866 0 .318-1.799 2.433-1.376 2.433.424 0 1.693-1.904 1.376-1.058-.318.847-.424 2.75-.212 2.75.17 0 .423-1.41.529-2.115 0 .141.042.571.212 1.164.211.74 1.48 2.01 1.586 1.798.106-.212-1.375-2.645-1.587-3.28-.211-.634.212-3.596.53-4.23.253-.508.458-2.045.528-2.75.53.211 5.29 4.548 5.606 4.76.318.211.424 1.903 1.058 1.269.508-.508-1.622-2.821-2.75-3.914h2.539c.317 0 2.75 1.48 1.904.317-.847-1.163-4.654-.846-5.183-.846-.53 0-1.904-1.27-2.75-2.01-.678-.592-1.27-3.35-1.481-4.654.634.317 3.173.423 4.02.106.845-.318 1.903 1.692 2.115 2.221.169.423.493.6.634.635-.211-2.327-1.163-2.645-1.586-3.174-.423-.528-1.799-.211-2.856-.105-1.058.105-2.116-.423-2.433-.635-.254-.17-2.08-2.539-2.962-3.702-.247-.423-.698-1.396-.529-1.904.212-.635 3.49-.318 4.337-.529.677-.17 2.75 1.551 3.702 2.433.635.317 1.904.846 1.904.423s-2.256-1.728-3.385-2.327c.882-.07 2.793-.275 3.385-.529.74-.317.423-.529 0-.74-.423-.212-3.067.74-3.702.74-.508 0-1.692-.494-2.221-.74.846-.74.74-1.904.74-2.75 0-.678.564-2.187.846-2.857-1.692.74-1.692 2.856-1.586 3.703.105.846-.424 1.48-.847 1.904-.338.338-1.057.14-1.375 0 1.693-1.27.952-4.126 0-5.924-.761-1.439-2.503-2.433-3.279-2.75.987.916 3.025 3.385 3.28 5.923.317 3.174-2.434 2.856-3.597 2.75zM53.7 38.904c.93-.084 3.201-2.023 3.941-3.15l1.347-.34v2.221c-1.692 1.834-6.03 4.76-7.616 7.616-1.638 2.949-1.375 4.549-1.163 4.972.211.423 2.961 1.798 2.22 2.01-.74.211-1.903-.847-2.538-1.27-.508-.338-.705-1.904-.74-2.644-.74 1.586.423 6.452 1.058 7.404.507.762 1.34 1.375 1.692 1.587.317.388.55.93-1.058 0-1.608-.931-2.362-3.773-2.538-5.078-.53.424.529 4.549.529 4.866 0 .318 1.798 2.433 1.375 2.433-.424 0-1.693-1.904-1.376-1.058.318.847.424 2.75.212 2.75-.17 0-.423-1.41-.529-2.115 0 .141-.042.571-.211 1.164-.212.74-1.481 2.01-1.587 1.798-.106-.212 1.375-2.645 1.587-3.28.211-.634-.212-3.596-.53-4.23-.253-.508-.458-2.045-.528-2.75-.53.211-5.29 4.548-5.606 4.76-.318.211-.423 1.903-1.058 1.269-.508-.508 1.622-2.821 2.75-3.914h-2.539c-.317 0-2.75 1.48-1.904.317.847-1.163 4.655-.846 5.184-.846.528 0 1.904-1.27 2.75-2.01.677-.592 1.269-3.35 1.48-4.654-.634.317-3.173.423-4.019.106-.846-.318-1.904 1.692-2.115 2.221-.17.423-.494.6-.635.635.211-2.327 1.163-2.645 1.587-3.174.423-.528 1.798-.211 2.855-.105 1.058.105 2.116-.423 2.433-.635.254-.17 2.08-2.539 2.962-3.702.247-.423.698-1.396.529-1.904-.212-.635-3.49-.318-4.337-.529-.677-.17-2.75 1.551-3.702 2.433-.635.317-1.904.846-1.904.423s2.257-1.728 3.385-2.327c-.882-.07-2.793-.275-3.385-.529-.74-.317-.423-.529 0-.74.423-.212 3.068.74 3.702.74.508 0 1.693-.494 2.222-.74-.847-.74-.741-1.904-.741-2.75 0-.678-.564-2.187-.846-2.857 1.692.74 1.692 2.856 1.587 3.703-.106.846.423 1.48.846 1.904.338.338 1.057.14 1.375 0-1.693-1.27-.952-4.126 0-5.924.761-1.439 2.503-2.433 3.279-2.75-.987.916-3.025 3.385-3.28 5.923-.316 3.174 2.434 2.856 3.597 2.75z"
                fill="#fff"
              />
              <Path
                d="M57.642 27.15v8.672L59 37.564l1.259-1.632c-.046-2.855.179-8.783.069-8.783H57.64z"
                fill="#fff"
              />
              <Defs>
                <LinearGradient
                  id="prefix__paint0_linear"
                  x1={24.46}
                  y1={55.204}
                  x2={81.028}
                  y2={55.678}
                  gradientUnits="userSpaceOnUse"
                >
                  <Stop stopColor="#800152" />
                  <Stop offset={1} stopColor="#D12121" stopOpacity={0.55} />
                </LinearGradient>
              </Defs>
            </Svg>
            </View>
            <View style={styles.labelsLayout}>
              <Text style={styles.cardTitle}>PULIH ASTHMA </Text>
              <Text style={styles.cardDescription}>Program bantuan terapi untuk
                menyediakan akses pengobatan
                kanker bagi pasien kanker yang
                memiliki asuransi sesuai dengan
                kriteria tertentu.</Text>
            </View>
          </View>
          <View style={styles.buttonWrapper}>
              <TouchableOpacity>
              <ExpoLinearGradient style={styles.buttonLogin} start={{ x: -1, y: 0 }}
end={{ x: 1, y: 0 }} colors={['#660142', '#660142', '#B86A6A',]}>
                <Text style={styles.buttonLoginLabel}>AJUKAN PROGRAM ASTHMA</Text>
              </ExpoLinearGradient>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f1f5f7'
  },
  layout: {
    marginHorizontal: 20,
    flexDirection: 'column',
    marginTop: 10,
    backgroundColor: 'white',
    marginBottom: 20,
    borderRadius: 15,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
    padding: 10,
  },
  buttonWrapper: {
    width: "100%",
  },
  buttonLogin: {
    height: 50,
    fontSize: 14,
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
  },
  row: {
    alignSelf: 'center',
    marginVertical: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    // background color must be set
  },
  labelsLayout: {
    flex: 3,
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  cardTitle: {
    fontFamily: 'avenir_bold',
    fontSize: 20,
    color: "black",
    flex: 1,
  },
  cardDescription: {
    fontFamily: 'avenir_next_demibold',
    color: "gray",
    lineHeight: 20,
    marginTop: 7,
    fontSize: 12,
    flex: 2,
  },
  imageWrapper: {
    marginVertical: 0,
    marginLeft: 11,
    flex: 2
  },
  boardImage: {
    width: 130,
    height: 110,
  },
});
