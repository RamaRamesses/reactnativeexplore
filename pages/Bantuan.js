import React, {useState, useEffect} from 'react';
import AppLoading from 'expo-app-loading';
import { useFonts } from '@use-expo/font';
import { StyleSheet, SafeAreaView, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity, TouchableHighlight, CheckBox, PickerIOSBase } from 'react-native';
import { Checkbox, Center, NativeBaseProvider } from "native-base"
import {LinearGradient} from 'expo-linear-gradient';
import { Input, Icon, Overlay } from 'react-native-elements';
const {width, height} = Dimensions.get("window")
import { Appbar as NativeAppBar, Searchbar } from 'react-native-paper';
import { getAll } from '../services/api/help'
import { register } from '../services/api/auth'
import { useNavigation, useIsFocused } from '@react-navigation/native';
import RNPickerSelect from 'react-native-picker-select';
import { useDispatch, useSelector } from 'react-redux';
import { checkTestLabCheckPoint } from '../helpers';
import { getById } from '../services/api/patient';
import BarHeader from '../components/BarHeader';
import AsyncStorage from '@react-native-async-storage/async-storage';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import { baseUrl } from '../config';
import ImageView from "react-native-image-viewing";
import BarFooter from '../components/BarFooter';
import Svg, { Path } from "react-native-svg"

export default function Bantuan() {
  const navigation = useNavigation();
  const isFocused = useIsFocused();

  useEffect(() => {
    loadData()
  }, [isFocused])

  const [isLoaded] = useFonts({
    'avenir_next_bold': require('../assets/fonts/avenir_next_bold.ttf'),
    'avenir_next_medium': require('../assets/fonts/avenir_next_medium.ttf'),
    'avenir_next_demibold': require('../assets/fonts/avenir_next_demibold.ttf'),
    'avenir_heavy': require('../assets/fonts/avenir_heavy.ttf'),
  });

  const [password, setPassword] = useState(true);
  const [pertanyaanSeringDiajukan, setPertanyaanSeringDiajukan] = useState([]);
  const [pertanyaanUmum, setPertanyaanUmum] = useState([]);
  const [uniqueIndex, setUniqueIndex] = useState(1)

  const [user, setUser] = useState({
    status: 1,
    email: '',
    password: '',
    city: '',
    idNumber: '',
    fullname: '',
    gender: '',
    code: '',
    dateOfBirth: ''
  })

  const loadData = async () => {
    try {
      const token = await AsyncStorage.getItem('@token')
      const res = await getAll()
        if (res.data) {
        let faq = []
        let general = []
        setPertanyaanSeringDiajukan([])
        setPertanyaanUmum([])
        pertanyaanSeringDiajukan.forEach(el => faq.push(el))
        pertanyaanUmum.forEach(el => general.push(el))
        for (const i in res.data) {
          let body = res.data
          if (body[i].name.toString() == "Pertanyaan Umum") {
            setPertanyaanUmum([])
            general = []
            for (const j in body[i].subcategory) {
              let subcategory = body[i].subcategory
              for (const r in subcategory[j].help) {
                subcategory[j].help[r].visible = false
              }
              general.push({
                name: subcategory[j].name,
                help: subcategory[j].help,
                visible: false
              })
            }
            console.log(general)
            setPertanyaanUmum(general)
            general = []
          } else {
            faq = []
            setPertanyaanSeringDiajukan([])
            for (const j in body[i].subcategory) {
              let subcategory = body[i].subcategory
              for (const r in subcategory[j].help) {
                subcategory[j].help[r].visible = false
              }
              faq.push({
                name: subcategory[j].name,
                help: subcategory[j].help,
                visible: false
              })
            }
            setPertanyaanSeringDiajukan(faq)
            faq = []
          }
        }
      } else {
        console.log(e)
      }
    } catch (e) {
      console.log(e)
    }
  }

  const categoryOnTouch = async (index, category) => {
    let newCategory = {}
    switch (category) {
      case "faq":
        newCategory = pertanyaanSeringDiajukan
        newCategory[index].visible = !newCategory[index].visible
        setPertanyaanSeringDiajukan(newCategory)
        console.log(newCategory, "new")
        break;
      case "general":
        newCategory = pertanyaanUmum
        newCategory[index].visible = !newCategory[index].visible
        setPertanyaanUmum(newCategory)
        break;
    }
    setUniqueIndex(uniqueIndex + 1)
  }

  const subcategoryOnTouch = async (catIndex, subIndex, category) => {
    let newCategory = {}
    switch (category) {
      case "faq":
        newCategory = pertanyaanSeringDiajukan
        newCategory[catIndex].help[subIndex].visible = !newCategory[catIndex].help[subIndex].visible
        setPertanyaanSeringDiajukan(newCategory)
        console.log(newCategory, "new")
        break;
      case "general":
        newCategory = pertanyaanUmum
        newCategory[catIndex].help[subIndex].visible =!newCategory[catIndex].help[subIndex].visible
        break;
    }
    setUniqueIndex(uniqueIndex + 1)
  }


  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return (
      <NativeBaseProvider>
      <BarHeader header="BANTUAN" />
        <ScrollView style={{backgroundColor: "#f1f5f7"}}>
          <View key={uniqueIndex} style={styles.container}>
            <Searchbar
              placeholder="Ketik kata kunci"
              style={styles.searchBar}
            />
            <Text style={styles.sectionText}>Pertanyaan yang Sering Diajukan</Text>
            {
              pertanyaanSeringDiajukan.map((data, i) => (
                <View key={i} style={styles.contentWrapper}>
                  <TouchableOpacity style={styles.row} onPress={() => categoryOnTouch(i, "faq")}>
                  <View style={styles.itemsTextLayout}>
                      <Text style={styles.contentTitle}>{data.name}</Text>
                    </View>
                    <Svg style={data.visible == true ? styles.arrowImage : styles.arrowImageRotate} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                      <Path d="M10 17l5-5-5-5v10z" fill="#830051" />
                    </Svg>
                  </TouchableOpacity>
                  <View style={styles.subcategory}>
                  {
                    data.visible == true ?
                    data.help.map((subcat, j) => (
                      <View key={j}>
                        <TouchableOpacity style={styles.row} onPress={() => subcategoryOnTouch(i, j, "faq")}>
                          <Text style={styles.itemsText}>{subcat.title}</Text>
                        </TouchableOpacity>
                        {
                          subcat.visible ? (
                          <View style={styles.row}>
                            <Text style={styles.subItemText}>{subcat.body}</Text>
                          </View>              
                          ) : (<></>)
                        }
                      </View>
                    )) : (<></>)
                  }
                  </View>
                </View>
              ))
            }
            <Text style={styles.sectionText}>Pertanyaan Umum</Text>
            {
              pertanyaanUmum.map((data, i) => (
                <View key={i} style={styles.contentWrapper}>
                  <TouchableOpacity style={styles.row} onPress={() => categoryOnTouch(i, "general")}>
                  <View style={styles.itemsTextLayout}>
                      <Text style={styles.contentTitle}>{data.name}</Text>
                    </View>
                    <Svg style={data.visible == true ? styles.arrowImage : styles.arrowImageRotate} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                      <Path d="M10 17l5-5-5-5v10z" fill="#830051" />
                    </Svg>
                  </TouchableOpacity>
                  <View style={styles.subcategory}>
                  {
                    data.visible == true ?
                    data.help.map((subcat, j) => (
                      <View key={j}>
                        <TouchableOpacity style={styles.row} onPress={() => subcategoryOnTouch(i, j, "general")}>
                          <Text style={styles.itemsText}>{subcat.title}</Text>
                        </TouchableOpacity>
                        {
                          subcat.visible ? (
                          <View style={styles.row}>
                            <Text style={styles.subItemText}>{subcat.body}</Text>
                          </View>              
                          ) : (<></>)
                        }
                      </View>
                    )) : (<></>)
                  }
                  </View>
                </View>
              ))
            }
            {/* <View style={styles.contentWrapper}>
              <View style={styles.row}>
                <View style={styles.itemsTextLayout}>
                  <Text style={styles.contentTitle}>Tentang Kami</Text>
                </View>
                <Svg style={styles.arrowImage} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                  <Path d="M10 17l5-5-5-5v10z" fill="#830051" />
                </Svg>
              </View>
              <>
              </>
            </View> */}
            <Text style={styles.bottomText}>Bila Anda mengalami kendala teknis, silakan menghubungi: PAPprogram.com</Text>
          </View>
        </ScrollView>
        <BarFooter />
      </NativeBaseProvider>
    );
  }
}

const styles = StyleSheet.create({
  sectionText: {
    textAlign: 'left',
    alignSelf: 'flex-start',
    fontSize: 20,
    fontWeight: "600",
    letterSpacing: 0,
    marginBottom: 6,
    fontFamily: 'avenir_next_demibold',
    marginTop: 26,
  },
  itemsText: {
    fontFamily: 'avenir_next_demibold',
    fontSize: 14,
    fontWeight: "bold",
    flex: 1,
    color: "#830051"
  },
  subItemText: {
    fontFamily: 'avenir_next_demibold',
    fontSize: 14,
    fontWeight: "bold",
    flex: 1,
  },
  bottomText: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: "600",
    letterSpacing: 0,
    marginBottom: 6,
    fontFamily: 'avenir_next_demibold',
    marginTop: 26,
  },
  contentWrapper: {
    flexDirection: 'column',
    borderRadius: 8,
    marginTop: 5,
    padding: 0,
    width: "100%"
  },
  searchBar: {
    marginTop: 16,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 20,
    marginBottom: 1,
    paddingHorizontal: 15,
    backgroundColor: "#FFF",
  },
  contentTitle: {
    alignSelf: 'flex-start',
    fontFamily: "avenir_next_demibold",
    fontSize: 18,
    fontWeight: "600",
    letterSpacing: 0,
  },
  arrowImage: {
    flex: 1,
    width: 37,
    height: 30,
  },
  arrowImageRotate: {
    transform: [{ rotate: '90deg' }],
    flex: 1,
    width: 37,
    height: 30,
  },
  container: {
    marginVertical: 20,
    width: width - 30,
    alignSelf: 'center'
  },
  itemsTextLayout: {
    flex: 10,
    alignSelf: 'center'
  }
});
