import React, { useRef, useState } from "react";
import { StyleSheet, Text, View, Image, Dimensions, Button, TouchableOpacity } from "react-native";
import Signature from "react-native-signature-canvas";
import { upload, uploadSignature } from '../services/api/fileUpload';
import * as FileSystem from 'expo-file-system';
import BarHeader from '../components/BarHeader';
import {LinearGradient} from 'expo-linear-gradient';
import { degrees, PDFDocument, rgb, StandardFonts } from 'pdf-lib';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';
const {width, height} = Dimensions.get("window")

export const SignatureDrawer = () => {
  const ref = useRef();
  const navigation = useNavigation();
  const [signature, setSign] = useState(null);

  const handleOK = async (signature) => {
    setSign(signature);
    try {
      const patientString = await AsyncStorage.getItem('@patient')
      const programType = await AsyncStorage.getItem('@NAMA_PROGRAM_TYPE')
      const patient = JSON.parse(patientString)
      const data = await dataURLtoFile(signature)
      // dataURLtoFile(decodedImg)
      let formData = new FormData();
      formData.append('upload', { uri: data, name: "png", type: "image/png" });    
      const res = await uploadSignature(patient.fullname, programType, formData);
      console.log(res.data.path)
      await AsyncStorage.setItem("@CONSENT", res.data.path)
      navigation.navigate("PapPersetujuanDokumen", { signed: true })
      // console.log(data)
    } catch (e) {
      console.log(e, "deadwrong")
    }
    // var decodedImg = decodeBase64Image(signature);
    // console.log(signature)
  };

  const dataURLtoFile = async (dataurl) => {
    console.log(dataurl, "dataUrl")
    const base64Code = dataurl.split("data:image/png;base64,")[1];

    const filename = FileSystem.documentDirectory + "some_unique_file_name.png";
    await FileSystem.writeAsStringAsync(filename, base64Code, {
      encoding: FileSystem.EncodingType.Base64,
    });
    return filename
  }

  const handleEmpty = () => {
    console.log("Empty");
  };

  const handleClear = () => {
    ref.current.clearSignature();
  };

  const handleConfirm = () => {
    console.log("end");
    ref.current.readSignature();
  };

  const style = `.m-signature-pad--footer
  {
    display: none;
    margin: 0px;
    height: 200px;
  }
  body,html {
    width: 100%; height: 250px;}`;
  return (
    <View style={styles.container}>
      <BarHeader header="PROGRAM PULIH" />
      <View style={styles.contentWrapper}>
        <View style={styles.textWrapper}>
          <Text style={styles.headerText}> Silahkan tanda tangan {"\n"} di dalam kotak berikut </Text>
        </View>
      </View>
      {/* <View style={styles.preview}>
        {signature ? (
          <Image
            resizeMode={"contain"}
            style={{ width: 335, height: 114 }}
            source={{ uri: signature }}
          />
        ) : null}
      </View> */}
      <View style={styles.signatureWrapper}>
        <Signature
          ref={ref}
          descriptionText="Sign"
          onOK={handleOK}
          webStyle={style}
        />
      </View>
       <View style={styles.buttonWrapper}>
          <TouchableOpacity onPress={() => handleConfirm()}>
          <LinearGradient style={styles.button} colors={['#F0AB00', '#C76A1E']}>
            <Text style={styles.buttonYes}>SIMPAN</Text>
          </LinearGradient>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => handleClear()}>
          <LinearGradient style={styles.button} colors={['#dfdfdf', '#dfdfdf']}>
            <Text style={styles.buttonNo}>HAPUS</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width,
    height,
    backgroundColor: "#FFF"
  },
  signatureWrapper: {
    height: 400,
    marginHorizontal: 20,
  },
  row: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    alignItems: "center",
  },
  buttonWrapper: {
    width: width - 20,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-around',
    bottom: 0,
  },
  button: {
    height: 65,
    width: 182,
    fontSize: 14,
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonYes: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 18,
    color: 'white',
  },
  buttonNo: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 18,
    color: 'gray',
  },
  textWrapper: {
    paddingVertical: 27,
  },
  headerText: {
    alignSelf: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_next_demibold",
    textAlign: 'center',
    fontSize: 26,
  },
  preview: {
    width: 335,
    height: 114,
    backgroundColor: "#F8F8F8",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 15,
  },
  previewText: {
    color: "#FFF",
    fontSize: 14,
    height: 40,
    lineHeight: 40,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: "#69B2FF",
    width: 120,
    textAlign: "center",
    marginTop: 10,
  },
});