import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity, SafeAreaView, Alert } from 'react-native';
import Svg, { SvgProps, Path } from "react-native-svg"
import BarHeader from '../components/BarHeader';
import { useFonts } from '@use-expo/font';
import { Input, NativeBaseProvider } from 'native-base';
import { Icon } from 'react-native-elements';
import {LinearGradient} from 'expo-linear-gradient';
import AppLoading from 'expo-app-loading';
import { useIsFocused, useNavigation, useRoute } from '@react-navigation/native';
import { baseUrl } from '../config';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { getById } from '../services/api/article';
import ImageView from "react-native-image-viewing";
import moment from 'moment';
const {width, height} = Dimensions.get("window")

export default function IsiArtikel() {
  
  const navigation = useNavigation()
  const route = useRoute()
  const isFocused = useIsFocused()

  const [isLoaded] = useFonts({
    'avenir_next_bold': require('../assets/fonts/avenir_next_bold.ttf'),
    'avenir_next_medium': require('../assets/fonts/avenir_next_medium.ttf'),
  });

  useEffect(() => {
    getImage()
    loadArtikel()
  }, [isFocused])

  const [imageSize, setImageSize] = useState({
    width: 0,
    height: 0,
  })
  const [articleBody, setArticleBody] = useState("")
  const [imageVisibility, setImageVisibility] = useState(false)
  const [article, setArticle] = useState({
    title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed.',
    date: 'May 16, 2021',
    body: `	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ac nisl sagittis, dictum felis id, feugiat erat. Mauris elementum venenatis sem at rutrum. \n
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ac nisl sagittis, dictum felis id, feugiat erat. Mauris elementum venenatis sem at rutrum.\n
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ac nisl sagittis, dictum felis id, feugiat erat. Mauris elementum venenatis sem at rutrum.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ac nisl sagittis, dictum felis id, feugiat erat. Mauris elementum venenatis sem at rutrum\n\n.
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.`
  })

  const loadArtikel = async () => {
    const token = await AsyncStorage.getItem('@token')
    const articleId = route.params.articleId
    const res = await getById(token, articleId)
    if (res.data) {
      res.data.date = moment(res.data.createdAt).format("MMM DD, YYYY")
      setArticle(res.data)
      console.log(res.data, "article")
    } else {
      console.log(res)
      Alert.alert(res)
    }
  }

  const getImage = async () => {
    Image.getSize(baseUrl + "/uploads/news/NEWS-1631868675191.jpg", (width, height) => {
      setImageSize({
        width,
        height
      })
    })
  }
  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return (
        <NativeBaseProvider style={styles.container}>
          <BarHeader header={"ARTIKEL"} />
          <ScrollView>
            <View style={styles.contentWrapper}>
              <Text style={styles.title}>{article.title}</Text>
              <View style={styles.row}>
                <Text style={styles.date}>{article.date} by </Text>
                <Text style={styles.author}>Astra Zeneca</Text>
              </View>
              <TouchableOpacity onPress={() => setImageVisibility(true)}>
                <Image
                    style={{
                      width: "100%",
                      height: undefined,
                      aspectRatio: 0.8,
                      resizeMode: 'contain'
                    }}
                    resizeMode="contain"
                    source={{
                      uri: baseUrl + article.image
                    }}
                  />
              </TouchableOpacity>
                <ImageView
                      images={[{
                        uri: baseUrl + article.image
                      }]}
                      imageIndex={0}
                      visible={imageVisibility}
                      onRequestClose={() => setImageVisibility(false)}
                    />
              <Text style={styles.body}>{article.body}</Text>
            </View>
          </ScrollView>
        </NativeBaseProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width,
    height,
    backgroundColor: "#ffffff",
  },
  contentWrapper: {
    marginHorizontal: 24,
    marginVertical: 30,
  },
  boardImage: {
    height: 436
  },
  row: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  date: {
    fontFamily: "avenir_next_demibold",
    fontSize: 12,
    color: "#3e3e3e"
  },
  body: {
    flex: 1,
    fontSize: 18,
    color: "#000000",
    fontFamily: 'avenir_next_medium',
    textAlign: 'justify',
    marginTop: 25,
  },
  author: {
    fontSize: 12,
    color: "#800152",
    fontFamily: "avenir_next_demibold",
  },
  title: {
    fontFamily: "avenir_next_demibold",
    fontSize: 22,
    color: "#800152",
    marginBottom: 10
  },
});
