import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity } from 'react-native';
import Svg, { SvgProps, Path, G } from "react-native-svg"
import BarHeader from '../components/BarHeader';
import { useFonts } from '@use-expo/font';
import RNPickerSelect from 'react-native-picker-select';
import { Input, NativeBaseProvider, Spinner } from 'native-base';
import { Icon, Overlay } from 'react-native-elements';
import {LinearGradient} from 'expo-linear-gradient';
import AppLoading from 'expo-app-loading';
import { useNavigation } from '@react-navigation/native';
import { background, backgroundColor } from 'styled-system';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { getOne } from '../services/api/testLab';
import * as ImagePicker from 'expo-image-picker';
import ImageView from "react-native-image-viewing";
import { baseUrl } from '../config';
import { upload, uploadResep } from '../services/api/fileUpload';
import { create } from '../services/api/programEvidence';
const {width, height} = Dimensions.get("window")

export default function PapUnggahResepDokter() {

  useEffect(() => {
    getDoctorData()
  }, [])

  const [form, setForm] = useState({
    doctor: '',
    code: '',
    voucher: '',
    laboratorium: '',
    address: ''
  })
  const [doctor, setDoctor] = useState("")
  const [program, setProgram] = useState("")
  const [isImageLoading, setIsImageLoading] = useState(false);
  const [savedImageList, setSavedImageList] = useState([])
  const [pickedImagePath, setPickedImagePath] = useState('');
  const [visibleOverlay, setVisibleOverlay] = useState(false)
  const [imageVisibility, setImageVisibility] = useState(false);
  const [imageUrl, setImageUrl] = useState(null)

  const onChangeSelect = (val) => {
    console.log(val)
  }

  const showImagePicker = async () => {
    try {
      const token = await AsyncStorage.getItem('@token')
      setIsImageLoading(true)
      // Ask the user for the permission to access the media library 
      const permissionResult = await ImagePicker.requestMediaLibraryPermissionsAsync();
  
      if (permissionResult.granted === false) {
        alert("You've refused to allow this appp to access your photos!");
        return;
      }
  
      const result = await ImagePicker.launchImageLibraryAsync();
  
      // Explore the result
      console.log(result);
  
      if (!result.cancelled) {
        setPickedImagePath(result.uri);
        let filename = result.uri.split('/').pop();
    
        let match = /\.(\w+)$/.exec(filename);
        let type = match ? `image/${match[1]}` : `image`;
    
        // Upload the image using the fetch and FormData APIs
        let formData = new FormData();
        // Assume "photo" is the name of the form field the server expects
        formData.append('upload', { uri: result.uri, name: filename, type });
        console.log(result.uri);
        
        const res = await uploadResep(token, formData);
        console.log(res.data, "racse")
        setImageUrl(res.data.data)
        setIsImageLoading(false)
       } else {
        setIsImageLoading(false)

      }
    } catch (e) {
      console.log(e)
    }
  }

  const openCamera = async () => {
    // Ask the user for the permission to access the camera
    try {
      const token = await AsyncStorage.getItem('@token')
      setIsImageLoading(true)
      const permissionResult = await ImagePicker.requestCameraPermissionsAsync();
  
      if (permissionResult.granted === false) {
        alert("You've refused to allow this appp to access your camera!");
        return;
      }
  
      const result = await ImagePicker.launchCameraAsync();
  
      // Explore the result
      console.log(result);
  
      if (!result.cancelled) {
      setPickedImagePath(result.uri);
      let filename = result.uri.split('/').pop();
  
      let match = /\.(\w+)$/.exec(filename);
      let type = match ? `image/${match[1]}` : `image`;
  
      // Upload the image using the fetch and FormData APIs
      let formData = new FormData();
      // Assume "photo" is the name of the form field the server expects
      formData.append('upload', { uri: result.uri, name: filename, type });
      console.log(result.uri);
  
      const res = await uploadResep(token, formData);
      console.log(res.data, "racse")
      setImageUrl(res.data.data)
      setIsImageLoading(false)
      } else {
        setIsImageLoading(false)

      }
    } catch (e) {
      console.log(e)
    }
  }

  const saveReceipt = async () => {
    try {
      const token = await AsyncStorage.getItem('@token')
      const programId = await AsyncStorage.getItem('@UUID_PROGRAM')
      const res = await create(token, {
        programId,
        url: imageUrl
      })
      if (res.data) {
        if (res.data.data.toString() == "5") {
          // TODO
        } else {
          try { 
            await AsyncStorage.setItem("@PROGRAM_STATUS", "receipt")
            navigation.navigate("PapStatusSetujuiDokumen", { dokumen: "done", teslab: "done", doctor: "done", receipt: "done", verify: "" })
          } catch (e) {
            console.log(e)
          }
        }
      } else {
        console.log(res)
        alert(res)
      }
    } catch (e) {
      console.log()
    }
  }

  const getDoctorData = async () => {
    const doctorName = await AsyncStorage.getItem('@PROGRAM_NAMA_DOKTER')
    const programType = await AsyncStorage.getItem('@NAMA_PROGRAM_TYPE')

    setDoctor(doctorName)
    setProgram(programType)
  }

  const navigation = useNavigation()
  const [isLoaded] = useFonts({
    'avenir_next_bold': require('../assets/fonts/avenir_next_bold.ttf'),
    'avenir_next_medium': require('../assets/fonts/avenir_next_medium.ttf'),
  });
  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return (
      <NativeBaseProvider>
        <View style={styles.container}>
          <BarHeader header="TES LABORATORIUM" />
          <View style={styles.textHeader}>
            <Text style={styles.sectionTitle}> RESEP DOKTER</Text>
          </View>
          <View style={styles.contentWrapper}>
            <View style={styles.row}>
              <Text style={styles.itemsLabel}>Nama dokter </Text>
              <Text style={{flex: 1}}>: </Text>
              <Text style={styles.itemsText}>{doctor}</Text>
            </View>
            <View style={styles.row}>
              <Text style={styles.itemsLabel}>Jenis Terapi </Text>
              <Text style={{flex: 1}}>: </Text>
              <Text style={styles.itemsText}>{program}</Text>
            </View>
            <Overlay isVisible={isImageLoading}>
              <View style={{margin: 5}}>
                  <Spinner color="cyan.500" />
              </View>
            </Overlay>
            <View style={styles.column}>
              <Text style={styles.descriptionText}>UNGGAH RESEP DOKTER {"\n"} Pastikan Anda mengunggah foto resep secara lengkap dan jelas</Text>
              {
                imageUrl == null ? (
                  <View style={styles.dashedLayout}>
                    <Svg onPress={() => openCamera()} style={styles.camera} xmlns="http://www.w3.org/2000/svg" width={61} height={52}>
                      <G
                        stroke="#454545"
                        fill="none"
                        fillRule="evenodd"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                      >
                        <Path d="M60 11.572C60 8.497 57.57 6 54.579 6H6.42C3.43 6 1 8.497 1 11.572v33.856C1 48.503 3.43 51 6.421 51H54.58c2.99 0 5.42-2.497 5.42-5.572V11.572zM22.459 1l-3.918 5" />
                        <Path d="M22 1h15.481L41 6M30.5 18C37.399 18 43 23.601 43 30.5S37.399 43 30.5 43 18 37.399 18 30.5 23.601 18 30.5 18zM50 15a3.001 3.001 0 010 6 3.001 3.001 0 010-6z" />
                      </G>
                    </Svg>
                    <Text style={styles.dashedLayoutLabel}> Atau </Text>
                    <Svg onPress={() => showImagePicker()} style={styles.camera} xmlns="http://www.w3.org/2000/svg" width={61} height={52} >
                      <G fill="none" fillRule="evenodd">
                        <Path
                          stroke="#454545"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M53 19.5C53 13.705 48.338 9 42.596 9H11.404C5.662 9 1 13.705 1 19.5v21C1 46.295 5.662 51 11.404 51h31.192C48.338 51 53 46.295 53 40.5v-21z"
                        />
                        <Path
                          stroke="#454545"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M54.232 41c5.608 0 4.716-3.004 4.716-8.752v-20.83c0-5.748-4.553-10.415-10.162-10.415H18.321C12.713 1.003 11 .68 11 6.428"
                        />
                        <Path
                          stroke="#454545"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M4 44.768l13.143-12.644L24.573 49 37.36 25 52 40.795"
                        />
                        <Path
                          fill="#454545"
                          d="M14 17c-2.76 0-5 2.24-5 5s2.24 5 5 5 5-2.24 5-5-2.24-5-5-5zm0 1.034A3.968 3.968 0 0117.966 22c0 2.188-1.777 3.966-3.966 3.966S10.034 24.188 10.034 22A3.968 3.968 0 0114 18.034z"
                        />
                      </G>
                    </Svg>
                  </View>
                ) : (
                  <TouchableOpacity onPress={() => setImageVisibility(true)} style={styles.dashedLayoutView}>
                    <Image
                    style={styles.imagePrescription}
                    source={{
                      uri: baseUrl + imageUrl
                    }}
                    />
                    <ImageView
                      images={[{
                        uri: baseUrl + imageUrl
                      }]}
                      imageIndex={0}
                      visible={imageVisibility}
                      onRequestClose={() => setImageVisibility(false)}
                    />
                  </TouchableOpacity>
                )
              }
            </View>
          </View>
          <View style={styles.buttonWrapper}>
              <TouchableOpacity onPress={() => { saveReceipt() }}>
              <LinearGradient style={styles.buttonLogin} colors={['#F0AB00', '#C76A1E']}>
                <Text style={styles.buttonLoginLabel}>UNGGAH HASIL TES</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </View>
      </NativeBaseProvider>
    );
  }
}

const styles = StyleSheet.create({
  contentWrapper: {
    alignSelf: 'center',
    flexDirection: 'column',
    width: width -30,
    height: 450,
    paddingVertical: 21,
    paddingHorizontal: 17,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.1,
    // background color must be set
  },
  camera: {
    alignSelf: 'center'
  },
  dashedLayoutLabel: {
    color: "#9b9b9b",
    fontSize: 18,
    alignSelf: 'center',
    marginHorizontal: 20,
    fontFamily: "avenir_next_medium"
  },
  dashedLayout: {
    width: width - 80,
    flexDirection: 'row',
    height: 146,
    padding: 10,
    marginTop: 25,
    backgroundColor: '#E8E8E8',
    borderStyle: 'dotted',
    alignSelf: 'center',
    borderWidth: 2,
    borderColor: '#A8A8A8',
    borderRadius: 7,
    justifyContent: 'center',
  },
  dashedLayoutView: {
    width: 203,
    height: 203,
    padding: 10,
    marginTop: 20,
    backgroundColor: '#E8E8E8',
    borderStyle: 'dotted',
    borderWidth: 2,
    borderColor: '#A8A8A8',
    borderRadius: 7,
  },
  imagePrescription: {
    width: "100%",
    height: "100%",
    resizeMode: "stretch"
  },
  textHeader: {
    height: 76,
    backgroundColor: "#FFF",
    width: width -30,
    alignSelf: 'center',
    marginTop: 22,
    alignItems: 'center',
    borderRadius: 5,
    justifyContent: 'center'
  },
  arrowImage: {
    flex: 1,
    width: 40,
    height: 38,
  },
  input: {
    alignSelf: 'center',
    borderRadius: 12,
    borderColor: "#979797",
    marginBottom: 80,
  },
  itemsLabel: {
    flex: 8,
    alignSelf: 'center',
    textAlign: 'center',
    color: "#2F2F2F",
    fontFamily: 'avenir_black',
  },
  itemsText: {
    flex: 14,
    paddingHorizontal: 5,
    color: "#830051",
    fontFamily: 'avenir_heavy'
  },
  row: {
    flexDirection: "row",
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#FFF",
    height: 60,
    width: width - 30,
    alignSelf: 'center',
    borderRadius: 5,
    marginBottom: 1.5
  },
  column: {
    flexDirection: "column",
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#FFF",
    width: width - 30,
    paddingVertical: 20,
    alignSelf: 'center',
    borderRadius: 5,
    marginBottom: 1.5
  },
  descriptionText: {
    alignSelf: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_next_medium",
    textAlign: 'center',
    color: "#2f2f2f",
    fontSize: 12,
  },
  container: {
    width,
    height,
    backgroundColor: "#f1f5f7"
  },
  buttonWrapper: {
    bottom: 0,
    position: 'absolute',
    width: width - 20,
    marginBottom: 20,
    alignSelf: 'center'
  },
  buttonLogin: {
    height: 55,
    width: "100%",
    fontSize: 14,
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
  },
  sectionTitle: {
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_bold",
    letterSpacing: 1,
    fontSize: 30,
  },
  sectionDescription: {
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_next_medium",
    fontSize: 18,
    marginTop: 21,
    fontWeight: "500",
    marginBottom: 35,
    lineHeight: 20,
  },
  bottomText: {
    alignSelf: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_next_demibold",
    textAlign: 'center',
    fontSize: 20,
    bottom: 120,
    position: 'absolute'
  },
  warningIcon: {
    alignSelf: 'center',
    width: 65,
    height: 65,
    position: 'absolute',
    bottom: 180,
  },
  sectionLayout: {
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#3c1053',
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  statusTree: {
    flex: 2,
    alignSelf: 'center',
    marginLeft: 44,
  },
  circleLineGreen: {
    height: 20,
    width: 2,
    alignSelf: 'center',
    backgroundColor: "#C4D600"
  },
  circleLineGray: {
    height: 20,
    width: 2,
    alignSelf: 'center',
    backgroundColor: "#DFDFDF"
  },
  circleLineWhite: {
    height: 20,
    width: 2,
    alignSelf: 'center',
    backgroundColor: "#FFF"
  },
  startTree: {
    alignSelf: 'center',
    flexDirection: 'row',
  },
  treeLabel: {
    textAlign: 'left',
    fontSize: 18,
    alignSelf: 'center',
    color: '#191b26',
    flex: 3,
    fontFamily: "avenir_next_demibold"
  }
});
