import React, { useState } from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import Signature from "react-native-signature-canvas";
import { upload } from '../services/api/fileUpload';
import * as FileSystem from 'expo-file-system';

export const Canvas = () => {
  const [signature, setSign] = useState(null);

  const handleOK = async (signature) => {
    setSign(signature);
    // var decodedImg = decodeBase64Image(signature);
    // console.log(signature)
    const data = await dataURLtoFile(signature)
    // dataURLtoFile(decodedImg)
    let formData = new FormData();
    formData.append('upload', { uri: data, name: "png", type: "image/png" });    
    const res = await upload(formData);
    console.log(res.data)
    console.log(data)
  };

  const dataURLtoFile = async (dataurl) => {
    const base64Code = dataurl.split("data:image/png;base64,")[1];

    const filename = FileSystem.documentDirectory + "some_unique_file_name.png";
    await FileSystem.writeAsStringAsync(filename, base64Code, {
      encoding: FileSystem.EncodingType.Base64,
    });
    return filename
  }

  const handleEmpty = () => {
    console.log("Empty");
  };

  const style = `.m-signature-pad--footer
    .button {
      background-color: red;
      color: #FFF;
    }`;
  return (
    <View style={{ flex: 1 }}>
      <View style={styles.preview}>
        {signature ? (
          <Image
            resizeMode={"contain"}
            style={{ width: 335, height: 114 }}
            source={{ uri: signature }}
          />
        ) : null}
      </View>
      <Signature
        onOK={handleOK}
        onEmpty={handleEmpty}
        descriptionText="Sign"
        clearText="Clear"
        confirmText="Save"
        webStyle={style}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  preview: {
    width: 335,
    height: 114,
    backgroundColor: "#F8F8F8",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 15,
  },
  previewText: {
    color: "#FFF",
    fontSize: 14,
    height: 40,
    lineHeight: 40,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: "#69B2FF",
    width: 120,
    textAlign: "center",
    marginTop: 10,
  },
});