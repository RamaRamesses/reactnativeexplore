import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font'
import { useFonts } from '@use-expo/font';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Svg, { Defs, ClipPath, Path } from "react-native-svg"
import { useNavigation, useRoute } from '@react-navigation/native';
import {LinearGradient} from 'expo-linear-gradient';
import { marginTop, style } from 'styled-system';
import BarHeader from '../components/BarHeader';
import { createSelfTest } from '../services/api/testLab';
const {width, height} = Dimensions.get("window")

export default function PilihTesLaboratorium ({ type }) {
  const navigation = useNavigation();
  const route = useRoute()

  const saveTestLab = async (tes_lab_name, tes_lab_id) => {
    try {
      await AsyncStorage.setItem("@TES_LAB_NAME", tes_lab_name)
      await AsyncStorage.setItem("@UUID_TES_LAB_TYPE", tes_lab_id)
      await AsyncStorage.removeItem('@PROGRAM_STATUS')
      if (route.params){
        if (route.params.type === "regular"){
          navigation.navigate("TestLabStatusPilihLaboratorium")
        } else {
          postTestLab()
        }
      } else {
        navigation.navigate("TestLabStatusPilihLaboratorium")
      }
    } catch (err) {
      console.log(err)
    }
  }

  const postTestLab = async () => {
    try {
      const patientId = await AsyncStorage.getItem('@UUID_PATIENT')
      const testLabTypeId = await AsyncStorage.getItem('@UUID_TES_LAB_TYPE')
      const res = await createSelfTest({ patientId, testLabTypeId })
      if (res.data) {
        console.log(res.data)
        await AsyncStorage.setItem("@TES_LAB_ID", res.data.id)
        navigation.navigate("TestLabStatusUnggahHasilTes", { type: 'self' })
      } else {
        console.log(res)
      }
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <>
      <BarHeader header="TES LABORATORIUM" />
      <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
        <View style={styles.layout}>
          <View style={styles.row}>
            <View style={styles.imageWrapper}>
            <Svg style={styles.boardImage} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 113 100">
              <Defs>
                <ClipPath id="prefix__a">
                  <Path d="M-16-21h137v137H-16z" />
                </ClipPath>
                <ClipPath id="prefix__b">
                  <Path d="M-16-21h137v137H-16z" />
                </ClipPath>
                <ClipPath id="prefix__d">
                  <Path d="M-16-21h137v137H-16z" />
                </ClipPath>
                <ClipPath id="prefix__e">
                  <Path d="M-16-21h137v137H-16z" />
                </ClipPath>
                <ClipPath id="prefix__f">
                  <Path d="M-16-21h137v137H-16z" />
                </ClipPath>
                <ClipPath id="prefix__g">
                  <Path d="M-16-21h137v137H-16z" />
                </ClipPath>
                <ClipPath id="prefix__h">
                  <Path d="M-16-21h137v137H-16z" />
                </ClipPath>
                <ClipPath id="prefix__i">
                  <Path d="M-16-21h137v137H-16z" />
                </ClipPath>
                <ClipPath id="prefix__j">
                  <Path d="M-16-21h137v137H-16z" />
                </ClipPath>
                <ClipPath id="prefix__k">
                  <Path d="M-16-21h137v137H-16z" />
                </ClipPath>
                <ClipPath id="prefix__l">
                  <Path d="M-16-21h137v137H-16z" />
                </ClipPath>
                <ClipPath id="prefix__c">
                  <Path d="M-16 45.612a65.128 65.128 0 0144.957-62.126 65.13 65.13 0 0140.325 0 65.128 65.128 0 0144.957 62.126 65.128 65.128 0 01-44.957 62.126 65.13 65.13 0 01-40.325 0A65.128 65.128 0 01-16 45.612" />
                </ClipPath>
              </Defs>
              <Path
                clipPath="url(#prefix__a)"
                d="M9.907 34.901s.98-2.192 6.083-4.151c5.444-2.091 4.461-12.549-4.524-12.801-8.984-.253-14.145 13.702-9.846 17.855 3.145 3.038 6.857 2.236 8.287-.903h0z"
                fill="#fa9776"
                stroke="#00000000"
                fillRule="evenodd"
              />
              <Path
                clipPath="url(#prefix__d)"
                d="M42.923 14.965c-8.183 0-14.816 7.855-14.816 17.541v34.765c0 9.686 6.633 17.541 14.816 17.541h40.211c8.183 0 14.82-7.855 14.82-17.541V32.506c0-9.686-6.637-17.541-14.82-17.541H42.923z"
                fill="#830051"
                stroke="#00000000"
              />
              <Path
                clipPath="url(#prefix__e)"
                d="M67.474 23.293a.979.979 0 00-.978-.979h-6.339a.98.98 0 00-.978.979v1.956c0 .541.439.979.978.979h6.339c.54 0 .978-.438.978-.979v-1.956z"
                fill="#e1ecf6"
                stroke="#00000000"
                fillRule="evenodd"
              />
              <Path
                clipPath="url(#prefix__f)"
                d="M67.474 30.969a.979.979 0 00-.978-.978h-6.339a.98.98 0 00-.978.978v1.957c0 .54.439.978.978.978h6.339c.54 0 .978-.438.978-.978v-1.957z"
                fill="#e1ecf6"
                stroke="#00000000"
                fillRule="evenodd"
              />
              <Path
                clipPath="url(#prefix__g)"
                d="M67.474 38.682a.978.978 0 00-.978-.977h-6.339a.979.979 0 00-.978.977v1.957c0 .541.439.978.978.978h6.339c.54 0 .978-.437.978-.978v-1.957z"
                fill="#e1ecf6"
                stroke="#00000000"
                fillRule="evenodd"
              />
              <Path
                clipPath="url(#prefix__h)"
                d="M56.644 36.685c0-2.614.3-4.738-2.286-4.738 0 0-15.796.749-15.796 19.525v5.813c0 2.616 2.099 4.738 4.684 4.738l8.712-2.502c3.003-1.309 4.686-2.123 4.686-4.738V36.685h0z"
                fill="#e1ecf6"
                stroke="#00000000"
                fillRule="evenodd"
              />
              <Path
                clipPath="url(#prefix__i)"
                d="M70.088 36.685c0-2.614-.3-4.738 2.285-4.738 0 0 15.797.749 15.797 19.525v5.813c0 2.616-2.1 4.738-4.685 4.738l-8.712-2.502c-3.002-1.309-4.685-2.123-4.685-4.738V36.685h0z"
                fill="#e1ecf6"
                stroke="#00000000"
                fillRule="evenodd"
              />
              <Path
                clipPath="url(#prefix__j)"
                d="M93.608 5.023a4.969 4.969 0 018.479-3.512 4.969 4.969 0 010 7.024 4.969 4.969 0 01-7.024 0 4.969 4.969 0 01-1.455-3.512"
                fill="#fa9776"
                stroke="#00000000"
                fillRule="evenodd"
              />
              <Path
                clipPath="url(#prefix__k)"
                d="M12.785 77.737a4.973 4.973 0 013.429-4.739 4.974 4.974 0 013.076 0 4.973 4.973 0 013.429 4.739 4.969 4.969 0 01-8.479 3.512 4.969 4.969 0 01-1.455-3.512"
                fill="#fff"
                stroke="#00000000"
                fillRule="evenodd"
              />
              <Path
                clipPath="url(#prefix__l)"
                d="M40.466 70.254h13.413c.292 0 .56-.171.692-.441l3.545-7.338a.772.772 0 01.763-.449c.32.029.589.258.678.577l3.459 11.92a.778.778 0 00.692.553.772.772 0 00.749-.468l3.23-7.798a.778.778 0 01.639-.481.768.768 0 01.713.35l2.296 3.564H88.17"
                fill="#00000000"
                stroke="#fff"
                strokeWidth={1.762}
                fillRule="evenodd"
              />
            </Svg>
            </View>
            <View style={styles.labelsLayout}>
              <Text style={styles.cardTitle}>TES MUTASI EGFR </Text>
              <Text style={styles.cardDescription}>Tes yang dilakukan setelah anda terdiagnosa kanker paru. </Text>
            </View>
          </View>
          <View style={styles.buttonWrapper}>
              <TouchableOpacity onPress={() => saveTestLab("TES MUTASI EGFR", "be3e4481-bdee-4ad2-ac88-70cee6d10f05")}>
              <LinearGradient style={styles.buttonLogin} colors={['#F0AB00', '#C76A1E']}>
                <Text style={styles.buttonLoginLabel}>AJUKAN TES LABORATORIUM</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.layout}>
          <View style={styles.row}>
            <View style={styles.imageWrapper}>
            <Svg style={styles.boardImage} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 113 100">
              <Defs>
                <ClipPath id="prefix__a">
                  <Path d="M-16-21h137v137H-16z" />
                </ClipPath>
                <ClipPath id="prefix__b">
                  <Path d="M-16-21h137v137H-16z" />
                </ClipPath>
                <ClipPath id="prefix__d">
                  <Path d="M-16-21h137v137H-16z" />
                </ClipPath>
                <ClipPath id="prefix__e">
                  <Path d="M-16-21h137v137H-16z" />
                </ClipPath>
                <ClipPath id="prefix__f">
                  <Path d="M-16-21h137v137H-16z" />
                </ClipPath>
                <ClipPath id="prefix__g">
                  <Path d="M-16-21h137v137H-16z" />
                </ClipPath>
                <ClipPath id="prefix__h">
                  <Path d="M-16-21h137v137H-16z" />
                </ClipPath>
                <ClipPath id="prefix__i">
                  <Path d="M-16-21h137v137H-16z" />
                </ClipPath>
                <ClipPath id="prefix__j">
                  <Path d="M-16-21h137v137H-16z" />
                </ClipPath>
                <ClipPath id="prefix__k">
                  <Path d="M-16-21h137v137H-16z" />
                </ClipPath>
                <ClipPath id="prefix__l">
                  <Path d="M-16-21h137v137H-16z" />
                </ClipPath>
                <ClipPath id="prefix__c">
                  <Path d="M-16 45.612a65.128 65.128 0 0144.957-62.126 65.13 65.13 0 0140.325 0 65.128 65.128 0 0144.957 62.126 65.128 65.128 0 01-44.957 62.126 65.13 65.13 0 01-40.325 0A65.128 65.128 0 01-16 45.612" />
                </ClipPath>
              </Defs>
              <Path
                clipPath="url(#prefix__a)"
                d="M9.907 34.901s.98-2.192 6.083-4.151c5.444-2.091 4.461-12.549-4.524-12.801-8.984-.253-14.145 13.702-9.846 17.855 3.145 3.038 6.857 2.236 8.287-.903h0z"
                fill="#fa9776"
                stroke="#00000000"
                fillRule="evenodd"
              />
              <Path
                clipPath="url(#prefix__d)"
                d="M42.923 14.965c-8.183 0-14.816 7.855-14.816 17.541v34.765c0 9.686 6.633 17.541 14.816 17.541h40.211c8.183 0 14.82-7.855 14.82-17.541V32.506c0-9.686-6.637-17.541-14.82-17.541H42.923z"
                fill="#830051"
                stroke="#00000000"
              />
              <Path
                clipPath="url(#prefix__e)"
                d="M67.474 23.293a.979.979 0 00-.978-.979h-6.339a.98.98 0 00-.978.979v1.956c0 .541.439.979.978.979h6.339c.54 0 .978-.438.978-.979v-1.956z"
                fill="#e1ecf6"
                stroke="#00000000"
                fillRule="evenodd"
              />
              <Path
                clipPath="url(#prefix__f)"
                d="M67.474 30.969a.979.979 0 00-.978-.978h-6.339a.98.98 0 00-.978.978v1.957c0 .54.439.978.978.978h6.339c.54 0 .978-.438.978-.978v-1.957z"
                fill="#e1ecf6"
                stroke="#00000000"
                fillRule="evenodd"
              />
              <Path
                clipPath="url(#prefix__g)"
                d="M67.474 38.682a.978.978 0 00-.978-.977h-6.339a.979.979 0 00-.978.977v1.957c0 .541.439.978.978.978h6.339c.54 0 .978-.437.978-.978v-1.957z"
                fill="#e1ecf6"
                stroke="#00000000"
                fillRule="evenodd"
              />
              <Path
                clipPath="url(#prefix__h)"
                d="M56.644 36.685c0-2.614.3-4.738-2.286-4.738 0 0-15.796.749-15.796 19.525v5.813c0 2.616 2.099 4.738 4.684 4.738l8.712-2.502c3.003-1.309 4.686-2.123 4.686-4.738V36.685h0z"
                fill="#e1ecf6"
                stroke="#00000000"
                fillRule="evenodd"
              />
              <Path
                clipPath="url(#prefix__i)"
                d="M70.088 36.685c0-2.614-.3-4.738 2.285-4.738 0 0 15.797.749 15.797 19.525v5.813c0 2.616-2.1 4.738-4.685 4.738l-8.712-2.502c-3.002-1.309-4.685-2.123-4.685-4.738V36.685h0z"
                fill="#e1ecf6"
                stroke="#00000000"
                fillRule="evenodd"
              />
              <Path
                clipPath="url(#prefix__j)"
                d="M93.608 5.023a4.969 4.969 0 018.479-3.512 4.969 4.969 0 010 7.024 4.969 4.969 0 01-7.024 0 4.969 4.969 0 01-1.455-3.512"
                fill="#fa9776"
                stroke="#00000000"
                fillRule="evenodd"
              />
              <Path
                clipPath="url(#prefix__k)"
                d="M12.785 77.737a4.973 4.973 0 013.429-4.739 4.974 4.974 0 013.076 0 4.973 4.973 0 013.429 4.739 4.969 4.969 0 01-8.479 3.512 4.969 4.969 0 01-1.455-3.512"
                fill="#fff"
                stroke="#00000000"
                fillRule="evenodd"
              />
              <Path
                clipPath="url(#prefix__l)"
                d="M40.466 70.254h13.413c.292 0 .56-.171.692-.441l3.545-7.338a.772.772 0 01.763-.449c.32.029.589.258.678.577l3.459 11.92a.778.778 0 00.692.553.772.772 0 00.749-.468l3.23-7.798a.778.778 0 01.639-.481.768.768 0 01.713.35l2.296 3.564H88.17"
                fill="#00000000"
                stroke="#fff"
                strokeWidth={1.762}
                fillRule="evenodd"
              />
            </Svg>
            </View>
            <View style={styles.labelsLayout}>
              <Text style={styles.cardTitle}>TES T790M </Text>
              <Text style={styles.cardDescription}>Tes yang dilakukan setelah anda menjalani terapi kanker paru dan mengalami perburukan penyakit </Text>
            </View>
          </View>
          <View style={styles.buttonWrapper}>
              <TouchableOpacity onPress={() => saveTestLab("TES T790M", "f964e1fe-66f2-440f-8760-8cc2c116f986")}>
              <LinearGradient style={styles.buttonLogin} colors={['#F0AB00', '#C76A1E']}>
                <Text style={styles.buttonLoginLabel}>AJUKAN TES LABORATORIUM</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.layout}>
          <View style={styles.row}>
          <View style={styles.imageWrapper}>
            <Svg style={styles.boardImage} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 109 95" >
      <Defs>
        <ClipPath id="prefix__a">
          <Path d="M-15-20h131v131H-15z" />
        </ClipPath>
        <ClipPath id="prefix__b">
          <Path d="M-15-20h131v131H-15z" />
        </ClipPath>
        <ClipPath id="prefix__d">
          <Path d="M-15-20h131v131H-15z" />
        </ClipPath>
        <ClipPath id="prefix__e">
          <Path d="M-15-20h131v131H-15z" />
        </ClipPath>
        <ClipPath id="prefix__f">
          <Path d="M-15-20h131v131H-15z" />
        </ClipPath>
        <ClipPath id="prefix__g">
          <Path d="M-15-20h131v131H-15z" />
        </ClipPath>
        <ClipPath id="prefix__h">
          <Path d="M-15-20h131v131H-15z" />
        </ClipPath>
        <ClipPath id="prefix__i">
          <Path d="M-15-20h131v131H-15z" />
        </ClipPath>
        <ClipPath id="prefix__j">
          <Path d="M-15-20h131v131H-15z" />
        </ClipPath>
        <ClipPath id="prefix__c">
          <Path d="M-15 43.695A62.278 62.278 0 01-3.178 6.994 62.28 62.28 0 0127.988-15.71a62.28 62.28 0 0138.559 0A62.28 62.28 0 0197.713 6.994a62.278 62.278 0 0111.822 36.701 62.278 62.278 0 01-11.822 36.701A62.28 62.28 0 0166.547 103.1a62.28 62.28 0 01-38.559 0A62.28 62.28 0 01-3.178 80.396 62.278 62.278 0 01-15 43.695" />
        </ClipPath>
      </Defs>
      <Path
        clipPath="url(#prefix__a)"
        d="M9.772 33.453s.937-2.096 5.816-3.969c5.206-2 4.267-12-4.325-12.241-8.59-.242-13.526 13.102-9.414 17.074 3.007 2.904 6.556 2.137 7.923-.864h0z"
        fill="#fa9776"
        stroke="#00000000"
        fillRule="evenodd"
      />
      <Path
        clipPath="url(#prefix__d)"
        d="M41.342 14.39c-7.825 0-14.167 7.511-14.167 16.773v33.242c0 9.262 6.342 16.773 14.167 16.773h38.451c7.824 0 14.171-7.511 14.171-16.773V31.163c0-9.262-6.347-16.773-14.171-16.773H41.342z"
        fill="#830051"
        stroke="#00000000"
      />
      <Path
        clipPath="url(#prefix__e)"
        d="M89.808 4.883a4.75 4.75 0 018.597-2.799 4.75 4.75 0 01.902 2.799 4.75 4.75 0 01-8.597 2.799 4.75 4.75 0 01-.902-2.799"
        fill="#fa9776"
        stroke="#00000000"
        fillRule="evenodd"
      />
      <Path
        clipPath="url(#prefix__f)"
        d="M12.524000000000001 74.413a4.75 4.75 0 018.597-2.799 4.75 4.75 0 01.902 2.799 4.75 4.75 0 01-8.597 2.799 4.75 4.75 0 01-.902-2.799"
        fill="#fff"
        stroke="#00000000"
        fillRule="evenodd"
      />
      <Path
        clipPath="url(#prefix__g)"
        d="M52.252 31.721s1.886-7.929 6.545-8.571c4.661-.641 8.484 1.067 9.885 7.087l-1.752.214s-5.394-6.452-14.678 1.27z"
        fill="#fff"
        stroke="#00000000"
        fillRule="evenodd"
      />
      <Path
        clipPath="url(#prefix__h)"
        d="M42.665 55.478s7.71-4.107 14.516-13.518c6.806-9.41 9.395-12.62 9.138-16.645 0 0 4.128 5.093 2.118 9.399-2.01 4.306-7.412 16.648-18.675 27.374l-1.329-4.942-5.768-1.668h0z"
        fill="#fff"
        stroke="#00000000"
        fillRule="evenodd"
      />
      <Path
        clipPath="url(#prefix__i)"
        d="M78.473 56.401s-7.709-4.107-14.515-13.518c-6.806-9.41-9.396-12.62-9.139-16.645 0 0-4.129 5.093-2.118 9.399 2.01 4.306 7.411 16.648 18.674 27.374l1.331-4.942 5.767-1.668h0z"
        fill="#fff"
        stroke="#00000000"
        fillRule="evenodd"
      />
      <Path
        clipPath="url(#prefix__j)"
        d="M38.993 69.957h12.826a.736.736 0 00.661-.422l3.39-7.015a.735.735 0 01.73-.429.746.746 0 01.648.551l3.308 11.398a.742.742 0 00.661.529.738.738 0 00.716-.448l3.089-7.457a.743.743 0 01.611-.459.733.733 0 01.682.335l2.195 3.407h16.097"
        fill="#00000000"
        stroke="#fff"
        strokeWidth={1.762}
        fillRule="evenodd"
      />
    </Svg>
            </View>
            <View style={styles.labelsLayout}>
              <Text style={styles.cardTitle}>BRCA TUMOR</Text>
              <Text style={styles.cardDescription}>Test yang dilakukan setelah anda terdiagnosa kanker ovarium </Text>
            </View>
          </View>
          <View style={styles.buttonWrapper}>
              <TouchableOpacity onPress={() => saveTestLab("TES T-BRCA", "f957b371-2eeb-42b6-9804-5dbe691a5ad5")}>
              <LinearGradient style={styles.buttonLogin} colors={['#F0AB00', '#C76A1E']}>
                <Text style={styles.buttonLoginLabel}>AJUKAN TES LABORATORIUM</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.layout}>
          <View style={styles.row}>
            <View style={styles.imageWrapper}>
            <Svg style={styles.boardImage} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 109 95" >
      <Defs>
        <ClipPath id="prefix__a">
          <Path d="M-15-20h131v131H-15z" />
        </ClipPath>
        <ClipPath id="prefix__b">
          <Path d="M-15-20h131v131H-15z" />
        </ClipPath>
        <ClipPath id="prefix__d">
          <Path d="M-15-20h131v131H-15z" />
        </ClipPath>
        <ClipPath id="prefix__e">
          <Path d="M-15-20h131v131H-15z" />
        </ClipPath>
        <ClipPath id="prefix__f">
          <Path d="M-15-20h131v131H-15z" />
        </ClipPath>
        <ClipPath id="prefix__g">
          <Path d="M-15-20h131v131H-15z" />
        </ClipPath>
        <ClipPath id="prefix__h">
          <Path d="M-15-20h131v131H-15z" />
        </ClipPath>
        <ClipPath id="prefix__i">
          <Path d="M-15-20h131v131H-15z" />
        </ClipPath>
        <ClipPath id="prefix__j">
          <Path d="M-15-20h131v131H-15z" />
        </ClipPath>
        <ClipPath id="prefix__c">
          <Path d="M-15 43.695A62.278 62.278 0 01-3.178 6.994 62.28 62.28 0 0127.988-15.71a62.28 62.28 0 0138.559 0A62.28 62.28 0 0197.713 6.994a62.278 62.278 0 0111.822 36.701 62.278 62.278 0 01-11.822 36.701A62.28 62.28 0 0166.547 103.1a62.28 62.28 0 01-38.559 0A62.28 62.28 0 01-3.178 80.396 62.278 62.278 0 01-15 43.695" />
        </ClipPath>
      </Defs>
      <Path
        clipPath="url(#prefix__a)"
        d="M9.772 33.453s.937-2.096 5.816-3.969c5.206-2 4.267-12-4.325-12.241-8.59-.242-13.526 13.102-9.414 17.074 3.007 2.904 6.556 2.137 7.923-.864h0z"
        fill="#fa9776"
        stroke="#00000000"
        fillRule="evenodd"
      />
      <Path
        clipPath="url(#prefix__d)"
        d="M41.342 14.39c-7.825 0-14.167 7.511-14.167 16.773v33.242c0 9.262 6.342 16.773 14.167 16.773h38.451c7.824 0 14.171-7.511 14.171-16.773V31.163c0-9.262-6.347-16.773-14.171-16.773H41.342z"
        fill="#830051"
        stroke="#00000000"
      />
      <Path
        clipPath="url(#prefix__e)"
        d="M89.808 4.883a4.75 4.75 0 018.597-2.799 4.75 4.75 0 01.902 2.799 4.75 4.75 0 01-8.597 2.799 4.75 4.75 0 01-.902-2.799"
        fill="#fa9776"
        stroke="#00000000"
        fillRule="evenodd"
      />
      <Path
        clipPath="url(#prefix__f)"
        d="M12.524000000000001 74.413a4.75 4.75 0 018.597-2.799 4.75 4.75 0 01.902 2.799 4.75 4.75 0 01-8.597 2.799 4.75 4.75 0 01-.902-2.799"
        fill="#fff"
        stroke="#00000000"
        fillRule="evenodd"
      />
      <Path
        clipPath="url(#prefix__g)"
        d="M52.252 31.721s1.886-7.929 6.545-8.571c4.661-.641 8.484 1.067 9.885 7.087l-1.752.214s-5.394-6.452-14.678 1.27z"
        fill="#fff"
        stroke="#00000000"
        fillRule="evenodd"
      />
      <Path
        clipPath="url(#prefix__h)"
        d="M42.665 55.478s7.71-4.107 14.516-13.518c6.806-9.41 9.395-12.62 9.138-16.645 0 0 4.128 5.093 2.118 9.399-2.01 4.306-7.412 16.648-18.675 27.374l-1.329-4.942-5.768-1.668h0z"
        fill="#fff"
        stroke="#00000000"
        fillRule="evenodd"
      />
      <Path
        clipPath="url(#prefix__i)"
        d="M78.473 56.401s-7.709-4.107-14.515-13.518c-6.806-9.41-9.396-12.62-9.139-16.645 0 0-4.129 5.093-2.118 9.399 2.01 4.306 7.411 16.648 18.674 27.374l1.331-4.942 5.767-1.668h0z"
        fill="#fff"
        stroke="#00000000"
        fillRule="evenodd"
      />
      <Path
        clipPath="url(#prefix__j)"
        d="M38.993 69.957h12.826a.736.736 0 00.661-.422l3.39-7.015a.735.735 0 01.73-.429.746.746 0 01.648.551l3.308 11.398a.742.742 0 00.661.529.738.738 0 00.716-.448l3.089-7.457a.743.743 0 01.611-.459.733.733 0 01.682.335l2.195 3.407h16.097"
        fill="#00000000"
        stroke="#fff"
        strokeWidth={1.762}
        fillRule="evenodd"
      />
    </Svg>
            </View>
            <View style={styles.labelsLayout}>
              <Text style={styles.cardTitle}>BRCA DARAH</Text>
              <Text style={styles.cardDescription}>Test yang dilakukan setelah anda terdiagnosa kanker payudara atau ovarium </Text>
            </View>
          </View>
          <View style={styles.buttonWrapper}>
              <TouchableOpacity onPress={() => saveTestLab("TES Z-BRCA", "a85f8986-f4f5-4b22-9952-10e9e4a3d307")}>
              <LinearGradient style={styles.buttonLogin} colors={['#F0AB00', '#C76A1E']}>
                <Text style={styles.buttonLoginLabel}>AJUKAN TES LABORATORIUM</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f1f5f7'
  },
  layout: {
    marginHorizontal: 20,
    flexDirection: 'column',
    marginTop: 10,
    backgroundColor: 'white',
    marginBottom: 20,
    borderRadius: 15,
    padding: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
  },
  buttonWrapper: {
    width: "100%",
  },
  buttonLogin: {
    height: 50,
    fontSize: 14,
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
  },
  row: {
    alignSelf: 'center',
    marginVertical: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    // background color must be set
  },
  labelsLayout: {
    flex: 3,
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  cardTitle: {
    fontFamily: 'avenir_bold',
    fontSize: 20,
    color: "black",
    flex: 1,
  },
  cardDescription: {
    fontFamily: 'avenir_next_demibold',
    color: "gray",
    lineHeight: 20,
    marginTop: 7,
    fontSize: 12,
    flex: 2,
  },
  imageWrapper: {
    marginVertical: 0,
    marginLeft: 11,
    flex: 2
  },
  boardImage: {
    width: 130,
    height: 110,
  },
});
