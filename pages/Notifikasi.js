import React, {useState, useEffect} from 'react';
import AppLoading from 'expo-app-loading';
import { useFonts } from '@use-expo/font';
import { StyleSheet, SafeAreaView, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity, CheckBox, PickerIOSBase } from 'react-native';
import { Checkbox, Center, NativeBaseProvider } from "native-base"
import {LinearGradient} from 'expo-linear-gradient';
import { Input, Icon, Overlay } from 'react-native-elements';
const {width, height} = Dimensions.get("window")
import { Appbar as NativeAppBar } from 'react-native-paper';
import { getAll as getAllCity } from '../services/api/city'
import { register } from '../services/api/auth'
import { useNavigation, useIsFocused } from '@react-navigation/native';
import RNPickerSelect from 'react-native-picker-select';
import { useDispatch, useSelector } from 'react-redux';
import { checkTestLabCheckPoint } from '../helpers';
import { getById } from '../services/api/notification';
import BarHeader from '../components/BarHeader';
import AsyncStorage from '@react-native-async-storage/async-storage';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import { baseUrl } from '../config';
import ImageView from "react-native-image-viewing";
import BarFooter from '../components/BarFooter';

export default function Notifikasi() {
  const navigation = useNavigation();
  const isFocused = useIsFocused();

  const [isLoaded] = useFonts({
    'avenir_next_bold': require('../assets/fonts/avenir_next_bold.ttf'),
    'avenir_next_medium': require('../assets/fonts/avenir_next_medium.ttf'),
    'avenir_heavy': require('../assets/fonts/avenir_heavy.ttf'),
  });

  const [password, setPassword] = useState(true);
  const [notifications, setNotifications] = useState([]);

  useEffect(() => {
    loadNotification()
  }, [isFocused])

  const [user, setUser] = useState({
    status: 1,
    email: '',
    password: '',
    city: '',
    idNumber: '',
    fullname: '',
    gender: '',
    code: '',
    dateOfBirth: ''
  })

  const loadNotification = async () => {
    try {
      const token = await AsyncStorage.getItem('@token')
      const patientId = await AsyncStorage.getItem('@UUID_PATIENT')
      const res = await getById(token, patientId)
      if (res.data) {
        console.log(res.data, "notification")
        setNotifications(res.data)
      } else {
        console.log(res)
        alert(res)
      }
    } catch (e) {
      console.log(e)
    }
  }

  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return (
      <NativeBaseProvider>
      <BarHeader header="NOTIFIKASI" />
        <ScrollView style={{backgroundColor: "#f1f5f7"}}>
          <View style={styles.container}>
            <Text style={styles.sectionText}>Daftar Notifikasi</Text>
            <View style={styles.contentWrapper}>
              {
                notifications.map((notification, i) => (
                  <View style={styles.row} key={i}>
                    <Text style={styles.notificationTitle}>{notification.message}</Text>
                    <Text style={styles.notificationDate}>{moment(notification.createdAt).format("YYYY-MM-DD HH:mm:ss")}</Text>
                  </View>
                ))
              }
              {/* <View style={styles.row}>
                <Text style={styles.notificationTitle}>Pengajuan PAP dibuat</Text>
                <Text style={styles.notificationDate}>12:00 {"\n"} 5 Jan 2021</Text>
              </View> */}
            </View>
          </View>
        </ScrollView>
        <BarFooter activeIcon="notifikasi" />
      </NativeBaseProvider>
    );
  }
}

const styles = StyleSheet.create({
  sectionText: {
    textAlign: 'left',
    alignSelf: 'flex-start',
    fontSize: 28,
    fontWeight: "500",
    letterSpacing: 0,
    fontFamily: 'avenir_next_medium',
    marginVertical: 26,
  },
  contentWrapper: {
    flexDirection: 'column',
    borderRadius: 8,
    width: "100%",
    padding: 0,
    backgroundColor: "#f1f5f7"
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 20,
    marginBottom: 2,
    paddingHorizontal: 15,
    backgroundColor: "#FFF",
    flex: 1,
  },
  notificationTitle: {
    alignSelf: 'center',
    fontFamily: "avenir_heavy",
    color: "#6D7278",
    fontSize: 16,
    flex: 2,
    fontWeight: "900",
    letterSpacing: 0,
  },
  notificationDate: {
    textAlign: 'center',
    fontFamily: "avenir_heavy",
    color: "#6D7278",
    fontSize: 16,
    flex: 1,
    fontWeight: "900",
    letterSpacing: 0,
  },
  container: {
    alignItems: 'center',
    marginHorizontal: 20,
  },
});
