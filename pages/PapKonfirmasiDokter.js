import React, { useRef, useState } from "react";
import { StyleSheet, Text, View, Image, Dimensions, Button, TouchableOpacity } from "react-native";
import Signature from "react-native-signature-canvas";
import { upload, uploadSignature } from '../services/api/fileUpload';
import * as FileSystem from 'expo-file-system';
import BarHeader from '../components/BarHeader';
import {LinearGradient} from 'expo-linear-gradient';
import { degrees, PDFDocument, rgb, StandardFonts } from 'pdf-lib';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input'
import { position } from 'styled-system';
import { confirm } from '../services/api/program';
const {width, height} = Dimensions.get("window")

export const PapKonfirmasiDokter = () => {
  const ref = useRef();
  const navigation = useNavigation();
  const [signature, setSign] = useState(null);
  const [code, setCode] = useState("");

  const submitCode = async () => {
    console.log(code)
    const token = await AsyncStorage.getItem('@token')
    const programId = await AsyncStorage.getItem('@UUID_PROGRAM')

    if (token != null) {
      const res = await confirm(token, {
        id: programId,
        confirmationCode: code
      })
      if (res.data) {
        try { 
          await AsyncStorage.setItem("@PROGRAM_STATUS", "doctor")
        } catch (e) {
          console.log(e)
        }
        navigation.navigate("PapStatusSetujuiDokumen", { dokumen: "done", teslab: "done", doctor: "done", receipt: "", verify: "" })
      } else {
        console.log(res)
        alert(res)
      }
    }

  }

  const style = `.m-signature-pad--footer
  {
    display: none;
    margin: 0px;
    height: 200px;
  }
  body,html {
    width: 100%; height: 250px;}`;
  return (
    <View style={styles.container}>
      <BarHeader header="PROGRAM PULIH" />
      <View style={styles.contentWrapper}>
        <View style={styles.textWrapper}>
          <Text style={styles.headerText}> Masukan Kode {"\n"} Verifikasi </Text>
        </View>
        <View style={styles.textWrapper}>
          <Text style={styles.descriptionText}> Silahkan masukan kode verifikasi yang telah {"\n"} dokter informasikan.</Text>
        </View>
      </View>
      {/* <View style={styles.preview}>
        {signature ? (
          <Image
            resizeMode={"contain"}
            style={{ width: 335, height: 114 }}
            source={{ uri: signature }}
          />
        ) : null}
      </View> */}
      <View style={styles.signatureWrapper}>
        <SmoothPinCodeInput
          cellSize={36}
          codeLength={8}
          keyboardType="email-address"
          codeLength={6}
          cellSize={60}
          value={code}
          onTextChange={code => setCode(code)}
        />
      </View>
      <View style={styles.buttonWrapper}>
          <TouchableOpacity onPress={() => submitCode()}>
          <LinearGradient style={styles.buttonYellow} colors={['#F0AB00', '#C76A1E']}>
            <Text style={styles.buttonLoginLabel}>LANJUTKAN PROSES</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width,
    height,
    backgroundColor: "#FFF"
  },
  signatureWrapper: {
    marginHorizontal: 20,
    marginVertical: 40,
    alignSelf: 'center'
  },
  row: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    alignItems: "center",
  },
  buttonWrapper: {
    bottom: 10,
    width: width - 20,
    alignSelf: 'center',
    position: 'absolute'
  },
  buttonYellow: {
    height: 55,
    width: "100%",
    fontSize: 14,
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
  },
  textWrapper: {
    paddingTop: 37,
  },
  headerText: {
    alignSelf: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_next_bold",
    textAlign: 'center',
    fontSize: 36,
    color: "#3c1053"
  },
  descriptionText: {
    alignSelf: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_next_medium",
    textAlign: 'center',
    color: "#2f2f2f",
    fontSize: 18,
  },
  preview: {
    width: 335,
    height: 114,
    backgroundColor: "#F8F8F8",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 15,
  },
  previewText: {
    color: "#FFF",
    fontSize: 14,
    height: 40,
    lineHeight: 40,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: "#69B2FF",
    width: 120,
    textAlign: "center",
    marginTop: 10,
  },
});