import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity } from 'react-native';
import Svg, { SvgProps, Path } from "react-native-svg"
import BarHeader from '../components/BarHeader';
import { useFonts } from '@use-expo/font';
import RNPickerSelect from 'react-native-picker-select';
import { Input, NativeBaseProvider, useToast } from 'native-base';
import { Icon } from 'react-native-elements';
import {LinearGradient} from 'expo-linear-gradient';
import AppLoading from 'expo-app-loading';
import { useNavigation } from '@react-navigation/native';
import { getAll } from '../services/api/doctor';
import { getAll as getLabs } from '../services/api/laboratorium';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { createTest } from '../services/api/testLab';
import { AutocompleteDropdown } from 'react-native-autocomplete-dropdown'
const {width, height} = Dimensions.get("window")

export default function TestLabKodeRujukan() {

  const [doctors, setDoctors] = useState([])
  const [doctorsRaw, setDoctorsRaw] = useState([])
  const [labs, setLabs] = useState([])
  const [labsRaw, setLabsRaw] = useState([])
  const [doctorCodes, setDoctorCodes] = useState([])
  const [doctorCode, setDoctorCode] = useState([])
  const [selectedItem, setSelectedItem] = useState(null)

  const toast = useToast()

  const [form, setForm] = useState({
    doctorId: '',
    patientId: '',
    voucherCode: '',
    laboratoriumId: '',
    testLabTypeId: '',
  })

  useEffect(() => {
    getAllDoctors()
    getAllLaboratorium()
  }, [])

  const onChangeSelect = (val) => {
    console.log(val)
    setForm({...form, code: val})
  }
  const onChangeDoctor = (val) => {
    console.log(val)
    if (val) {
      const doctor = doctorsRaw.find(el => el.id == val.id)
      if (doctor) {
        console.log(doctor.code, "onchangedoctor")
        console.log(doctor.fullname, "onchangedoctor")
        setForm({ ...form, doctorId: doctor.id})
        setDoctorCode(doctor.code)
        console.log(form)
      }
    }
  }
  const onChangeLab = (val) => {
    setForm({...form, laboratoriumId: val})
  }
  const getAllDoctors = async () => {
    try {
      const token = await AsyncStorage.getItem('@token')
      const res = await getAll(token)
      let items = [];
      let codes = [];
      for (const i in res.data) {
        items.push({ title: res.data[i].fullname, id: res.data[i].id })
        codes.push({ label: res.data[i].code, value: res.data[i].code })
      }
      setDoctors(items)
      setDoctorsRaw(res.data)
      setDoctorCodes(codes)
    } catch (e) {
      console.log(e)
    }
  }

  const getAllLaboratorium = async () => {
    try {
      const token = await AsyncStorage.getItem('@token')
      const res = await getLabs(token)
      let items = [];
      for (const i in res.data) {
        items.push({ label: res.data[i].name, value: res.data[i].id })
      }
      setLabs(items)
      setLabsRaw(res.data)
    } catch (e) {
      console.log(e)
    }
  }

  const submitData = async () => {
    try {
      const patientId = await AsyncStorage.getItem('@UUID_PATIENT')
      const testLabTypeId = await AsyncStorage.getItem('@UUID_TES_LAB_TYPE')
      const body = {
        patientId,
        testLabTypeId,
        doctorId: form.doctorId,
        laboratoriumId: form.laboratoriumId,
        voucherCode: form.voucherCode
      }
      if (body.patientId && body.testLabTypeId && body.doctorId && body.laboratoriumId && body.voucherCode) {
        const res = await createTest(body)
        if (res.data) {
          console.log(res.data, "teslabId")
          await AsyncStorage.setItem("@TES_LAB_ID", res.data.id)
          navigation.navigate("TestLabPermintaanDiterima")
        } else {
          alert(res)
          console.log(res, "RES")
        }
      }
      console.log(body, "body")
    } catch (e) {
      console.log(e)
    }
  }

  const navigation = useNavigation()
  const [isLoaded] = useFonts({
    'avenir_next_bold': require('../assets/fonts/avenir_next_bold.ttf'),
    'avenir_next_medium': require('../assets/fonts/avenir_next_medium.ttf'),
  });
  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return (
      <NativeBaseProvider>
        <View style={styles.container}>
          <BarHeader header="TES LABORATORIUM" route="TestLabStatusPilihLaboratorium" />
          <View style={styles.contentWrapper}>
            <Text style={styles.sectionTitle}> Masukan {"\n"} Kode Rujukan </Text>
            <Text style={styles.sectionDescription}> Lakukan konfirmasi kepada Dokter {"\n"}
              dengan meminta Kode rujukan. </Text>
            <View style={styles.row}>
            <AutocompleteDropdown
                clearOnFocus={false}
                closeOnBlur={true}
                debounce={600}
                closeOnSubmit={false}
                textInputProps={{
                  placeholder: "Nama Dokter",
                  autoCorrect: false,
                  autoCapitalize: "none",
                  style: {
                    width: width-120,
                    height: 50,
                    borderRadius: 13,
                    backgroundColor: "#fff",
                    fontSize: 18,
                    color: "black",
                    paddingLeft: 18,
                    borderWidth: 1,
                    borderColor: "gray",
                  }
                }}
                suggestionsListContainerStyle={{
                  backgroundColor: "#FFF",
                  zIndex: 99,
                  height: 300,
                }}
                rightButtonsContainerStyle={{
                  borderRadius: 25,
                  right: 1,
                  height: 30,
                  top: 10,
                  alignSelfs: "center",
                  backgroundColor: "#fff"
                }}
                onSelectItem={(value) => onChangeDoctor(value)}
                dataSet={doctors}
              />
              {/* <RNPickerSelect
                placeholder={{
                  label: "Nama Dokter",
                }}
                useNativeAndroidPickerStyle={false}
                style={{...pickerSelectStyles,
                  iconContainer: {
                    top: 4,
                  },
                }}
                Icon={() => {
                  return <Svg style={styles.arrowImage} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                  <Path d="M10 17l5-5-5-5v10z" fill="#830051" />
                </Svg>
                }}
                onValueChange={(value) => onChangeDoctor(value)}
                items={doctors}
              /> */}
              
            </View>
            <RNPickerSelect
                disabled={true}
                placeholder={{
                  label: "Kode Dokter",
                }}
                value={doctorCode}
                useNativeAndroidPickerStyle={false}
                onValueChange={(value) => onChangeSelect(value)}
                style={{
                  ...laboratoriumPickerStyles,
                }}
                items={doctorCodes}
              />
            <Input
              placeholder='Voucher'
              size="xl"
              value={form.voucherCode}
              onChangeText={(val) => setForm({...form, voucherCode: val})}
              style={styles.input}
              w={{
                base: "84%"
              }}
              h={{
                base: "11%"
              }}
            />
            <RNPickerSelect
                placeholder={{
                  label: "Pilih Laboratorium",
                }}
                useNativeAndroidPickerStyle={false}
                onValueChange={(value) => onChangeLab(value)}
                style={{...laboratoriumPickerStyles,
                  iconContainer: {
                    top: 30,
                    right: 35,
                  },
                }}
                items={labs}
                Icon={() => {
                  return <Icon
                    name="search"
                    size={26}
                    type='ionic'
                    color='#757575'
                  />
                }}
              />
          </View>
          <View style={styles.buttonWrapper}>
              <TouchableOpacity onPress={() => submitData()}>
              <LinearGradient style={styles.buttonLogin} colors={['#F0AB00', '#C76A1E']}>
                <Text style={styles.buttonLoginLabel}>LANJUTKAN</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </View>
      </NativeBaseProvider>
    );
  }
}

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 18,
    paddingVertical: 12,
    borderWidth: 1,
    borderRadius: 13,
    borderColor: 'gray',
    width: 140,
    marginHorizontal: 5,
    paddingHorizontal: 10,
    color: 'black',
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});

const laboratoriumPickerStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 18,
    paddingVertical: 22,
    borderWidth: 1,
    borderRadius: 13,
    borderColor: 'gray',
    width: "84%",
    marginHorizontal: 15,
    alignSelf: 'center',
    marginVertical: 20,
    paddingHorizontal: 10,
    height: 40,
    position:'relative',
    color: 'black',
  },
})

const styles = StyleSheet.create({
  contentWrapper: {
    alignSelf: 'center',
    flexDirection: 'column',
    width: width -30,
    marginTop: 22,
    paddingVertical: 21,
    paddingHorizontal: 17,
    backgroundColor: "#FFF",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.1,
    // background color must be set
  },
  arrowImage: {
    flex: 1,
    width: 40,
    height: 38,
  },
  input: {
    alignSelf: 'center',
    borderRadius: 12,
    borderColor: "#979797",
  },
  row: {
    flexDirection: "row",
    justifyContent: 'center',
    zIndex: 1,
  },
  container: {
    width,
    height
  },
  buttonWrapper: {
    bottom: 0,
    position: 'absolute',
    width: width - 20,
    marginBottom: 20,
    alignSelf: 'center'
  },
  buttonLogin: {
    height: 55,
    width: "100%",
    fontSize: 14,
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
  },
  sectionTitle: {
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_bold",
    letterSpacing: 1,
    fontSize: 38,
    marginTop: 21,
  },
  sectionDescription: {
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_next_medium",
    fontSize: 18,
    marginTop: 21,
    fontWeight: "500",
    marginBottom: 35,
    lineHeight: 20,
  },
  bottomText: {
    alignSelf: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_next_demibold",
    textAlign: 'center',
    fontSize: 20,
    bottom: 120,
    position: 'absolute'
  },
  warningIcon: {
    alignSelf: 'center',
    width: 65,
    height: 65,
    position: 'absolute',
    bottom: 180,
  },
  sectionLayout: {
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#3c1053',
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  statusTree: {
    flex: 2,
    alignSelf: 'center',
    marginLeft: 44,
  },
  circleLineGreen: {
    height: 20,
    width: 2,
    alignSelf: 'center',
    backgroundColor: "#C4D600"
  },
  circleLineGray: {
    height: 20,
    width: 2,
    alignSelf: 'center',
    backgroundColor: "#DFDFDF"
  },
  circleLineWhite: {
    height: 20,
    width: 2,
    alignSelf: 'center',
    backgroundColor: "#FFF"
  },
  startTree: {
    alignSelf: 'center',
    flexDirection: 'row',
  },
  treeLabel: {
    textAlign: 'left',
    fontSize: 18,
    alignSelf: 'center',
    color: '#191b26',
    flex: 3,
    fontFamily: "avenir_next_demibold"
  }
});
