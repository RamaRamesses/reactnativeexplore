import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font'
import { useFonts } from '@use-expo/font';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';
import {LinearGradient} from 'expo-linear-gradient';
import { alignItems, alignSelf, fontFamily, justifyContent, marginLeft } from 'styled-system';
import Svg, { SvgProps, Path } from "react-native-svg"
import BarHeader from '../components/BarHeader';
const {width, height} = Dimensions.get("window")

export default function TestLabStatusPilihLaboratorium() {
  const [isLoaded] = useFonts({
    'avenir_next_bold': require('../assets/fonts/avenir_next_bold.ttf'),
    'avenir_next_medium': require('../assets/fonts/avenir_next_medium.ttf'),
  });
  const navigation = useNavigation()
  const [testName, setTestName] = useState('')

  useEffect(() => {
    getTestLabName()
  }, [])

  const getTestLabName = async () => {
    try {
      const test_lab = await AsyncStorage.getItem('@TES_LAB_NAME')
      setTestName(test_lab)
    } catch (err) {
      console.log(err)
    }
  }

  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return (
      <View style={styles.container}>
        <BarHeader header="TES LABORATORIUM" route="PilihTesLaboratorium" />
        <View style={styles.contentWrapper}>
          <View style={styles.sectionLayout}>
            <Text style={styles.sectionTitle}> {testName} </Text>
          </View>
          <View style={styles.statusTree}>
            <View style={styles.startTree}>
              <View style={{flexDirection: 'column', alignSelf: 'flex-end', flex: 1}}>
              <View style={styles.circleLineWhite}></View>

                <Svg style={{alignSelf: 'center'}} width="34" height="34" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                  <Path
                    d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"
                    fill="#DFDFDF"
                  />
                </Svg>
                <View style={styles.circleLineGray}></View>
              </View>
              <Text style={styles.treeLabel}>Pilih Laboratorium</Text>
            </View>
            <View style={styles.startTree}>
              <View style={{flexDirection: 'column', justifyContent: 'center', flex: 1}}>
              <View style={styles.circleLineGray}></View>
                <Svg style={{alignSelf: 'center'}} width="34" height="34" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                  <Path
                    d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"
                    fill="#DFDFDF"
                  />
                </Svg>
                <View style={styles.circleLineWhite}></View>
              </View>
              <Text style={styles.treeLabel}>Unggah hasil tes</Text>
            </View>
          </View>
        </View>
        <Svg style={styles.warningIcon} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
          <Path
            d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-2h2v2zm0-4h-2V7h2v6z"
            fill="#830051"
          />
        </Svg>
        <Text style={styles.bottomText}> Silahkan anda berkonsultasi kepada {"\n"} dokter. </Text>
        <View style={styles.buttonWrapper}>
            <TouchableOpacity onPress={() => navigation.navigate("TestLabKodeRujukan")}>
            <LinearGradient style={styles.buttonLogin} colors={['#F0AB00', '#C76A1E']}>
              <Text style={styles.buttonLoginLabel}>LANJUTKAN</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  contentWrapper: {
    alignSelf: 'center',
    height: 238,
    flexDirection: 'column',
    width: width -30,
    marginTop: 22,
    backgroundColor: "#FFF",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.1,
    // background color must be set
  },
  container: {
    width,
    height
  },
  buttonWrapper: {
    bottom: 0,
    position: 'absolute',
    width: width - 20,
    marginBottom: 20,
    alignSelf: 'center'
  },
  buttonLogin: {
    height: 55,
    width: "100%",
    fontSize: 14,
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
  },
  sectionTitle: {
    color: "#FFF",
    alignSelf: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_next_demibold",
    fontSize: 18,
  },
  bottomText: {
    alignSelf: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_next_demibold",
    textAlign: 'center',
    fontSize: 20,
    bottom: 120,
    position: 'absolute'
  },
  warningIcon: {
    alignSelf: 'center',
    width: 65,
    height: 65,
    position: 'absolute',
    bottom: 180,
  },
  sectionLayout: {
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#3c1053',
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  statusTree: {
    flex: 2,
    alignSelf: 'center',
    marginLeft: 44,
  },
  circleLineGreen: {
    height: 20,
    width: 2,
    alignSelf: 'center',
    backgroundColor: "#C4D600"
  },
  circleLineGray: {
    height: 20,
    width: 2,
    alignSelf: 'center',
    backgroundColor: "#DFDFDF"
  },
  circleLineWhite: {
    height: 20,
    width: 2,
    alignSelf: 'center',
    backgroundColor: "#FFF"
  },
  startTree: {
    alignSelf: 'center',
    flexDirection: 'row',
  },
  treeLabel: {
    textAlign: 'left',
    fontSize: 18,
    alignSelf: 'center',
    color: '#191b26',
    flex: 3,
    fontFamily: "avenir_next_demibold"
  }
});
