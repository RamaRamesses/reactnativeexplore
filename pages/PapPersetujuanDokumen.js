import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font'
import { useFonts } from '@use-expo/font';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity, KeyboardAvoidingView, Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useIsFocused, useNavigation, useRoute } from '@react-navigation/native';
import {LinearGradient} from 'expo-linear-gradient';
import { alignItems, alignSelf, fontFamily, justifyContent, marginLeft } from 'styled-system';
import Svg, { SvgProps, Path } from "react-native-svg"
import BarHeader from '../components/BarHeader';
import { checkTestLabCheckPoint } from '../helpers';
import { Overlay } from 'react-native-elements';
import { AutocompleteDropdown } from 'react-native-autocomplete-dropdown'
import PDFReader from 'rn-pdf-reader-js';
import { getByType } from '../services/api/doctor';
import { create } from '../services/api/program';
import { NativeBaseProvider, Spinner } from 'native-base';
const {width, height} = Dimensions.get("window")

export default function PapPersetujuanDokumen({ signed }) {
  const [isLoaded] = useFonts({
    'avenir_next_bold': require('../assets/fonts/avenir_next_bold.ttf'),
    'avenir_next_medium': require('../assets/fonts/avenir_next_medium.ttf'),
  });
  const navigation = useNavigation()
  const isFocused = useIsFocused();
  const route = useRoute()
  const [testName, setTestName] = useState('')
  const [doctors, setDoctors] = useState([])
  const [doctorsRaw, setDoctorsRaw] = useState([])
  const [pdfUrl, setPdfUrl] = useState("http://206.189.95.69:3004/uploads/pdf/PapTagrisso.pdf")
  const [buttonText, setButtonText] = useState("TANDA TANGANI DOKUMEN")
  const [visibleOverlay, setVisibleOverlay] = useState(false)

  useEffect(() => {
    getAllDoctors()
    conditionalRendering()
  }, [isFocused])


  const conditionalRendering = async (data) => {
    try {
      const consent = await AsyncStorage.getItem('@CONSENT')
      const programType = await AsyncStorage.getItem('@NAMA_PROGRAM_TYPE')
      console.log(route.params.signed, "signed")
      switch (route.params.signed) {
        case true:
          setPdfUrl(`http://206.189.95.69:3004${consent}`)
          setButtonText("MENGIRIM DOKUMEN")
          return;
          // return (<PDFReader
          //   source={{
          //     uri: `http://206.189.95.69:3004${consent}`,
          //   }}
          // />)
        case false:
          setButtonText("TANDA TANGANI DOKUMEN")
          if (programType == "TAGRISSO") {
            setPdfUrl('http://206.189.95.69:3004/uploads/pdf/PapTagrisso.pdf')
          } else if (programType == "FASLODEX") {
            setPdfUrl('http://206.189.95.69:3004/uploads/pdf/PapFaslodex.pdf')
          } else {
            setPdfUrl('http://206.189.95.69:3004/uploads/pdf/PapLynparza.pdf')
          }
          return;
        //   return (<PDFReader
        //     source={{
        //       uri: 'http://206.189.95.69:3004/uploads/pdf/PapTagrisso.pdf',
        //     }}
        //   />)
        // default:
        //   return (<PDFReader
        //     source={{
        //       uri: 'http://206.189.95.69:3004/uploads/pdf/PapTagrisso.pdf',
        //     }}
        //   />)
      }
    } catch (e) {
      console.log(e)
    }
  }

  const nextButton = async () => {
    if (route.params.signed) {
      createProgram()
    } else {
      navigation.navigate("SignatureDrawer")
    }
  }

  const createProgram = async () => {
    try {
      setVisibleOverlay(true)
      await AsyncStorage.setItem("@document_ready", "false")
      const token = await AsyncStorage.getItem('@token')
      const patientId = await AsyncStorage.getItem('@UUID_PATIENT')
      const doctorId = await AsyncStorage.getItem('@PROGRAM_UUID_DOCTOR')
      const programCategory = await AsyncStorage.getItem('@program_category')
      const programTypeId = await AsyncStorage.getItem('@UUID_PROGRAM_TYPE')
      const consent = await AsyncStorage.getItem('@CONSENT')
      const res = await create(token, {
        patientId, doctorId, programTypeId, consent, checkPoint: 0
      })
      if (res.data) {
        console.log(res.data.checkPoint, "checkpoitn")
        await AsyncStorage.setItem("@UUID_PROGRAM", res.data.id)
        setVisibleOverlay(false)
        if (res.data.checkPoint == 2) {
          await AsyncStorage.setItem("@UUID_PROGRAM", res.data.id)
          await AsyncStorage.setItem("@PROGRAM_STATUS", "teslab")
          navigation.navigate("PapStatusSetujuiDokumen", { dokumen: "done", teslab: "done", doctor: "", receipt: "", verify: "" })
        } else {
          await AsyncStorage.setItem("@PROGRAM_STATUS", "dokumen")
          navigation.navigate("PapStatusSetujuiDokumen", { dokumen: "done", teslab: "", doctor: "", receipt: "", verify: "" })
        }
      } else {
        console.log(res)
        Alert.alert(
          "Error",
          res,
          [
            { text: "OK", onPress: () => setVisibleOverlay(false)}
          ]
        );
        
      }
    } catch (e) {
      setVisibleOverlay(false)
      console.log(e)
      Alert.alert(
        "Error",
        e,
        [
          { text: "OK", onPress: () => setVisibleOverlay(false)}
        ]
      );
    }
  }

  const onChangeDoctor = async (val) => {
    await AsyncStorage.removeItem("@PROGRAM_UUID_DOCTOR")
    console.log(val)
    if (val) {
      const doctor = doctorsRaw.find(el => el.id == val.id)
      if (doctor) {
        console.log(doctor.code, "onchangedoctor")
        console.log(doctor.fullname, "onchangedoctor")
        try {
          console.log(doctor.id, "doctor i")
          await AsyncStorage.setItem("@PROGRAM_UUID_DOCTOR", doctor.id)
          await AsyncStorage.setItem("@PROGRAM_NAMA_DOKTER", doctor.fullname)
        } catch (e) {
          console.log(e)
        }
      }
    }
  }

  const getAllDoctors = async () => {
    try {
      const token = await AsyncStorage.getItem('@token')
      const programType = await AsyncStorage.getItem('@NAMA_PROGRAM_TYPE')
      const res = await getByType(token, programType)
      let items = [];
      let codes = [];
      for (const i in res.data) {
        items.push({ title: res.data[i].fullname, id: res.data[i].id })
        codes.push({ label: res.data[i].code, value: res.data[i].code })
      }
      setDoctors(items)
      setDoctorsRaw(res.data)
    } catch (e) {
      console.log(e)
    }
  }

  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return (
      <NativeBaseProvider>
        <View style={styles.container}>
        <BarHeader header="PROGRAM PULIH" />
          <View style={styles.contentWrapper}>
            <PDFReader
              source={{
                uri: pdfUrl,
              }}
            />
            {/* {conditionalRendering()} */}
          </View>
            <Overlay isVisible={visibleOverlay}>
              <View style={{margin: 5}}>
                  <Spinner color="cyan.500" />
              </View>
            </Overlay>
              <View style={styles.textSearch}>
                <KeyboardAvoidingView
            behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
            style={{ flex: 1 }}>
                  <AutocompleteDropdown
                  debounce={600}
                    clearOnFocus={false}
                    closeOnBlur={true}
                    closeOnSubmit={true}
                    direction={Platform.select({ ios: "up" })}
                    textInputProps={{
                      placeholder: "Nama Dokter",
                      autoCorrect: false,
                      autoCapitalize: "none",
                      style: {
                        width: "90%",
                        height: 50,
                        borderRadius: 8,
                        alignSelf: 'center',
                        backgroundColor: "#fff",
                        fontSize: 18,
                        color: "black",
                        paddingLeft: 18,
                        borderWidth: 1,
                        borderColor: "#DFDFDF",
                      }
                    }}
                    suggestionsListContainerStyle={{
                      backgroundColor: "#FFF",
                      zIndex: 99,
                      height: 300,
                    }}
                    rightButtonsContainerStyle={{
                      borderRadius: 25,
                      right: 30,
                      height: 30,
                      top: 10,
                      alignSelfs: "center",
                      backgroundColor: "#fff"
                    }}
                    onSelectItem={(value) => onChangeDoctor(value)}
                    dataSet={doctors}
                  />
                </KeyboardAvoidingView>
              </View>
              <View style={styles.buttonWrapper}>
                  <TouchableOpacity onPress={() => nextButton()}>
                  <LinearGradient style={styles.buttonLogin} colors={['#F0AB00', '#C76A1E']}>
                    <Text style={styles.buttonLoginLabel}>{buttonText}</Text>
                  </LinearGradient>
                </TouchableOpacity>
              </View>
        </View>
      </NativeBaseProvider>
    );
  }
}

const styles = StyleSheet.create({
  contentWrapper: {
    alignSelf: 'center',
    flexDirection: 'column',
    width: width -30,
    height: 300,
    marginTop: 22,
    backgroundColor: "#FFF",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.1,
    // background color must be set
  },
  container: {
    width,
    height,
  },
  textSearch: {
    marginTop: 50,
    flex: 1,
  },
  buttonWrapper: {
    bottom: 20,
    flex: 1,
    position: 'absolute',
    width: width - 20,
    alignSelf: 'center'
  },
  buttonLogin: {
    height: 55,
    width: "100%",
    fontSize: 14,
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
  },
  sectionTitle: {
    color: "#FFF",
    alignSelf: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_next_demibold",
    fontSize: 18,
  },
  bottomText: {
    flex: 1,
    alignSelf: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_next_demibold",
    textAlign: 'center',
    fontSize: 20,
    marginBottom: 41,
  },
  warningIcon: {
    alignSelf: 'center',
    width: 65,
    height: 65,
    marginTop: 42,
    marginBottom: 10,
  },
  sectionLayout: {
    justifyContent: 'center',
    flexDirection: 'row',
    height: 80,
    alignItems: 'center',
    backgroundColor: '#3c1053',
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  statusTree: {
    alignSelf: 'center',
    marginLeft: 44,
    marginVertical: 10,
  },
  circleLineGreen: {
    height: 20,
    width: 2,
    alignSelf: 'center',
    backgroundColor: "#C4D600"
  },
  circleLineGray: {
    height: 20,
    width: 2,
    alignSelf: 'center',
    backgroundColor: "#DFDFDF"
  },
  circleLineWhite: {
    height: 20,
    width: 2,
    alignSelf: 'center',
    backgroundColor: "#FFF"
  },
  startTree: {
    alignSelf: 'center',
    flexDirection: 'row',
  },
  treeLabel: {
    textAlign: 'left',
    fontSize: 18,
    alignSelf: 'center',
    color: '#191b26',
    flex: 3,
    fontFamily: "avenir_next_demibold"
  }
});
