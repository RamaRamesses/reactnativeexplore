import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font'
import { useFonts } from '@use-expo/font';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useIsFocused, useNavigation, useRoute } from '@react-navigation/native';
import {LinearGradient} from 'expo-linear-gradient';
import { alignItems, alignSelf, fontFamily, justifyContent, marginLeft } from 'styled-system';
import Svg, { SvgProps, Path } from "react-native-svg"
import BarHeader from '../components/BarHeader';
import { checkTestLabCheckPoint } from '../helpers';
const {width, height} = Dimensions.get("window")

export default function PapStatusSetujuiDokumen({ type }) {
  const [isLoaded] = useFonts({
    'avenir_next_bold': require('../assets/fonts/avenir_next_bold.ttf'),
    'avenir_next_medium': require('../assets/fonts/avenir_next_medium.ttf'),
  });
  const navigation = useNavigation()
  const route = useRoute()
  const isFocused = useIsFocused()
  const [testName, setTestName] = useState('')
  const [routeDirection, setRouteDirection] = useState('')
  const [bottomText, setBottomText] = useState("Anda belum memasukan dokumen \n persetujuan, silakan isi dokumen.")
  const [buttonLabel, setButtonLabel] = useState("MENGISI DOKUMEN")
  const [isTestLab, setIsTestLab] = useState(false)

  useEffect(() => {
    initialState()
    getTestLabName()
  }, [isFocused])

  const initialState = async () => {
    try {
      const programStatus = await AsyncStorage.getItem('@PROGRAM_STATUS')
      const programName = await AsyncStorage.getItem('@NAMA_PROGRAM_TYPE')
      console.log(programName)
      console.log(programStatus, "programwstatus")
      if (programStatus == "dokumen") {
        route.params.dokumen = "done"
        setBottomText("Hasil tes anda kosong,\n Apakah anda ingin melakukan test?")
        setIsTestLab(true)
      } else if (programStatus == "teslab") {
        route.params.dokumen = "done"
        route.params.teslab = "done"
        setButtonLabel("KONFIRMASI DOKTER")
        setBottomText("Silahkan hubungi dokter Anda untuk mendapatkan kode verifikasi. Jika sudah memiliki kode verifikasi, Anda dapat melanjutkan proses selanjutnya.")
      } else if (programStatus == "doctor") {
        route.params.dokumen = "done"
        route.params.teslab = "done"
        route.params.doctor = "done"
        setBottomText("Anda belum memasukan resep, silakan unggah resep dokter rujukan untuk Anda.")
        setButtonLabel("UNGGAH RESEP")
      } else if (programStatus == "receipt") {
        route.params.dokumen = "done"
        route.params.teslab = "done"
        route.params.doctor = "done"
        route.params.receipt = "done"
        setBottomText("Dokumen Anda sedang ditinjau oleh \n verifikator. Silakan menunggu hingga dokumen Anda disetujui atau ditolak.")
        setButtonLabel("KEMBALI")
      } else if (programStatus == "verify") {
        route.params.dokumen = "done"
        route.params.teslab = "done"
        route.params.doctor = "done"
        route.params.receipt = "done"
        route.params.verify = "done"
        setBottomText("Dokumen Anda sudah diverifikasi \n Harap lanjut ke proses Scan QR.")
        setButtonLabel("LANJUTKAN")
      }
    } catch (e) {
      console.log(e)
    }
  }

  const setRoute = () => {
    if (route.params) {
      if(route.params.dokumen == "done") {
        setRouteDirection("PapPersetujuanDokumen")
      }
    } else {
      setRouteDirection("PilihProgramPAP")
    }
  }

  const persetujuanDokumen = () => {
    switch (route.params.dokumen) {
      case "done":
        return (
          <View style={styles.startTree}>
              <View style={{flexDirection: 'column', alignSelf: 'flex-end', flex: 1}}>
              <View style={styles.circleLineWhite}></View>

                <Svg style={{alignSelf: 'center'}} width="34" height="34" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                  <Path
                    d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"
                    // fill="#c4d600"
                    fill="#c4d600"
                  />
                </Svg>
                <View style={styles.circleLineGreen}></View>
              </View>
              <Text style={styles.treeLabel}>Persetujuan Pasien</Text>
            </View>
        )
      case "":
        return (
        <View style={styles.startTree}>
          <View style={{flexDirection: 'column', alignSelf: 'flex-end', flex: 1}}>
          <View style={styles.circleLineWhite}></View>

            <Svg style={{alignSelf: 'center'}} width="34" height="34" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
              <Path
                d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"
                // fill="#c4d600"
                fill="#DFDFDF"
              />
            </Svg>
            <View style={styles.circleLineGray}></View>
          </View>
          <Text style={styles.treeLabel}>Persetujuan Pasien</Text>
        </View>)
      default:
        return (
          <View style={styles.startTree}>
            <View style={{flexDirection: 'column', alignSelf: 'flex-end', flex: 1}}>
            <View style={styles.circleLineWhite}></View>
  
              <Svg style={{alignSelf: 'center'}} width="34" height="34" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                <Path
                  d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"
                  // fill="#c4d600"
                  fill="#DFDFDF"
                />
              </Svg>
              <View style={styles.circleLineGray}></View>
            </View>
            <Text style={styles.treeLabel}>Persetujuan Pasien</Text>
          </View>)
    }
  }

  const getTestLabName = async () => {
    try {
      const test_lab = await AsyncStorage.getItem('@TES_LAB_NAME')
      setTestName(test_lab)
    } catch (err) {
      console.log(err)
    }
  }

  const nextButton = async () => {
    try {
      const programStatus = await AsyncStorage.getItem('@PROGRAM_STATUS')
      console.log(programStatus, "Program Status")
      if (programStatus == "dokumen") {
        navigation.navigate(navigation.navigate("PapPersetujuanDokumen", { signed: false }))
      } else if (programStatus == "teslab") {
        navigation.navigate("PapKonfirmasiDokter")
      } else if (programStatus == "doctor") {
        navigation.navigate(navigation.navigate("PapUnggahResepDokter"))
      } else if (programStatus == "receipt" || programStatus == "verify") {
        const route = await checkTestLabCheckPoint()
        navigation.navigate(route.page, { fragment: route.fragment })
      } else {
        navigation.navigate(navigation.navigate("PapPersetujuanDokumen", { signed: false }))
      }
    } catch(e) {
      console.log(e)
    }
  }

  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return (
      <ScrollView style={styles.container}>
        <BarHeader header="PROGRAM PULIH" />
        <View style={styles.contentWrapper}>
          <View style={styles.sectionLayout}>
            <Text style={styles.sectionTitle}> STATUS </Text>
          </View>
          <View style={styles.statusTree}>
            {persetujuanDokumen()}
            <View style={styles.startTree}>
              <View style={{flexDirection: 'column', justifyContent: 'center', flex: 1}}>
                {
                  route.params?.teslab == "done" ?
                  (
                    <View style={styles.circleLineGreen}></View>
                  ) : ( <View style={styles.circleLineGray}></View> )
                }
                <Svg style={{alignSelf: 'center'}} width="34" height="34" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                  <Path
                    d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"
                    fill={route.params?.teslab == "done" ? "#c4d600" : "#DFDFDF"} 
                  />
                </Svg>
                {
                  route.params?.teslab == "done" ?
                  (
                    <View style={styles.circleLineGreen}></View>
                  ) : ( <View style={styles.circleLineGray}></View> )
                }
              </View>
              <Text style={styles.treeLabel}>Unggah Hasil Tes Laboratorium</Text>
            </View>
            <View style={styles.startTree}>
              <View style={{flexDirection: 'column', justifyContent: 'center', flex: 1}}>
              {
                  route.params?.doctor == "done" ?
                  (
                    <View style={styles.circleLineGreen}></View>
                  ) : ( <View style={styles.circleLineGray}></View> )
                }
                <Svg style={{alignSelf: 'center'}} width="34" height="34" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                  <Path
                    d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"
                    fill={route.params?.doctor == "done" ? "#c4d600" : "#DFDFDF"} 
                  />
                </Svg>
                {
                  route.params?.doctor == "done" ?
                  (
                    <View style={styles.circleLineGreen}></View>
                  ) : ( <View style={styles.circleLineGray}></View> )
                }
              </View>
              <Text style={styles.treeLabel}>Konfirmasi Dokter</Text>
            </View>
            <View style={styles.startTree}>
              <View style={{flexDirection: 'column', justifyContent: 'center', flex: 1}}>
              {
                  route.params?.receipt == "done" ?
                  (
                    <View style={styles.circleLineGreen}></View>
                  ) : ( <View style={styles.circleLineGray}></View> )
                }
                <Svg style={{alignSelf: 'center'}} width="34" height="34" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                  <Path
                    d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"
                    fill={route.params?.receipt == "done" ? "#c4d600" : "#DFDFDF"} 
                  />
                </Svg>
                {
                  route.params?.receipt == "done" ?
                  (
                    <View style={styles.circleLineGreen}></View>
                  ) : ( <View style={styles.circleLineGray}></View> )
                }
              </View>
              <Text style={styles.treeLabel}>Unggah Resep Dokter</Text>
            </View>
            <View style={styles.startTree}>
              <View style={{flexDirection: 'column', justifyContent: 'center', flex: 1}}>
                { 
                  route.params?.verify == "done" ?
                  (
                    <View style={styles.circleLineGreen}></View>
                  ) : ( <View style={styles.circleLineGray}></View> )
                } 
                <Svg style={{alignSelf: 'center'}} width="34" height="34" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                  <Path
                    d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"
                    fill={route.params?.verify == "done" ? "#c4d600" : "#DFDFDF"} 
                  />
                </Svg>
                <View style={styles.circleLineWhite}></View>
              </View>
              <Text style={styles.treeLabel}>Verifikasi</Text>
            </View>
          </View>
        </View>
        <Svg style={styles.warningIcon} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
          <Path
            d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-2h2v2zm0-4h-2V7h2v6z"
            fill="#830051"
          />
        </Svg>
        <Text style={styles.bottomText}> {bottomText} </Text>
        {
          isTestLab == false ?
          (
            <View style={styles.buttonWrapper}>
                <TouchableOpacity onPress={() => nextButton()}>
                <LinearGradient style={styles.buttonYellow} colors={['#F0AB00', '#C76A1E']}>
                  <Text style={styles.buttonLoginLabel}>{buttonLabel}</Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
          ) : (
            <View style={styles.tesLabButtonWrapper}>
              <TouchableOpacity onPress={() => navigation.navigate("PilihTesLaboratorium", { type: 'regular' })}>
                <LinearGradient style={styles.button} colors={['#F0AB00', '#C76A1E']}>
                  <Text style={styles.buttonYes}>TES LAB</Text>
                </LinearGradient>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate("PilihTesLaboratorium", { type: 'self' })}>
                <LinearGradient style={styles.button} colors={['#dfdfdf', '#dfdfdf']}>
                  <Text style={styles.buttonNo}>UNGGAH TES</Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
          )
        }
        {/* <View style={styles.buttonWrapper}>
            <TouchableOpacity onPress={() => navigation.navigate("PapPersetujuanDokumen", { signed: false })}>
            <LinearGradient style={styles.buttonLogin} colors={['#F0AB00', '#C76A1E']}>
              <Text style={styles.buttonLoginLabel}>{buttonLabel}</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View> */}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  contentWrapper: {
    alignSelf: 'center',
    flexDirection: 'column',
    width: width -30,
    marginTop: 22,
    backgroundColor: "#FFF",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.1,
    // background color must be set
  },
  container: {
    width,
    height
  },
  buttonWrapper: {
    bottom: 0,
    width: width - 20,
    marginBottom: 20,
    alignSelf: 'center'
  },
  tesLabButtonWrapper: {
    width: width - 20,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-around',
    marginBottom: 20,
    bottom: 0,
  },
  button: {
    height: 65,
    width: 182,
    fontSize: 14,
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonYes: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 18,
    color: 'white',
  },
  buttonNo: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 18,
    color: 'gray',
  },
  buttonYellow: {
    height: 55,
    width: "100%",
    fontSize: 14,
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
  },
  sectionTitle: {
    color: "#FFF",
    alignSelf: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_next_demibold",
    fontSize: 18,
  },
  bottomText: {
    flex: 1,
    alignSelf: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_next_demibold",
    textAlign: 'center',
    fontSize: 20,
    marginHorizontal: 30,
    marginBottom: 41,
  },
  warningIcon: {
    alignSelf: 'center',
    width: 65,
    height: 65,
    marginTop: 42,
    marginBottom: 10,
  },
  sectionLayout: {
    justifyContent: 'center',
    flexDirection: 'row',
    height: 80,
    alignItems: 'center',
    backgroundColor: '#3c1053',
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  statusTree: {
    alignSelf: 'center',
    marginLeft: 44,
    marginVertical: 10,
  },
  circleLineGreen: {
    height: 20,
    width: 2,
    alignSelf: 'center',
    backgroundColor: "#C4D600"
  },
  circleLineGray: {
    height: 20,
    width: 2,
    alignSelf: 'center',
    backgroundColor: "#DFDFDF"
  },
  circleLineWhite: {
    height: 20,
    width: 2,
    alignSelf: 'center',
    backgroundColor: "#FFF"
  },
  startTree: {
    alignSelf: 'center',
    flexDirection: 'row',
  },
  treeLabel: {
    textAlign: 'left',
    fontSize: 18,
    alignSelf: 'center',
    color: '#191b26',
    flex: 3,
    fontFamily: "avenir_next_demibold"
  }
});
