import React, {useState, useEffect, useRef} from 'react';
import AppLoading from 'expo-app-loading';
import { useFonts } from '@use-expo/font';
import { StyleSheet, SafeAreaView, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity, CheckBox, PickerIOSBase } from 'react-native';
import { Checkbox, Center, NativeBaseProvider, Spinner } from "native-base"
import {LinearGradient} from 'expo-linear-gradient';
import { Input, Icon, Overlay } from 'react-native-elements';
const {width, height} = Dimensions.get("window")
import { useNavigation, useRoute } from '@react-navigation/native';
import RNPickerSelect from 'react-native-picker-select';
import { getById, updateById } from '../services/api/patient';
import BarHeader from '../components/BarHeader';
import AsyncStorage from '@react-native-async-storage/async-storage';
import DateTimePicker from '@react-native-community/datetimepicker';
import { baseUrl } from '../config';
import ImageView from "react-native-image-viewing";
import * as ImagePicker from 'expo-image-picker';
import { upload } from '../services/api/fileUpload';
import Toast, {DURATION} from 'react-native-easy-toast'

export default function EditProfile() {
  const [date, setDate] = useState(new Date(1598051730000));
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
    setUser({...user, dateOfBirth: selectedDate})
    console.log(user, "uzser")
  };

  const [password, setPassword] = useState(true);
  const [passwordErrorMsg, setPasswordErrorMsg] = useState("");
  const [emailErrorMsg, setEmailErrorMsg] = useState("");
  const [cities, setCities] = useState([]);
  const [cityValue, setCityValue] = useState("");
  const [buttonDisabled, setButtonDisabled] = useState(false);
  const [ktpVisibility, setKtpVisibility] = useState(false);
  const [selfieVisibility, setSelfieVisibility] = useState(false);
  const [visibleOverlay, setVisibleOverlay] = useState(false);
  const [selectedImage, setSelectedImage] = useState("");
  const [isImageLoading, setIsImageLoading] = useState(false);

  const toggleOverlay = (field = "none") => {
    setSelectedImage(field)
    setVisibleOverlay(!visibleOverlay);
  };

  const navigation = useNavigation();
  const route = useRoute();
  const ref = useRef();
  const [isLoaded] = useFonts({
    'avenir_next_bold': require('../assets/fonts/avenir_next_bold.ttf'),
    'avenir_next_medium': require('../assets/fonts/avenir_next_medium.ttf'),
  });
useEffect(() => {
  getProfileById()
}, [])

   // The path of the picked image
   const [pickedImagePath, setPickedImagePath] = useState('');

   // This function is triggered when the "Select an image" button pressed

  const gender = [
    { label: 'Pria', value: 'male' },
    { label: 'Wanita', value: 'female' },
  ]

  const onChangeGender = (val) => {
    setUser({...user, gender: val})
  }

  const [user, setUser] = useState({
    status: 1,
    email: '',
    password: '',
    city: '',
    idNumber: '',
    fullname: '',
    gender: 'male',
    idPicture: '',
    selfiePicture: '',
    code: '',
    dateOfBirth: ''
  })

  const [errorMessage, setErrorMessage] = useState({
    email: '',
    password: '',
    city: '',
    idNumber: '',
    fullname: '',
    gender: '',
    code: '',
    dateOfBirth: ''
  })

  const showImagePicker = async () => {
    setIsImageLoading(true)
    // Ask the user for the permission to access the media library 
    const permissionResult = await ImagePicker.requestMediaLibraryPermissionsAsync();

    if (permissionResult.granted === false) {
      alert("You've refused to allow this appp to access your photos!");
      return;
    }

    const result = await ImagePicker.launchImageLibraryAsync();

    // Explore the result
    console.log(result);

    if (!result.cancelled) {
      setPickedImagePath(result.uri);
      let filename = result.uri.split('/').pop();
  
      let match = /\.(\w+)$/.exec(filename);
      let type = match ? `image/${match[1]}` : `image`;
  
      // Upload the image using the fetch and FormData APIs
      let formData = new FormData();
      // Assume "photo" is the name of the form field the server expects
      formData.append('upload', { uri: result.uri, name: filename, type });
      console.log(result.uri);
      
      const res = await upload(formData);
      console.log(res.data, "racse")
      if (selectedImage === "idPicture") setUser({...user, idPicture: res.data.data})
      else setUser({...user, selfiePicture: res.data.data})
      toggleOverlay()
      setIsImageLoading(false)
     }
  }

  const openCamera = async () => {
   // Ask the user for the permission to access the camera
   setIsImageLoading(true)
   const permissionResult = await ImagePicker.requestCameraPermissionsAsync();

   if (permissionResult.granted === false) {
     alert("You've refused to allow this appp to access your camera!");
     return;
   }

   const result = await ImagePicker.launchCameraAsync();

   // Explore the result
   console.log(result);

   if (!result.cancelled) {
    setPickedImagePath(result.uri);
    let filename = result.uri.split('/').pop();

    let match = /\.(\w+)$/.exec(filename);
    let type = match ? `image/${match[1]}` : `image`;

    // Upload the image using the fetch and FormData APIs
    let formData = new FormData();
    // Assume "photo" is the name of the form field the server expects
    formData.append('upload', { uri: result.uri, name: filename, type });
    console.log(result.uri);

    const res = await upload(formData);
    console.log(res.data, "racse")
    if (selectedImage === "idPicture") setUser({...user, idPicture: res.data.data})
    else setUser({...user, selfiePicture: res.data.data})
    setIsImageLoading(false)
    toggleOverlay()
   }
 }

  const getProfileById = async () => {
    const token = await AsyncStorage.getItem('@token')
    const patientId = await AsyncStorage.getItem('@UUID_PATIENT')

    if (route.params.redirect == true) {
      ref.current.show("Harap lengkapi data anda terlebih dahulu!", 1500)
    }
  
    if (patientId != null) {
      const res = await getById(token, patientId)
      if (res.data) {
        const body = res.data
        let user = {}
        if (body.id != null) {
          if (body.code != null) {
            user.code = body.code
          }
          if (body.dateOfBirth != null) {
            user.dateOfBirth = setDate(new Date(body.dateOfBirth))
            console.log(date, "detto")
          }
          if (body.gender != null) {
            user.gender = body.gender
          }
          if (body.fullname != null) {
            user.fullname = body.fullname
          }
          if (body.email != null) {
            user.email = body.email
          }
          if (body.idNumber != null) {
            user.idNumber = body.idNumber
          }
          if (body.representativeName != null) {
            user.representativeName = body.representativeName
          }
          if (body.representativePhone != null) {
            user.representativePhone = body.representativePhone
          }
          if (body.representativeRelationship != null) {
            user.representativeRelationship = body.representativeRelationship
          }
          if (body.idPicture != null) {
            user.idPicture = body.idPicture
          }
          if (body.selfiePicture != null) {
            user.selfiePicture = body.selfiePicture
          }
          setUser(user)
        } 
      }
    }
  }

  const submitData = async () => {
    try {
      const token = await AsyncStorage.getItem('@token')
      const patientId = await AsyncStorage.getItem('@UUID_PATIENT')
      let isNotValid = false
      if (user.email == null || user.email == "") {
        isNotValid = true
        setErrorMessage({...errorMessage, email: "Email tidak valid"})
      }
      if (user.idNumber.length < 16 || user.idNumber == null) {
        isNotValid = true
        setErrorMessage({...errorMessage, idNumber: "No. KTP wajib 16 digit"})
      }
      if (isNotValid == false) {
        const data = user
        if (!data.gender) {
          data.gender = 'male'
        }
        console.log(user, "user")
        const res = await updateById(token, patientId, data)
        if (res.data) {
          navigation.navigate("SplashScreen")
        }
      }
    } catch (e) {
      console.log(e, "error")
    }
  }

  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return (
      <NativeBaseProvider>
      <BarHeader />
        <ScrollView>
          <View style={styles.contentWrapper}>
            <View style={styles.input}>
              <Text style={styles.inputLabel}> Nama lengkap sesuai KTP </Text>
              <Input
                onChangeText={(val) => setUser({...user, fullname: val})}
                value={user.fullname}
                errorStyle={{ color: 'red' }}
              errorMessage={passwordErrorMsg}
              style={{paddingLeft: 8}} />
            </View>
            <View style={styles.input}>
              <Text style={styles.inputLabel}> Tanggal lahir sesuai KTP </Text>
                <DateTimePicker
                  testID="dateTimePicker"
                  value={date}
                  mode={mode}
                  is24Hour={true}
                  style={{ width: "100%", marginLeft: 10 }}
                  display="default"
                  onChange={onChange}
                />
            </View>
            <View style={styles.input}>
              <Text style={styles.inputLabel}> No KTP </Text>
              <Input
                value={user.idNumber}
                onChangeText={(val) => setUser({...user, idNumber: val})}
              errorStyle={{ color: 'red' }}
              errorMessage={passwordErrorMsg}
              style={{paddingLeft: 8}} />
            </View>
            <View style={styles.layoutUnggah}>
              <Text style={styles.inputLabel}> Foto KTP </Text>
              <TouchableOpacity onPress={() => setKtpVisibility(true)} style={styles.dashedLayout}>
                {!user.idPicture ? (
                  <Image
                  resizeMode="contain"
                  style={styles.imageKTP}
                  source={require('../assets/onboarding_login.png')}
                />
                ) : (
                  <Image
                  style={styles.imageKTP}
                  source={{
                    uri: baseUrl + user.idPicture
                  }}
                  />
                )}
                  <ImageView
                    images={[{
                      uri: baseUrl + user.idPicture
                    }]}
                    imageIndex={0}
                    visible={ktpVisibility}
                    onRequestClose={() => setKtpVisibility(false)}
                  />
              </TouchableOpacity>
              <View style={styles.editImageLinkLayout}>
                <TouchableOpacity onPress={() => setUser({...user, idPicture: null})}><Text style={styles.imageLink}>Hapus</Text></TouchableOpacity>
                <Text style={{color: "gray", marginHorizontal: 10}}>atau</Text>
                <TouchableOpacity onPress={() => toggleOverlay("idPicture")}><Text style={styles.imageLink}>Unggah Ulang</Text></TouchableOpacity>
              </View>
              <Overlay isVisible={visibleOverlay} onBackdropPress={toggleOverlay}>
                <View style={{margin: 5}}>
                  {isImageLoading ? (
                    <Spinner color="cyan.500" />
                  ) : (
                  <View>
                    <Text style={styles.overlayLabel}>Pilih Media Untuk Ambil {"\n"} gambar</Text>
                    <View style={styles.modalOverlay}>
                      <TouchableOpacity disabled={buttonDisabled} onPress={() => openCamera()}>
                        <LinearGradient style={styles.buttonOverlay} colors={['#F0AB00', '#C76A1E']}>
                          <Text style={styles.buttonLoginLabel}>CAMERA</Text>
                        </LinearGradient>
                      </TouchableOpacity>
                      <TouchableOpacity disabled={buttonDisabled} onPress={() => showImagePicker()}>
                        <LinearGradient style={styles.buttonOverlay} colors={['#F0AB00', '#C76A1E']}>
                          <Text style={styles.buttonLoginLabel}>GALLERY</Text>
                        </LinearGradient>
                      </TouchableOpacity>
                    </View>
                  </View>
                  )}
                </View>
              </Overlay>
            </View>
            <View style={styles.layoutUnggah}>
              <Text style={styles.inputLabel}> Foto Selfie </Text>
                <TouchableOpacity onPress={() => setSelfieVisibility(true)} style={styles.dashedLayout}>
                {!user.selfiePicture ? (
                    <Image
                    resizeMode="contain"
                    style={styles.imageKTP}
                    source={require('../assets/onboarding_login.png')}
                  />
                  ) : (
                    <Image
                    style={styles.imageKTP}
                    source={{
                      uri: baseUrl + user.selfiePicture
                    }}
                  />
                  )}
                  <ImageView
                      images={[{
                        uri: baseUrl + user.selfiePicture
                      }]}
                      imageIndex={0}
                      visible={selfieVisibility}
                      onRequestClose={() => setSelfieVisibility(false)}
                    />
                </TouchableOpacity>
                <View style={styles.editImageLinkLayout}>
                  <TouchableOpacity onPress={() => setUser({...user, idPicture: null})}><Text style={styles.imageLink}>Hapus</Text></TouchableOpacity>
                  <Text style={{color: "gray", marginHorizontal: 10}}>atau</Text>
                  <TouchableOpacity onPress={() => toggleOverlay("selfiePicture")}><Text style={styles.imageLink}>Unggah Ulang</Text></TouchableOpacity>
                </View>
            </View>
            <View style={styles.input}>
              <Text style={styles.inputLabel}> Jenis Kelamin </Text>
              {/* <Input
                value={user.gender}
                onChangeText={(val) => setUser({...user, gender: val})}
                errorStyle={{ color: 'red' }}
              errorMessage={passwordErrorMsg}
              style={{paddingLeft: 8}} /> */}
              <RNPickerSelect
              placeholder={{
              }}
              useNativeAndroidPickerStyle={false}
                onValueChange={(value) => onChangeGender(value)}
                value={user.gender}
                style={pickerSelectStyles}
                items={gender}
            />
            </View>
            <View style={styles.input}>
              <Text style={styles.inputLabel}> Email </Text>
              <Input
                value={user.email}
                onChangeText={(val) => setUser({...user, email: val})}
                errorStyle={{ color: 'red' }}
              errorMessage={passwordErrorMsg}
              style={{paddingLeft: 8}} />
            </View>
            <Text style={{ width: "100%", height: 30, backgroundColor: "#e4e9f2", marginBottom: 10 }}></Text>
            <View style={styles.input}>
              <Text style={styles.inputLabel}> Nama Perwakilan </Text>
              <Input
                onChangeText={(val) => setUser({...user, representativeName: val})}
                value={user.representativeName}
                errorStyle={{ color: 'red' }}
              errorMessage={passwordErrorMsg}
              style={{paddingLeft: 8}} />
            </View>
            <View style={styles.input}>
              <Text style={styles.inputLabel}> Telepon </Text>
              <Input
                onChangeText={(val) => setUser({...user, representativePhone: val})}
                value={user.representativePhone}
                errorStyle={{ color: 'red' }}
              errorMessage={passwordErrorMsg}
              style={{paddingLeft: 8}} />
            </View>
            <View style={styles.input}>
              <Text style={styles.inputLabel}> Hubungan </Text>
              <Input
                onChangeText={(val) => setUser({...user, representativeRelationship: val})}
                value={user.representativeRelationship}
                errorStyle={{ color: 'red' }}
              errorMessage={passwordErrorMsg}
              style={{paddingLeft: 8}} />
            </View>
            <View style={styles.buttonWrapper}>
              <TouchableOpacity disabled={buttonDisabled} onPress={() => submitData()}>
              <LinearGradient style={styles.buttonLogin} colors={['#F0AB00', '#C76A1E']}>
                <Text style={styles.buttonLoginLabel}>SIMPAN</Text>
              </LinearGradient>
            </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
          <Toast ref={ref} />
      </NativeBaseProvider>
    );
  }
}

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 18,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderBottomWidth: 1,
    borderColor: 'gray',
    marginHorizontal: 10,
    marginBottom: 30,
    color: 'black',
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    width: width,
    height: height,
    justifyContent: 'center',
  },
  modalOverlay: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  overlayLabel: {
    fontFamily: "avenir_bold",
    alignSelf: 'center',
    textAlign: 'center',
    color: "gray",
    fontSize: 18,
    marginVertical: 20,
  },
  buttonOverlay: {
    padding: 20,
    fontSize: 14,
    borderRadius: 18,
    marginHorizontal: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  editImageLinkLayout: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: 15,
  },
  imageKTP: {
    width: "100%",
    height: "100%",
    resizeMode: "stretch"
  },
  imageLink: {
    color: "#800152"
  },
  dashedLayout: {
    width: 203,
    height: 134,
    padding: 10,
    backgroundColor: '#E8E8E8',
    borderStyle: 'dotted',
    borderWidth: 2,
    borderColor: '#A8A8A8',
    borderRadius: 7,
  },
  buttonLogin: {
    padding: 20,
    fontSize: 14,
    borderRadius: 18,
    marginHorizontal: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
  },
  layoutUnggah: {
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: 30,
  },
  input: {
    width: width - 44,
    padding: 10,
  },
  inputPassword: {
    height: 40,
    width: width - 44,
    marginBottom: 50,
    marginTop: 50,
    fontSize: 28,
    padding: 10,
  },
  inputLabel: {
    color: 'gray',
    paddingLeft: 8,
    paddingBottom: 15,
  },
  text: {
    fontSize: 46,
    color: '#800152',
    textAlign: 'center',
    fontFamily: 'avenir_next_bold',
    bottom: 30,
  },
  contentWrapper: {
    alignItems: 'center',
    marginTop: 20,
  },
  buttonWrapper: {
    width: width,
    marginBottom: 20,
  }
});
