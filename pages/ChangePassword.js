import React, {useState, useEffect} from 'react';
import AppLoading from 'expo-app-loading';
import { useFonts } from '@use-expo/font';
import { StyleSheet, SafeAreaView, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity, CheckBox, PickerIOSBase, Alert } from 'react-native';
import { Checkbox, Center, NativeBaseProvider, Spinner } from "native-base"
import {LinearGradient} from 'expo-linear-gradient';
import { Input, Icon, Overlay } from 'react-native-elements';
const {width, height} = Dimensions.get("window")
import { useNavigation } from '@react-navigation/native';
import RNPickerSelect from 'react-native-picker-select';
import { getById, updateById } from '../services/api/patient';
import BarHeader from '../components/BarHeader';
import AsyncStorage from '@react-native-async-storage/async-storage';
import DateTimePicker from '@react-native-community/datetimepicker';
import { baseUrl } from '../config';
import ImageView from "react-native-image-viewing";
import * as ImagePicker from 'expo-image-picker';
import { changePassword, login } from '../services/api/auth';

export default function ChangePassword() {
  const [date, setDate] = useState(new Date(1598051730000));
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
    setUser({...user, dateOfBirth: selectedDate})
    console.log(user, "uzser")
  };

  const [password, setPassword] = useState(true);
  const [password2, setPassword2] = useState(true);
  const [password3, setPassword3] = useState(true);
  const [passwordErrorMsg, setPasswordErrorMsg] = useState("");
  const [emailErrorMsg, setEmailErrorMsg] = useState("");
  const [cities, setCities] = useState([]);
  const [cityValue, setCityValue] = useState("");
  const [buttonDisabled, setButtonDisabled] = useState(false);
  const [ktpVisibility, setKtpVisibility] = useState(false);
  const [selfieVisibility, setSelfieVisibility] = useState(false);
  const [visibleOverlay, setVisibleOverlay] = useState(false);
  const [selectedImage, setSelectedImage] = useState("");
  const [isImageLoading, setIsImageLoading] = useState(false);


  const toggleOverlay = (field = "none") => {
    setSelectedImage(field)
    setVisibleOverlay(!visibleOverlay);
  };

  const navigation = useNavigation();
  const [isLoaded] = useFonts({
    'avenir_next_bold': require('../assets/fonts/avenir_next_bold.ttf'),
    'avenir_next_medium': require('../assets/fonts/avenir_next_medium.ttf'),
  });

   // The path of the picked image
   const [pickedImagePath, setPickedImagePath] = useState('');

   // This function is triggered when the "Select an image" button pressed

  const [user, setUser] = useState({
    oldPassword: '',
    newPassword: '',
    confirmPassword: '',
    role: 'patient'
  })

  const [errorMessage, setErrorMessage] = useState({
    oldPassword: '',
    newPassword: '',
    confirmPassword: '',
  })

  const onChangePassword = (val) => {
    setUser({...user, password: val})
  }

  const submitData = async () => {
    try {
      const token = await AsyncStorage.getItem('@token')
      let isNotValid = false
      if (user.oldPassword == null || user.oldPassword == "") {
        isNotValid = true
        setErrorMessage({...errorMessage, oldPassword: "Harap isi password anda saat ini."})
      }
      if (user.newPassword.length < 8) {
        isNotValid = true
        setErrorMessage({...errorMessage, newPassword: "Password minimal harus 8 digit.", confirmPassword: ''})
      }
      if (user.newPassword == null || user.newPassword == "") {
        isNotValid = true
        setErrorMessage({...errorMessage, newPassword: "Harap isi password baru anda.", confirmPassword: ''})
      }
      if (user.confirmPassword != user.newPassword) {
        isNotValid = true
        setErrorMessage({...errorMessage, confirmPassword: "Konfirmasi password tidak sama."})
      }
      if (!isNotValid) {
        setErrorMessage({ oldPassword: '', newPassword: '', confirmPassword: '' })
        const res = await changePassword(token, user);
        if (res.data) {
          navigation.navigate("Profile")
        } else {
          Alert.alert(
            "Error",
            res,
            [
              { text: "OK", onPress: () => {}}
            ]
          );
        }
      }
    } catch (e) {
      console.log(e, "error")
    }
  }

  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return (
      <NativeBaseProvider>
      <BarHeader header="UBAH PASSWORD" />
        <ScrollView>
          <View style={styles.contentWrapper}>
            <View style={styles.input}>
              <Text style={styles.inputLabel}> Password Saat Ini </Text>
              <Input
              onChangeText={(val) => setUser({...user, oldPassword: val})}
              placeholder="Kata sandi"
              errorStyle={{ color: 'red' }}
              style={{paddingLeft: 8}}
              errorMessage={errorMessage.oldPassword}
              secureTextEntry={password}
              rightIcon={
                <Icon
                onPress={() => { 
                  setPassword(!password)
                 }}
                  name={password ? 'eye' : 'eye-off'}
                  size={24}
                  type='ionicon'
                  color='black'
                />
              } />
            </View>
            <Text style={{ width: "100%", height: 30, backgroundColor: "#e4e9f2", marginBottom: 10 }}></Text>
            <View style={styles.input}>
              <Text style={styles.inputLabel}> Password Baru </Text>
              <Input
              onChangeText={(val) => setUser({...user, newPassword: val})}
              placeholder="Kata sandi baru"
              errorStyle={{ color: 'red' }}
              errorMessage={errorMessage.newPassword}
              style={{paddingLeft: 8}}
              secureTextEntry={password2}
              rightIcon={
                <Icon
                onPress={() => { 
                  setPassword2(!password2)
                 }}
                  name={password2 ? 'eye' : 'eye-off'}
                  size={24}
                  type='ionicon'
                  color='black'
                />
              } />
            </View>
            <View style={styles.input}>
              <Text style={styles.inputLabel}> Konfirmasi Password Baru </Text>
              <Input
              onChangeText={(val) => setUser({...user, confirmPassword: val})}
              placeholder="Konfirmasi kata sandi baru"
              errorStyle={{ color: 'red' }}
              style={{paddingLeft: 8}}
              errorMessage={errorMessage.confirmPassword}
              secureTextEntry={password3}
              rightIcon={
                <Icon
                onPress={() => { 
                  setPassword3(!password3)
                 }}
                  name={password3 ? 'eye' : 'eye-off'}
                  size={24}
                  type='ionicon'
                  color='black'
                />
              } />
            </View>
            <View style={styles.buttonWrapper}>
              <TouchableOpacity disabled={buttonDisabled} onPress={() => submitData()}>
              <LinearGradient style={styles.buttonLogin} colors={['#F0AB00', '#C76A1E']}>
                <Text style={styles.buttonLoginLabel}>SIMPAN</Text>
              </LinearGradient>
            </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </NativeBaseProvider>
    );
  }
}

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 18,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderBottomWidth: 1,
    borderColor: 'gray',
    marginHorizontal: 10,
    marginBottom: 30,
    color: 'black',
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    width: width,
    height: height,
    justifyContent: 'center',
  },
  modalOverlay: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  overlayLabel: {
    fontFamily: "avenir_bold",
    alignSelf: 'center',
    textAlign: 'center',
    color: "gray",
    fontSize: 18,
    marginVertical: 20,
  },
  buttonOverlay: {
    padding: 20,
    fontSize: 14,
    borderRadius: 18,
    marginHorizontal: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  editImageLinkLayout: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: 15,
  },
  imageKTP: {
    width: "100%",
    height: "100%",
    resizeMode: "stretch"
  },
  imageLink: {
    color: "#800152"
  },
  dashedLayout: {
    width: 203,
    height: 134,
    padding: 10,
    backgroundColor: '#E8E8E8',
    borderStyle: 'dotted',
    borderWidth: 2,
    borderColor: '#A8A8A8',
    borderRadius: 7,
  },
  buttonLogin: {
    padding: 20,
    fontSize: 14,
    borderRadius: 18,
    marginHorizontal: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
  },
  layoutUnggah: {
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: 30,
  },
  input: {
    width: width - 44,
    padding: 10,
  },
  inputPassword: {
    height: 40,
    width: width - 44,
    marginBottom: 50,
    marginTop: 50,
    fontSize: 28,
    padding: 10,
  },
  inputLabel: {
    color: 'gray',
    paddingLeft: 8,
    paddingBottom: 15,
  },
  text: {
    fontSize: 46,
    color: '#800152',
    textAlign: 'center',
    fontFamily: 'avenir_next_bold',
    bottom: 30,
  },
  contentWrapper: {
    alignItems: 'center',
    marginTop: 20,
  },
  buttonWrapper: {
    width: width,
    marginBottom: 20,
  }
});
