import React, {useState, useEffect, useRef} from 'react';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity, SafeAreaView, Alert } from 'react-native';
import Svg, { SvgProps, Path } from "react-native-svg"
import BarHeader from '../components/BarHeader';
import { useFonts } from '@use-expo/font';
import RNPickerSelect from 'react-native-picker-select';
import { Input, NativeBaseProvider } from 'native-base';
import { Icon } from 'react-native-elements';
import {LinearGradient} from 'expo-linear-gradient';
import AppLoading from 'expo-app-loading';
import { useIsFocused, useNavigation } from '@react-navigation/native';
import BarFooter from '../components/BarFooter';
import { getOne } from '../services/api/program';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { checkTestLabCheckPoint } from '../helpers';
import PDFReader from 'rn-pdf-reader-js';
import Signature from 'react-native-signature-canvas';
import PDFLib, { PDFDocument, PDFPage } from 'react-native-pdf-lib';
import QRCode from 'react-native-qrcode-svg';
const {width, height} = Dimensions.get("window")

export default function PindaiQR() {

  const [address, setAddress] = useState("")
  const [programId, setProgramId] = useState("94c1b82b-eba6-41f8-93ec-0a6cc168d00c")
  
  const navigation = useNavigation()
  const isFocused = useIsFocused()
  const ref = useRef();
  
  useEffect(() => {
    getProgram()
  }, [isFocused])

  const navigateHome = async () => {
    const route = await checkTestLabCheckPoint()
    navigation.navigate(route.page, { fragment: route.fragment })
    
  }

  const getProgram = async () => {
    try {
      const program = await AsyncStorage.getItem('@UUID_PROGRAM')
      const token = await AsyncStorage.getItem('@token')
      setProgramId(program)
      const res = await getOne(token, program)
      if (res.data) {
        const pharmacy = res.data.pharmacy
        setAddress(`${pharmacy.name} \n ${pharmacy.address}`)
      } else {
        console.log(res)
        Alert.alert(res)
      }
    } catch (e) {
      console.log(e)
    }
  }

  const [isLoaded] = useFonts({
    'avenir_next_bold': require('../assets/fonts/avenir_next_bold.ttf'),
    'avenir_next_medium': require('../assets/fonts/avenir_next_medium.ttf'),
  });
  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return (
        <NativeBaseProvider style={styles.container}>
          <BarHeader header="SCAN QR" />
          <ScrollView>
              <View style={styles.contentWrapper}>
                <Text style={styles.sectionTitle}> Selamat, Program {"\n"} Pulih Anda telah{"\n"}disetujui </Text>
                <View style={styles.qrCodeWrapper}>
                  <QRCode
                    value={programId}
                    size={200}
                  />
                </View>
                <Text style={styles.sectionDescription}> Anda bisa mendapatkan{"\n"}obat dari Program PULIH di </Text>
                <Text style={styles.bottomText}> {address} </Text>
              </View>
          </ScrollView>
            <View style={styles.buttonWrapper}>
                <TouchableOpacity onPress={() => { navigateHome() }}>
                <LinearGradient style={styles.buttonLogin} colors={['#F0AB00', '#C76A1E']}>
                  <Text style={styles.buttonLoginLabel}>KEMBALI</Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
          {/* <BarFooter activeIcon="beranda" /> */}
        </NativeBaseProvider>
    );
  }
}

const styles = StyleSheet.create({
  pdf: {
    flex:1,
    width:Dimensions.get('window').width,
    height:Dimensions.get('window').height,
  },
  contentWrapper: {
    alignSelf: 'center',
    flexDirection: 'column',
    width: width -30,
    alignSelf: 'center',
    // background color must be set
  },
  qrCodeWrapper: {
    alignSelf: 'center',
    marginVertical: 21,
  },
  boardImage: {
    width: 360,
    height: 236
  },
  arrowImage: {
    flex: 1,
    width: 40,
    height: 38,
  },
  input: {
    alignSelf: 'center',
    borderRadius: 12,
    borderColor: "#979797",
    marginBottom: 80,
  },
  row: {
    flexDirection: "row",
    justifyContent: 'center'
  },
  container: {
    width,
    height,
    backgroundColor: "#FFF",
  },
  buttonWrapper: {
    width: width - 20,
    alignSelf: 'center',
    bottom: 10,
  },
  buttonLogin: {
    height: 55,
    width: "100%",
    fontSize: 14,
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
  },
  sectionTitle: {
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_bold",
    fontSize: 38,
    lineHeight: 50,
    letterSpacing: 0.13,
    marginTop: 41,
  },
  sectionDescription: {
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_next_medium",
    fontSize: 18,
    marginTop: 21,
    fontWeight: "500",
    marginBottom: 25,
    lineHeight: 20,
  },
  bottomText: {
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_bold",
    fontSize: 18,
    fontWeight: "500",
    lineHeight: 25,
    color: "#830051",
  },
});
