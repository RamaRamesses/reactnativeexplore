import React, {useState, useEffect} from 'react';
import AppLoading from 'expo-app-loading';
import { useFonts } from '@use-expo/font';
import { StyleSheet, SafeAreaView, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity, CheckBox, PickerIOSBase } from 'react-native';
import { Checkbox, Center, NativeBaseProvider } from "native-base"
import {LinearGradient} from 'expo-linear-gradient';
import { Input, Icon, Overlay } from 'react-native-elements';
const {width, height} = Dimensions.get("window")
import { Appbar as NativeAppBar } from 'react-native-paper';
import { getAll as getAllCity } from '../services/api/city'
import { register } from '../services/api/auth'
import { useNavigation, useIsFocused } from '@react-navigation/native';
import RNPickerSelect from 'react-native-picker-select';
import { useDispatch, useSelector } from 'react-redux';
import { checkTestLabCheckPoint } from '../helpers';
import { getById } from '../services/api/patient';
import BarHeader from '../components/BarHeader';
import AsyncStorage from '@react-native-async-storage/async-storage';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import { baseUrl } from '../config';
import ImageView from "react-native-image-viewing";

export default function Profile() {
  const navigation = useNavigation();
  const isFocused = useIsFocused();

  const [date, setDate] = useState(new Date(1598051730000));
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  const showTimepicker = () => {
    showMode('time');
  };

  const [isLoaded] = useFonts({
    'avenir_next_bold': require('../assets/fonts/avenir_next_bold.ttf'),
    'avenir_next_medium': require('../assets/fonts/avenir_next_medium.ttf'),
  });

  const [password, setPassword] = useState(true);
  const [passwordErrorMsg, setPasswordErrorMsg] = useState("");
  const [emailErrorMsg, setEmailErrorMsg] = useState("");
  const [cities, setCities] = useState([]);
  const [cityValue, setCityValue] = useState("");
  const [buttonDisabled, setButtonDisabled] = useState(false);
  const [ktpVisibility, setKtpVisibility] = useState(false);
  const [selfieVisibility, setSelfieVisibility] = useState(false);
useEffect(() => {
  getProfileById()
}, [isFocused])

  const [user, setUser] = useState({
    status: 1,
    email: '',
    password: '',
    city: '',
    idNumber: '',
    fullname: '',
    gender: '',
    code: '',
    dateOfBirth: ''
  })

  const logout = () => {
    AsyncStorage.clear()
    navigation.navigate("SplashScreen")
  }

  const onChangeEmail = (val) => {
    setUser({...user, email: val})
  }
  const onChangePassword = (val) => {
    setUser({...user, password: val})
  }
  const onChangeCity = (val) => {
    setUser({...user, city: val})
  }

  const getProfileById = async () => {
    const token = await AsyncStorage.getItem('@token')
    const patientId = await AsyncStorage.getItem('@UUID_PATIENT')
  
    if (patientId != null) {
      const res = await getById(token, patientId)
      if (res.data) {
        const body = res.data
        let user = {}
        if (body.id != null) {
          if (body.code != null) {
            user.code = body.code
          }
          if (body.dateOfBirth != null) {
            user.dateOfBirth = moment(body.dateOfBirth).format("YYYY-MM-DD")
          }
          if (body.gender != null) {
            user.gender = body.gender == "male" ? "Pria" : "Wanita"
          }
          if (body.fullname != null) {
            user.fullname = body.fullname
          }
          if (body.email != null) {
            user.email = body.email
          }
          if (body.idNumber != null) {
            user.idNumber = body.idNumber
          }
          if (body.representativeName != null) {
            user.representativeName = body.representativeName
          }
          if (body.representativePhone != null) {
            user.representativePhone = body.representativePhone
          }
          if (body.representativeRelationship != null) {
            user.representativeRelationship = body.representativeRelationship
          }
          if (body.idPicture != null) {
            user.idPicture = body.idPicture
          }
          if (body.selfiePicture != null) {
            user.selfiePicture = body.selfiePicture
          }
          setUser(user)
        } 
      }
    }
  }

  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return (
      <NativeBaseProvider>
      <BarHeader header="PROFIL" />
        <ScrollView>
          <View style={styles.contentWrapper}>
            <View style={styles.input}>
              <Text style={styles.inputLabel}> Id Pasien </Text>
              <Input
                disabled={true}
                onChangeText={(val) => onChangeEmail(val)}
                value={user.code}
                errorStyle={{ color: 'red' }}
                errorMessage={emailErrorMsg}
                style={{paddingLeft: 8}}
              />
            </View>
            <View style={styles.input}>
              <Text style={styles.inputLabel}> Nama lengkap sesuai KTP </Text>
              <Input
                value={user.fullname}
                disabled={true}
                errorStyle={{ color: 'red' }}
              errorMessage={passwordErrorMsg}
              style={{paddingLeft: 8}} />
            </View>
            <View style={styles.input}>
              <Text style={styles.inputLabel}> Tanggal lahir sesuai KTP </Text>
              <Input
                onFocus={showDatepicker}
                errorStyle={{ color: 'red' }}
                value={user.dateOfBirth}
                disabled={true}
              errorMessage={passwordErrorMsg}
              style={{paddingLeft: 8}} />
                {/* <DateTimePicker
                  testID="dateTimePicker"
                  value={date}
                  mode={mode}
                  is24Hour={true}
                  style={{ width: "100%", marginLeft: 10 }}
                  display="default"
                  onChange={onChange}
                /> */}
            </View>
            <View style={styles.input}>
              <Text style={styles.inputLabel}> No KTP </Text>
              <Input
                disabled={true}
                value={user.idNumber}
                onChangeText={(val) => onChangePassword(val)}
              errorStyle={{ color: 'red' }}
              errorMessage={passwordErrorMsg}
              style={{paddingLeft: 8}} />
            </View>
            <View style={styles.layoutUnggah}>
              <Text style={styles.inputLabel}> Foto KTP </Text>
                <TouchableOpacity onPress={() => setKtpVisibility(true)} style={styles.dashedLayout}>
                  {!user.idPicture ? (
                    <Image
                    resizeMode="contain"
                    style={styles.imageKTP}
                    source={require('../assets/onboarding_login.png')}
                  />
                  ) : (
                    <Image
                    style={styles.imageKTP}
                    source={{
                      uri: baseUrl + user.idPicture
                    }}
                    />
                  )}
                    <ImageView
                      images={[{
                        uri: baseUrl + user.idPicture
                      }]}
                      imageIndex={0}
                      visible={ktpVisibility}
                      onRequestClose={() => setKtpVisibility(false)}
                    />
                </TouchableOpacity>
            </View>
            <View style={styles.layoutUnggah}>
              <Text style={styles.inputLabel}> Foto Selfie </Text>
                <TouchableOpacity onPress={() => setSelfieVisibility(true)} style={styles.dashedLayout}>
                {!user.selfiePicture ? (
                    <Image
                    resizeMode="contain"
                    style={styles.imageKTP}
                    source={require('../assets/onboarding_login.png')}
                  />
                  ) : (
                    <Image
                    style={styles.imageKTP}
                    source={{
                      uri: baseUrl + user.selfiePicture
                    }}
                  />
                  )}
                  <ImageView
                      images={[{
                        uri: baseUrl + user.selfiePicture
                      }]}
                      imageIndex={0}
                      visible={selfieVisibility}
                      onRequestClose={() => setSelfieVisibility(false)}
                    />
                </TouchableOpacity>
            </View>
            <View style={styles.input}>
              <Text style={styles.inputLabel}> Jenis Kelamin </Text>
              <Input
                disabled={true}
                value={user.gender}
                errorStyle={{ color: 'red' }}
              errorMessage={passwordErrorMsg}
              style={{paddingLeft: 8}} />
            </View>
            <View style={styles.input}>
              <Text style={styles.inputLabel}> Email </Text>
              <Input
                disabled={true}
                value={user.email}
                errorStyle={{ color: 'red' }}
              errorMessage={passwordErrorMsg}
              style={{paddingLeft: 8}} />
            </View>
            <Text style={{ width: "100%", height: 30, backgroundColor: "#e4e9f2", marginBottom: 10 }}></Text>
            <View style={styles.input}>
              <Text style={styles.inputLabel}> Nama Perwakilan </Text>
              <Input
                disabled={true}
                value={user.representativeName}
                errorStyle={{ color: 'red' }}
              errorMessage={passwordErrorMsg}
              style={{paddingLeft: 8}} />
            </View>
            <View style={styles.input}>
              <Text style={styles.inputLabel}> Telepon </Text>
              <Input
                disabled={true}
                value={user.representativePhone}
                errorStyle={{ color: 'red' }}
              errorMessage={passwordErrorMsg}
              style={{paddingLeft: 8}} />
            </View>
            <View style={styles.input}>
              <Text style={styles.inputLabel}> Hubungan </Text>
              <Input
                disabled={true}
                value={user.representativeRelationship}
                errorStyle={{ color: 'red' }}
              errorMessage={passwordErrorMsg}
              style={{paddingLeft: 8}} />
            </View>
            <View style={styles.buttonWrapper}>
              <TouchableOpacity disabled={buttonDisabled} onPress={() => navigation.navigate("EditProfile", { redirect: false })}>
              <LinearGradient style={styles.buttonLogin} colors={['#F0AB00', '#C76A1E']}>
                <Text style={styles.buttonLoginLabel}>UBAH PROFIL</Text>
              </LinearGradient>
            </TouchableOpacity>
            </View>
            <View style={styles.buttonWrapper}>
              <TouchableOpacity disabled={buttonDisabled} onPress={() => navigation.navigate("ChangePassword")}>
              <LinearGradient style={styles.buttonLogin} colors={['#F0AB00', '#C76A1E']}>
                <Text style={styles.buttonLoginLabel}>UBAH PASSWORD</Text>
              </LinearGradient>
            </TouchableOpacity>
            </View>
            <View style={styles.buttonWrapper}>
              <TouchableOpacity disabled={buttonDisabled} onPress={logout}>
              <LinearGradient style={styles.buttonLogin} colors={['#d63031', '#b33939']}>
                <Text style={styles.buttonLoginLabel}>LOGOUT</Text>
              </LinearGradient>
            </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </NativeBaseProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    width: width,
    height: height,
    justifyContent: 'center',
  },
  imageKTP: {
    width: "100%",
    height: "100%",
    resizeMode: "stretch"
  },
  dashedLayout: {
    width: 203,
    height: 134,
    padding: 10,
    backgroundColor: '#E8E8E8',
    borderStyle: 'dotted',
    borderWidth: 2,
    borderColor: '#A8A8A8',
    borderRadius: 7,
  },
  layoutUnggah: {
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: 30,
  },
  input: {
    width: width - 44,
    padding: 10,
  },
  inputPassword: {
    height: 40,
    width: width - 44,
    marginBottom: 50,
    marginTop: 50,
    fontSize: 28,
    padding: 10,
  },
  inputLabel: {
    color: 'gray',
    paddingLeft: 8,
    paddingBottom: 15,
  },
  text: {
    fontSize: 46,
    color: '#800152',
    textAlign: 'center',
    fontFamily: 'avenir_next_bold',
    bottom: 30,
  },
  contentWrapper: {
    alignItems: 'center',
    marginTop: 20,
  },
  buttonWrapper: {
    width: width,
    marginBottom: 20,
  },
  buttonLogin: {
    padding: 20,
    fontSize: 14,
    borderRadius: 18,
    marginHorizontal: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
  },
});
