import { StatusBar } from 'expo-status-bar';
import React, {useState} from 'react';
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font'
import { useFonts } from '@use-expo/font';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import {LinearGradient} from 'expo-linear-gradient';
import * as Google from "expo-google-app-auth";
import { getByEmail } from '../services/api/patient';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as firebase from 'firebase';
import * as Facebook from 'expo-facebook';
const {width, height} = Dimensions.get("window")

const fetchFonts = () => {
  return Font.loadAsync({
    'avenir-next-demibold': require('../assets/fonts/avenir_next_demibold.ttf'),
  });
};

var firebaseConfig = {
  apiKey: "AIzaSyCJ9j65r-8QsjT6d189eER0j25Uc0pz5gI",
  authDomain: "papanalytics.firebaseapp.com",
  databaseURL: "https://papanalytics.firebaseio.com",
  projectId: "papanalytics",
  storageBucket: "papanalytics.appspot.com",
  messagingSenderId: "731779980494",
  appId: "1:731779980494:ios:73b194e74199380553849f"
};

if (!firebase.apps.length)
firebase.initializeApp(firebaseConfig);

// Listen for authentication state to change.
firebase.auth().onAuthStateChanged((user) => {
  if (user) {
    console.log("Logged in with user: ", user);
  } else {
    console.log('Not logged in')
  }
});

export default function OnBoardingLogin() {
  const navigation = useNavigation();

  const handleAuth = async () => {
    try {
      await Facebook.initializeAsync({appId: '1511332045877677'}); // enter your Facebook App Id 
      const { type, token } = await Facebook.logInWithReadPermissionsAsync({
        permissions: ['public_profile', 'email'],
      });
      if (type === 'success') {
        // Get the user's name using Facebook's Graph API
        const credential = firebase.auth.FacebookAuthProvider.credential(token);
        firebase.auth().signInWithCredential(credential)
          .then(user => {
            console.log('Logged in successfully', user)
          })
          .catch((error) => {
            console.log('Error occurred ', error)
          });
      } else {
        // type === 'cancel'
        console.log("error")
      }
    } catch ({ message }) {
      alert(`Facebook Login Error: ${message}`);
    }
  }

  const signInAsync = async () => {
    console.log("LoginScreen.js 6 | loggin in");
    try {
      const { type, user } = await Google.logInAsync({
        clientId: '731779980494-qf7j0q9hlms6npbnha8fe22nnljs2hh3.apps.googleusercontent.com'
      });
  
      if (type === "success") {
        // Then you can use the Google REST API
        console.log("LoginScreen.js 17 | success, navigating to profile");
        console.log(user, "user")
        const res = await getByEmail(user.email)
        if (res.data) {
          const jsonValue = JSON.stringify(res.data)
          await AsyncStorage.setItem('@patient', jsonValue)
          saveToken(res.headers.authorization, res.data.id, res.data.code, res.data.fullname)
          saveActiveTestLab(res.data)
          navigation.navigate('SplashScreen')
      } else {
          console.log(res)
        }
      }
    } catch (error) {
      console.log("LoginScreen.js 19 | error with login", error);
    }
  };
  
  const saveActiveTestLab = async (patient) => {
    console.log(patient, "patient")
    for (const i in patient.testLabs) {
      if (patient.testLabs[i].status == 1) {
        try {
          await AsyncStorage.setItem("@TES_LAB_ID", patient.testLabs[i].id.toString())
          if (patient.testLabs[i].doctor) await AsyncStorage.setItem("@UUID_DOKTER", patient.testLabs[i].doctor.id)
          if (patient.testLabs[i].laboratorium) await AsyncStorage.setItem("@UUID_LABORATORIUM",patient.testLabs[i].laboratorium.id)
          if (patient.testLabs[i].testLabType) await AsyncStorage.setItem("@UUID_TES_LAB_TYPE", patient.testLabs[i].testLabType.id)
          if (patient.testLabs[i].testLabType) await AsyncStorage.setItem("@TES_LAB_NAME", patient.testLabs[i].testLabType.name)
        } catch (err) {
          console.log(err)
        }
      }
    }
  
    for (const i in patient.programs) {
      const program = patient.programs[i]
      if (patient.programs[i].status == 1) {
        await AsyncStorage.setItem("@UUID_PROGRAM", program.id)
        if (program.doctor) await AsyncStorage.setItem("@PROGRAM_UUID_DOCTOR", program.doctor.id)
        if (program.doctor) await AsyncStorage.setItem("@PROGRAM_NAMA_DOKTER", program.doctor.fullname)
        if (program.programType) await AsyncStorage.setItem("@NAMA_PROGRAM_TYPE", program.programType.name)
        if (program.programType) await AsyncStorage.setItem("@UUID_PROGRAM_TYPE", program.programType.id)
        console.log(program, "program")
      }
    }
  }
  
  const saveToken = async (token, id, code, name) => {
    try {
      await AsyncStorage.setItem("@token", token)
      await AsyncStorage.setItem("@UUID_PATIENT", id)
      await AsyncStorage.setItem("@CODE_PATIENT", code)
      await AsyncStorage.setItem("@NAME_PATIENT", name)
    } catch (e) {
      console.log(e, "err")
    }
  }
  const [isLoaded] = useFonts({
    'avenir_next_demibold': require('../assets/fonts/avenir_next_demibold.ttf'),
  });
  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return (
      <LinearGradient colors={['#830051', '#1d2671']} style={styles.container}>
        <ScrollView>
        <View style={styles.contentWrapper}>
          <Image
            style={styles.boardImage}
            resizeMode="contain"
            source={require('../assets/onboarding_login.png')}
          />
          <Text
          style={styles.text}>
            Selamat datang di{"\n"} aplikasi Pulih.{"\n"} Silahkan telusuri
          </Text>
          <View style={styles.buttonLayout}>
          <TouchableOpacity style={{ flex: 15}} onPress={() => navigation.navigate('SignIn')}>
            <LinearGradient style={styles.buttonLogin} colors={['#F0AB00', '#C76A1E']}>
              <Text style={styles.buttonLoginLabel}>MASUK</Text>
            </LinearGradient>
          </TouchableOpacity>
          <View style={{ flex: 1 }} />
          <TouchableOpacity onPress={() => navigation.navigate('SignUp')} style={{ flex: 15 }} >
            <LinearGradient style={styles.buttonRegister} colors={['#FFF', '#FFF']}>
              <Text style={styles.buttonRegisterLabel}>DAFTAR</Text>
            </LinearGradient>
          </TouchableOpacity>
          </View>
          <View style={styles.socialButtons}>
          <TouchableOpacity onPress={signInAsync}>
            <LinearGradient style={styles.buttonGoogle} colors={['#FFF', '#FFF']}>
              <Image style={styles.googleIcon} source={require('../assets/Google__G__Logo.png')} />
              <Text style={styles.buttonGoogleLabel}>Masuk dengan Google</Text>
            </LinearGradient>
          </TouchableOpacity>
          <TouchableOpacity onPress={handleAuth}>
            <LinearGradient style={styles.buttonFacebook} colors={['#1873EB', '#1873EB']}>
              <Image style={styles.facebookIcon} source={require('../assets/facebook.png')} />
              <Text style={styles.buttonFacebookLabel}>Masuk dengan Facebook</Text>
            </LinearGradient>
          </TouchableOpacity>
          </View>
        </View>
        </ScrollView>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    width: width,
    justifyContent: 'center',
  },
  text: {
    fontSize: 30,
    color: '#FFFFFF',
    textAlign: 'center',
    fontFamily: 'avenir_next_demibold',
    bottom: 30,
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 18,
    color: 'white',
  },
  socialButtons: {
    flexDirection: 'column',
    marginTop: 15,
    alignItems: 'center',
    maxWidth: width - 15,
  },
  buttonGoogle: {
    padding: 20,
    width: '100%',
    fontSize: 16,
    marginBottom: 15,
    borderRadius: 15,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonFacebook: {
    padding: 20,
    width: '100%',
    fontSize: 16,
    marginBottom: 30,
    borderRadius: 15,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonLogin: {
    padding: 20,
    fontSize: 16,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonRegister: {
    padding: 20,
    fontSize: 16,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonRegisterLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 18,
    color: '#710757',
  },
  boardImage: {
    resizeMode: 'contain',
    width: width - 80,
  },
  googleIcon: {
    resizeMode: 'contain',
    width: 24,
    height: 24,
    flex: 1,
  },
  facebookIcon: {
    resizeMode: 'contain',
    width: 24,
    height: 24,
    flex: 1,
  },
  buttonGoogleLabel: {
    flex: 10,
    fontSize: 22,
    textAlign: 'center',
    color: '#8C949A',
    fontFamily: 'avenir_next_demibold',
  },
  buttonFacebookLabel: {
    flex: 10,
    fontSize: 22,
    textAlign: 'center',
    color: '#FFFFFF',
    fontFamily: 'avenir_next_demibold',
  },
  buttonLayout: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingHorizontal: 10,
    marginTop: 10,
    width: width,
  },
  contentWrapper: {
    flexDirection: 'column',
    alignItems: 'center',
  }
});
