import React, {useState, useEffect, useRef} from 'react';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity, SafeAreaView } from 'react-native';
import Svg, { SvgProps, Path } from "react-native-svg"
import BarHeader from '../components/BarHeader';
import { useFonts } from '@use-expo/font';
import RNPickerSelect from 'react-native-picker-select';
import { Input, NativeBaseProvider } from 'native-base';
import { Icon } from 'react-native-elements';
import {LinearGradient} from 'expo-linear-gradient';
import AppLoading from 'expo-app-loading';
import { useNavigation } from '@react-navigation/native';
import BarFooter from '../components/BarFooter';
import { getOne } from '../services/api/testLab';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { checkTestLabCheckPoint } from '../helpers';
import PDFReader from 'rn-pdf-reader-js';
import Signature from 'react-native-signature-canvas';
import PDFLib, { PDFDocument, PDFPage } from 'react-native-pdf-lib';
const {width, height} = Dimensions.get("window")

export default function TestLabPermintaanDiterima() {

  const [description, setDescription] = useState("")

  useEffect(() => {
    getTestLab()
  }, [])

  const navigation = useNavigation()
  const ref = useRef();

  const navigateHome = async () => {
    const route = await checkTestLabCheckPoint()
    navigation.navigate(route.page, { fragment: route.fragment })
    
  }

  const getTestLab = async () => {
    try {
      const token = await AsyncStorage.getItem('@token')
      const testLabId = await AsyncStorage.getItem('@TES_LAB_ID')
      const res = await getOne(token, testLabId)
      if (res.data) {
        if (res.data.laboratorium) {
          setDescription(res.data.laboratorium.address)
        } else {
          console.log(res.data)
        }
      } else {
        console.log(res)
      }
    } catch (e) {
      console.log(e)
    }
  }

  // Called after ref.current.readSignature() reads a non-empty base64 string
  const handleOK = (signature) => {
    console.log(signature);
    onOK(signature); // Callback from Component props
  };

  // Called after ref.current.readSignature() reads an empty string
  const handleEmpty = () => {
    console.log("Empty");
  };

  // Called after ref.current.clearSignature()
  const handleClear = () => {
    console.log("clear success!");
  };

  // Called after end of stroke
  const handleEnd = () => {
    ref.current.readSignature();
  };

  // Called after ref.current.getData()
  const handleData = (data) => {
    console.log(data);
  };

  const [isLoaded] = useFonts({
    'avenir_next_bold': require('../assets/fonts/avenir_next_bold.ttf'),
    'avenir_next_medium': require('../assets/fonts/avenir_next_medium.ttf'),
  });
  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return (
        <NativeBaseProvider style={styles.container}>
          <ScrollView>
            <View>
              <View style={styles.contentWrapper}>
                <Image
                  style={styles.boardImage}
                  resizeMode="contain"
                  source={require('../assets/dokumen_diterima.png')}
                />
                <Text style={styles.sectionTitle}> Permintaan {"\n"} Diterima </Text>
                <Text style={styles.sectionDescription}> Silakan melakukan tes kesehatan di  </Text>
                <Text style={styles.bottomText}> {description} </Text>
              </View>
            </View>
          </ScrollView>
          <View style={styles.buttonWrapper}>
              <TouchableOpacity onPress={() => { navigateHome() }}>
              <LinearGradient style={styles.buttonLogin} colors={['#F0AB00', '#C76A1E']}>
                <Text style={styles.buttonLoginLabel}>LANJUTKAN</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
          <BarFooter activeIcon="beranda" />
        </NativeBaseProvider>
    );
  }
}

const styles = StyleSheet.create({
  pdf: {
    flex:1,
    width:Dimensions.get('window').width,
    height:Dimensions.get('window').height,
  }
,  contentWrapper: {
    alignSelf: 'center',
    flexDirection: 'column',
    width: width -30,
    marginTop: 22,
    alignSelf: 'center',
    marginTop: 55,
    paddingVertical: 21,
    paddingHorizontal: 17,
    // background color must be set
  },
  boardImage: {
    width: 360,
    height: 236
  },
  arrowImage: {
    flex: 1,
    width: 40,
    height: 38,
  },
  input: {
    alignSelf: 'center',
    borderRadius: 12,
    borderColor: "#979797",
    marginBottom: 80,
  },
  row: {
    flexDirection: "row",
    justifyContent: 'center'
  },
  container: {
    width,
    height,
    backgroundColor: "#FFF",
  },
  buttonWrapper: {
    width: width - 20,
    alignSelf: 'center',
    bottom: 20,
  },
  buttonLogin: {
    height: 55,
    width: "100%",
    fontSize: 14,
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
  },
  sectionTitle: {
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_bold",
    fontSize: 38,
    lineHeight: 50,
    letterSpacing: 0.13,
    marginTop: 21,
  },
  sectionDescription: {
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_next_medium",
    fontSize: 18,
    marginTop: 21,
    fontWeight: "500",
    marginBottom: 25,
    lineHeight: 20,
  },
  bottomText: {
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_bold",
    fontSize: 18,
    fontWeight: "500",
    lineHeight: 25,
    color: "#830051",
  },
});
