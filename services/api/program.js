import apiClient from '../axios'

const getOne = async (token, id) => {
  try {
    const res = await apiClient.get(`/program/${id}`, {
        headers: {
          Authorization: token,
        }
      })
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

const create = async (token, data) => {
  try {
    const res = await apiClient.post('/program', data, {
      headers: {
        Authorization: token,
      }
    })
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

const continueProgram = async (token, data) => {
  try {
    const res = await apiClient.post(`/program/continue`, data, {
      headers: {
        Authorization: token,
      }
    })
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

const terminateProgram = async (token, data) => {
  try {
    const res = await apiClient.post(`/program/terminate`, data, {
      headers: {
        Authorization: token,
      }
    })
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

const confirm = async (token, data) => {
  try {
    const res = await apiClient.post(`/program/confirm`, data, {
      headers: {
        Authorization: token,
      }
    })
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

const updateTestLab = async (token, data) => {
  try {
    const res = await apiClient.post(`/program/updateTestLab`, data, {
      headers: {
        Authorization: token,
      }
    })
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

const updateInsurance = async (token, data) => {
  try {
    const res = await apiClient.post(`/program/updateInsurance`, data, {
      headers: {
        Authorization: token,
      }
    })
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

export {
  create,
  continueProgram,
  terminateProgram,
  confirm,
  updateTestLab,
  updateInsurance,
  getOne
}