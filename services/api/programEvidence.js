import apiClient from '../axios'
const create = async (token, data) => {
  try {
    const res = await apiClient.post('/programEvidence', data, {
      headers: {
        Authorization: token,
      }
    })
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

export {
  create
}