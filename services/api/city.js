import apiClient from '../axios'
const getAll = async (data) => {
  const res = await apiClient.get('/city', data, {})
  if (res) {
    return res
  } else {
    return null
  }
}

export {
    getAll
}