import apiClient from '../axios'
const upload = async (data) => {
  const res = await apiClient.post('/upload/id', data, {
      headers: {
          'Content-Type': 'multipart/form-data'
      }
  })
  if (res) {
    return res
  } else {
    return null
  }
}

const uploadSignature = async (name, program, data) => {
  const res = await apiClient.post(`/upload/signature?name=${name}&program=${program}`, data, {
      headers: {
          'Content-Type': 'multipart/form-data'
      }
  })
  if (res) {
    return res
  } else {
    return null
  }
}

const uploadTestLabEvidence = async (data, onUploadProgress) => {
  const res = await apiClient.post('/upload/id', data, {
      headers: {
          'Content-Type': 'multipart/form-data'
      },
      onUploadProgress
  })
  if (res) {
    return res
  } else {
    return null
  }
}

const uploadResep = async (token, data) => {
  const res = await apiClient.post('/upload/prescription', data, {
      headers: {
        Authorization: token,
        'Content-Type': 'multipart/form-data'
      }
  })
  if (res) {
    return res
  } else {
    return null
  }
}

export {
    upload,
    uploadTestLabEvidence,
    uploadSignature,
    uploadResep
}