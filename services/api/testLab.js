import apiClient from '../axios'

const createTest = async (data) => {
  try {
    const res = await apiClient.post('/testLab', data, {})
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

const createSelfTest = async (data) => {
  try {
    const res = await apiClient.post('/testLab/self-test', data, {})
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

const getOne = async (token, id) => {
  try {
    const res = await apiClient.get(`/testLab/${id}`, {
      headers: {
        Authorization: token,
      }
    })
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

export {
  createTest,
  createSelfTest,
  getOne
}