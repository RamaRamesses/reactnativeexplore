import apiClient from '../axios'
const getAll = async (token) => {
  try {
    const res = await apiClient.get('/laboratorium', {
      headers: {
        Authorization: token,
      }
    })
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

export {
    getAll
}