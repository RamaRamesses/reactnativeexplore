import apiClient from '../axios'
const getAll = async (token) => {
  try {
    const res = await apiClient.get('/news/category/general', {
      headers: {
        Authorization: token,
      }
    })
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

const getById = async (token, id) => {
  try {
    const res = await apiClient.get(`/news/${id}`, {
      headers: {
        Authorization: token,
      }
    })
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

const getByTag = async (token, tag) => {
  try {
    const res = await apiClient.get(`/news/category/tag?tag=${tag}`, {
      headers: {
        Authorization: token,
      }
    })
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

const getAllByProgram = async (token, program) => {
  try {
    const res = await apiClient.get(`/news/category/program?tag=${program}`, {
      headers: {
        Authorization: token,
      }
    })
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

const getAllTags = async () => {
  try {
    const res = await apiClient.get(`/news/category/getAllTags`, {
    })
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

export {
  getAll,
  getById,
  getAllTags,
  getByTag,
  getAllByProgram
}