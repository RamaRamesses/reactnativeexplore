import apiClient from '../axios'
const getById = async (token, id) => {
  try {
    const res = await apiClient.get(`/notification/${id}`, {
      headers: {
        Authorization: token,
      }
    })
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

export {
  getById
}