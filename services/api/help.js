import apiClient from '../axios'
const getAll = async () => {
  try {
    const res = await apiClient.get('/help')
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

export {
  getAll
}