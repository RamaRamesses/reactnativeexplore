import apiClient from '../axios'

const getById = async (token, id) => {
  try {
    const res = await apiClient.get(`/patient/${id}`, {
        headers: {
          Authorization: token,
        }
      })
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

const getByEmail = async (email) => {
  try {
    const res = await apiClient.get(`/patient/socialLogin/email?email=${email}`, {})
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

const validate = async (id) => {
  try {
    const res = await apiClient.get(`/patient/account/validate?id=${id}`, {})
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

const updateById = async (token, id, data) => {
  try {
    const res = await apiClient.patch(`/patient/${id}`, data, {
      headers: {
        Authorization: token,
      }
    })
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

export {
  getById,
  validate,
  updateById,
  getByEmail
}