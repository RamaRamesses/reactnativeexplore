import apiClient from '../axios'
const login = async (data) => {
  try {
    const res = await apiClient.post('/auth/login', data, {})
    return res
  } catch (e) {
    console.log(e, "E")
    return e.response.data.errorList[0]
  }
}

const changePassword = async (token, data) => {
  try {
    const res = await apiClient.post('/auth/change-password', data, {
      headers: {
        Authorization: token,
      }
    })
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

const register = async (data) => {
  try {
    const res = await apiClient.post('/patient', data, {})
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

export {
  login,
  register,
  changePassword
}