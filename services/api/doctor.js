import apiClient from '../axios'
const getAll = async (token) => {
  try {
    const res = await apiClient.get('/doctor', {
      headers: {
        Authorization: token,
      }
    })
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

const getByType = async (token, type) => {
  try {
    const res = await apiClient.get(`/doctor/type/${type}`, {
      headers: {
        Authorization: token,
      }
    })
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

export {
    getAll,
    getByType
}