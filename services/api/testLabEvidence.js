import apiClient from '../axios'
const create = async (data) => {
  try {
    const res = await apiClient.post('/testLabEvidence', data, {})
    return res
  } catch (e) {
    return e.response.data.errorList[0]
  }
}

export {
  create
}