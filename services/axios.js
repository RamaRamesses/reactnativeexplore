import { baseUrl } from '../config';

const axios = require('axios');
const apiClient = axios.create({
  baseURL: baseUrl,
})

// apiClient.interceptors.request.use(request => {
//   const accessToken = store.get('accessToken')
//   if (accessToken) {
//     request.headers.Authorization = `Bearer ${accessToken}`
//     request.headers.AccessToken = accessToken
//   }
//   return request
// })
  
  export default apiClient