import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font'
import { useFonts } from '@use-expo/font';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Svg, { SvgProps, Path } from "react-native-svg"
import { useNavigation } from '@react-navigation/native';
import {LinearGradient} from 'expo-linear-gradient';
import { marginTop } from 'styled-system';
import { validate } from '../../services/api/patient';
const {width, height} = Dimensions.get("window")

export default function PilihTesLab() {

  const navigation = useNavigation()

  const checkAccountValidation = async (intent) => {
    try {
      const patientId = await AsyncStorage.getItem('@UUID_PATIENT')
      if (patientId != null) {
        const res = await validate(patientId)
        if (res.data) {
          if (res.data.status == "valid") {
            if (intent == "self") {
              navigation.navigate("PilihTesLaboratorium", { type: 'self' })
            } else {
              navigation.navigate("PilihTesLaboratorium", { type: 'regular' })
            }
          } else {
            navigation.navigate("EditProfile", { redirect: true })
          }
        }
      }
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <View colors={['#FFF', '#FFF']}  style={styles.roundRect}>
      <Image
        style={styles.boardImage}
        resizeMode="contain"
        source={require('../../assets/belum_testlab_status_icon.png')}
      />
      <View style={styles.labelsLayout}>
        <Text style={styles.sectionTitle}>Apakah Anda Sudah Memiliki {"\n"} Hasil Tes Laboratorium? </Text>
        <TouchableOpacity onPress={() => checkAccountValidation('self')}>
          <LinearGradient style={styles.buttonLab} colors={['#F0AB00', '#C76A1E']}>
            <Text style={styles.buttonLoginLabel}>SUDAH TES MANDIRI</Text>
          </LinearGradient>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => checkAccountValidation('regular')}>
          <LinearGradient style={styles.buttonLab} colors={['#830051', '#830051']}>
            <Text style={styles.buttonLoginLabel}>BELUM AJUKAN TES</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  roundRect: {
    flex: 1,
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    height: 180,
    top: 100,
    width: width -30,
    marginTop: 20,
    backgroundColor: "#FFF",
    borderRadius: 15,
    position: 'absolute',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
    // background color must be set
  },
  labelsLayout: {
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  sectionTitle: {
    fontFamily: 'avenir_bold',
    fontSize: 17,
    textAlign: 'center',
  },
  boardImage: {
    width: 130,
    height: 130,
  },
  buttonLab: {
    paddingVertical: 10,
    paddingHorizontal: 30,
    borderRadius: 10,
    alignSelf: 'center',
    marginTop: 10,
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
  },
});
