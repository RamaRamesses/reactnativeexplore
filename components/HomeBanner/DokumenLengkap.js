import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font'
import { useFonts } from '@use-expo/font';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Svg, { SvgProps, Path } from "react-native-svg"
import { useNavigation } from '@react-navigation/native';
import {LinearGradient} from 'expo-linear-gradient';
import { marginTop } from 'styled-system';
const {width, height} = Dimensions.get("window")

export default function DokumenLengkap() {

  const navigation = useNavigation()

  const nextButton = async () => {
    await AsyncStorage.removeItem('@PROGRAM_STATUS')
    navigation.navigate("PilihJenisProgram")
  }

  return (
    <View colors={['#FFF', '#FFF']}  style={styles.roundRect}>
      <Image
        style={styles.boardImage}
        resizeMode="contain"
        source={require('../../assets/dokumen_diterima.png')}
      />
      <View style={styles.labelsLayout}>
        <Text style={styles.sectionTitle}>Dokumen Anda telah {"\n"} lengkap. Silahkan {"\n"} lanjutkan ke Program {"\n"} PULIH. </Text>
        <TouchableOpacity onPress={() => nextButton()}>
          <LinearGradient style={styles.buttonLogin} colors={['#830051', '#830051']}>
            <Text style={styles.buttonLoginLabel}>AJUKAN PROGRAM</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  roundRect: {
    flex: 1,
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    height: 180,
    top: 100,
    width: width -30,
    marginTop: 20,
    backgroundColor: "#FFF",
    borderRadius: 15,
    position: 'absolute',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
    // background color must be set
  },
  labelsLayout: {
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  sectionTitle: {
    fontFamily: 'avenir_bold',
    fontSize: 16,
    alignSelf: 'flex-start',
  },
  boardImage: {
    width: 130,
    height: 130,
  },
  buttonLogin: {
    padding: 20,
    borderRadius: 15,
    alignSelf: 'flex-start',
    marginTop: 10,
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
  },
});
