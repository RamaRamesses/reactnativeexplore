import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font'
import { useFonts } from '@use-expo/font';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Svg, { SvgProps, Path } from "react-native-svg"
import { useNavigation } from '@react-navigation/native';
import {LinearGradient} from 'expo-linear-gradient';
import { marginTop } from 'styled-system';
const {width, height} = Dimensions.get("window")

export default function TungguVerifikasi() {
  return (
    <View colors={['#FFF', '#FFF']}  style={styles.roundRect}>
      <View style={styles.labelsLayout}>
      <Image
        style={styles.boardImage}
        resizeMode="contain"
        source={require('../../assets/verify_status_icon.png')}
      />
        <Text style={styles.sectionTitle}>Pengajuan program Anda sedang diverifikasi</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  roundRect: {
    flex: 1,
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    height: 180,
    top: 100,
    width: width -30,
    marginTop: 20,
    backgroundColor: "#FFF",
    borderRadius: 15,
    position: 'absolute',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
    // background color must be set
  },
  labelsLayout: {
  },
  sectionTitle: {
    fontFamily: 'avenir_bold',
    fontSize: 22,
    textAlign: 'center',
  },
  boardImage: {
    width: 230,
    height: 120,
    alignSelf: 'center',
  },
  buttonLab: {
    paddingVertical: 15,
    paddingHorizontal: 80,
    borderRadius: 10,
    alignSelf: 'center',
    marginTop: 20,
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
  },
});
