import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font'
import { useFonts } from '@use-expo/font';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Svg, { SvgProps, Path } from "react-native-svg"
import { useNavigation } from '@react-navigation/native';
import {LinearGradient} from 'expo-linear-gradient';
import { justifyContent, marginTop } from 'styled-system';
const {width, height} = Dimensions.get("window")

export default function AturJadwal() {
  return (
    <View colors={['#FFF', '#FFF']}  style={styles.roundRect}>
      <Image
        style={styles.boardImage}
        resizeMode="contain"
        source={require('../../assets/atur_jadwal_home_status_icon_2.png')}
      />
      <View style={styles.labelsLayout}>
        <Text style={styles.sectionTitle}>Minum obat Tagrisso {"\n"}sesuai dengan resep dokter </Text>
        <TouchableOpacity>
          <LinearGradient style={styles.buttonLogin} colors={['#830051', '#830051']}>
            <Text style={styles.buttonLoginLabel}>ATUR JADWAL MINUM OBAT</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  roundRect: {
    flex: 1,
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    height: 180,
    top: 100,
    width: width -30,
    marginTop: 20,
    backgroundColor: "#FFF",
    borderRadius: 15,
    position: 'absolute',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
    // background color must be set
  },
  labelsLayout: {
    flexDirection: 'column',
    justifyContent: 'center',
  },
  sectionTitle: {
    fontFamily: 'avenir_bold',
    fontSize: 17,
    textAlign: 'center',
    alignSelf: 'center',
  },
  boardImage: {
    width: 115,
    height: 130,
  },
  buttonLogin: {
    padding: 18,
    borderRadius: 15,
    alignSelf: 'center',
    marginTop: 10,
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 13,
    color: 'white',
  },
});
