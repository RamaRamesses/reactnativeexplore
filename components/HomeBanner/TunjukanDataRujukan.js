import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font'
import { useFonts } from '@use-expo/font';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Svg, { SvgProps, Path } from "react-native-svg"
import { useNavigation } from '@react-navigation/native';
import {LinearGradient} from 'expo-linear-gradient';
import { marginTop } from 'styled-system';
const {width, height} = Dimensions.get("window")

export default function TunjukanDataRujukan() {
  const navigation = useNavigation()
  return (
    <View colors={['#FFF', '#FFF']}  style={styles.roundRect}>
      <View style={styles.labelsLayout}>
        <Text style={styles.sectionTitle}>Silakan lanjutkan proses tes laboratorium {"\n"} dengan membawa voucher tes laboratorium</Text>
        <TouchableOpacity onPress={() => navigation.navigate("TestLabStatusUnggahHasilTes", { type: 'regular' })}>
          <LinearGradient style={styles.buttonLab} colors={['#830051', '#830051']}>
            <Text style={styles.buttonLoginLabel}>TUNJUKAN DATA PENGAJUAN TES LAB</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  roundRect: {
    flex: 1,
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    height: 180,
    top: 100,
    width: width -30,
    marginTop: 20,
    backgroundColor: "#FFF",
    borderRadius: 15,
    position: 'absolute',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
    // background color must be set
  },
  labelsLayout: {
  },
  sectionTitle: {
    fontFamily: 'avenir_bold',
    fontSize: 18,
    textAlign: 'center',
  },
  boardImage: {
    width: 130,
    height: 130,
  },
  buttonLab: {
    paddingVertical: 15,
    width: "100%",
    borderRadius: 10,
    alignSelf: 'center',
    marginTop: 20,
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
  },
});
