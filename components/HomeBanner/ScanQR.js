import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font'
import { useFonts } from '@use-expo/font';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Svg, { SvgProps, Path } from "react-native-svg"
import { useNavigation } from '@react-navigation/native';
import {LinearGradient} from 'expo-linear-gradient';
import { marginTop } from 'styled-system';
import QRCode from 'react-native-qrcode-svg';
const {width, height} = Dimensions.get("window")

export default function ScanQR() {

  const navigation = useNavigation()

  return (
    <View colors={['#FFF', '#FFF']}  style={styles.roundRect}>
      {/* <Image
        style={styles.boardImage}
        resizeMode="contain"
        source={require('../../assets/qr_2.png')}
      /> */}
      <QRCode
        value="94c1b82b-eba6-41f8-93ec-0a6cc168d00c"
      />
      <View style={styles.labelsLayout}>
        <Text style={styles.sectionTitle}>Perlihatkan QR Code di {"\n"} samping kepada petugas {"\n"} apotek untuk melakukan {"\n"} penebusan obat</Text>
        <TouchableOpacity onPress={() => navigation.navigate("PindaiQR")}>
          <LinearGradient style={styles.buttonLogin} colors={['#830051', '#830051']}>
            <Text style={styles.buttonLoginLabel}>LANJUTKAN</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  roundRect: {
    flex: 1,
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    height: 180,
    top: 100,
    width: width -30,
    marginTop: 20,
    backgroundColor: "#FFF",
    borderRadius: 15,
    position: 'absolute',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
    // background color must be set
  },
  labelsLayout: {
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  sectionTitle: {
    fontFamily: 'avenir_bold',
    fontSize: 16,
    alignSelf: 'flex-start',
  },
  boardImage: {
    width: 100,
    height: 130,
  },
  buttonLogin: {
    padding: 15,
    paddingHorizontal: 45,
    borderRadius: 15,
    alignSelf: 'center',
    marginTop: 10,
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
  },
});
