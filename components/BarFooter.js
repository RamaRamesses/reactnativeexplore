import React, {useState, useEffect} from 'react';
import { Appbar as NativeAppBar } from 'react-native-paper';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity, SafeAreaView } from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';
import { Icon } from 'react-native-elements/dist/icons/Icon';
import { checkTestLabCheckPoint } from '../helpers';
import { useNavigation } from '@react-navigation/native';
import Svg, { Path } from "react-native-svg"
import { alignItems } from 'styled-system';
const {width, height} = Dimensions.get("window")

export default function BarFooter({ activeIcon }) {
  const navigation = useNavigation();
  const onBackPressed = async () => {
    const route = await checkTestLabCheckPoint()
    navigation.navigate(route.page, { fragment: route.fragment })
  }

  useEffect(() => {
    setSelected(activeIcon)
  }, [])

  const [selected, setSelected] = useState("notifikasi")

  return (
    <View>
      <LinearGradient colors={['#ffffff', '#ffffff']} style={styles.container}>
          <View style={styles.row}>
          <TouchableOpacity style={styles.items} onPress={() => navigation.navigate("Notifikasi")}>
            <Svg style={styles.itemImage} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
              <Path
                d="M12 22c1.1 0 2-.9 2-2h-4a2 2 0 002 2zm6-6v-5c0-3.07-1.64-5.64-4.5-6.32V4c0-.83-.67-1.5-1.5-1.5s-1.5.67-1.5 1.5v.68C7.63 5.36 6 7.92 6 11v5l-2 2v1h16v-1l-2-2z"
                fill={activeIcon === "notifikasi" ? "#830051" : "#878787"} 
              />
            </Svg>
            <Text style={activeIcon === "notifikasi" ? styles.itemText : styles.itemTextSecondary}> Notifikasi </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.items} onPress={() => onBackPressed()}>
          <Svg style={styles.itemImage} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
            <Path
              d="M12 5.69l5 4.5V18h-2v-6H9v6H7v-7.81l5-4.5M12 3L2 12h3v8h6v-6h2v6h6v-8h3L12 3z"
              fill={activeIcon === "beranda" ? "#830051" : "#878787"} 

            />
          </Svg>
            <Text style={activeIcon === "beranda" ? styles.itemText : styles.itemTextSecondary}> Beranda </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.items} onPress={() => navigation.navigate("Profile")}>
          <Svg style={styles.itemImage} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
            <Path
              d="M12 5.9a2.1 2.1 0 110 4.2 2.1 2.1 0 010-4.2m0 9c2.97 0 6.1 1.46 6.1 2.1v1.1H5.9V17c0-.64 3.13-2.1 6.1-2.1M12 4C9.79 4 8 5.79 8 8s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm0 9c-2.67 0-8 1.34-8 4v3h16v-3c0-2.66-5.33-4-8-4z"
              fill={activeIcon === "profil" ? "#830051" : "#878787"} 
            />
          </Svg>
            <Text style={activeIcon === "profil" ? styles.itemText : styles.itemTextSecondary}> Profil </Text>
          </TouchableOpacity>
          </View>
      </LinearGradient>
    </View>
  );
}

const styles = StyleSheet.create({
  container : {
    width,
    height: 64,
    bottom: 0,
    flexDirection: 'column',
    alignItems: 'stretch'
  },
  items: {
    flexDirection: 'column',
    justifyContent: 'center',
  },
  itemImage: {
    width: 25,
    height: 25,
    alignSelf: 'center'
  },
  itemText: {
    color: "#830051"
  },
  itemTextSecondary: {
    color: "#878787"
  },
  row: { 
    marginHorizontal: 15,
    flexDirection: 'row',
    marginTop: 10,
    justifyContent: 'space-around',
  },
  sectionText: {
    color: "white",
    alignSelf: 'center',
    fontSize: 20,
    fontFamily: "avenir_next_demibold"
  },
  toolbar: {
    marginLeft: 10,
    color: "white",
  },
  notificationBell: {
    width: 47,
    height: 47
  }
});
