import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font'
import { useFonts } from '@use-expo/font';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Svg, { SvgProps, Path } from "react-native-svg"
import { useNavigation } from '@react-navigation/native';
import {LinearGradient} from 'expo-linear-gradient';
import { marginTop } from 'styled-system';
import DokumenLengkap from './HomeBanner/DokumenLengkap'
import PilihTesLab from './HomeBanner/PilihTesLab';
import UnggahResepDokter from './HomeBanner/UnggahResepDokter';
import TinjauHasilTes from './HomeBanner/TinjauHasilTes';
import TungguVerifikasi from './HomeBanner/TungguVerifikasi';
import AturJadwal from './HomeBanner/AturJadwal';
import TunjukanDataRujukan from './HomeBanner/TunjukanDataRujukan';
import Perpanjang from './HomeBanner/Perpanjang';
import ScanQR from './HomeBanner/ScanQR';
const {width, height} = Dimensions.get("window")

export default function HomeBanner({ fragment }) {

  const conditionalRendering = (data) => {
    switch (data) {
      case "dokumen_lengkap":
        return (<DokumenLengkap />)
      case "tunjukan_data_rujukan":
        return (<TunjukanDataRujukan />)
      case "pilih_tes_pasien":
        return (<PilihTesLab />)
      case "tunggu_verifikasi":
          return (<TungguVerifikasi />)
      case "minum_obat":
          return (<AturJadwal />)
      case "pindai_qr":
          return (<ScanQR />)
      case "perpanjang_pap":
        return (<Perpanjang />)
      case "TinjauHasilTes":
        return (<TinjauHasilTes />)
      case "unggah_resep_dokter":
        return (<UnggahResepDokter />)
      default:
        return (<PilihTesLab />)
    }
  }

  return (
    <>
      {conditionalRendering(fragment)}
    </>
  );
}

const styles = StyleSheet.create({
  roundRect: {
    flex: 1,
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    height: 180,
    top: 100,
    width: width -30,
    marginTop: 20,
    backgroundColor: "#FFF",
    borderRadius: 15,
    position: 'absolute',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
    // background color must be set
  },
  labelsLayout: {
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  sectionTitle: {
    fontFamily: 'avenir_bold',
    fontSize: 16,
    alignSelf: 'flex-start',
  },
  boardImage: {
    width: 130,
    height: 130,
  },
  buttonLogin: {
    padding: 20,
    borderRadius: 15,
    alignSelf: 'flex-start',
    marginTop: 10,
  },
  buttonLoginLabel: {
    fontFamily: 'avenir_next_demibold',
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
  },
});
