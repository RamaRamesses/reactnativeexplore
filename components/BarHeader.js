import React, {useState, useEffect} from 'react';
import { Appbar as NativeAppBar } from 'react-native-paper';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity, SafeAreaView } from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';
import { Icon } from 'react-native-elements/dist/icons/Icon';
import { checkTestLabCheckPoint } from '../helpers';
import { useNavigation } from '@react-navigation/native';
const {width, height} = Dimensions.get("window")

export default function BarHeader({ header, route, fragment }) {
  const navigation = useNavigation();
  const onBackPressed = async () => {
    console.log(route, 'route')
    if (!route) {
      const route = await checkTestLabCheckPoint()
      navigation.navigate(route.page, { fragment: route.fragment })
    } else {
      navigation.navigate(route)
    }
    
  }
  return (
    <View>
      <LinearGradient colors={['#830051', '#1d2671']} style={styles.container}  start={{ x: 1, y: 0 }}>
        <SafeAreaView>
          <View style={styles.row}>
            <TouchableOpacity onPress={() => onBackPressed()}>
              <Icon
                name="arrow-back-outline"
                size={30}
                style={styles.icon}
                type='ionicon'
                color='white'
                />
            </TouchableOpacity>
            <Text style={styles.sectionText}> {header} </Text>
            <Icon
                name="arrow-back-outline"
                size={30}
                style={{ opacity: 0 }}
                type='ionicon'
                color='white'
            />
          </View>
        </SafeAreaView>
      </LinearGradient>
    </View>
  );
}

const styles = StyleSheet.create({
  container : {
    width,
    height: 84,
  },
  row: { 
    marginHorizontal: 15,
    flexDirection: 'row',
    marginTop: 10,
    justifyContent: 'space-between',
  },
  sectionText: {
    color: "white",
    alignSelf: 'center',
    fontSize: 20,
    fontFamily: "avenir_next_demibold"
  },
  toolbar: {
    marginLeft: 10,
    color: "white",
  },
  icon: {
  },
});
