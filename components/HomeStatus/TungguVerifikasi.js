import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font'
import { useFonts } from '@use-expo/font';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';
import {LinearGradient} from 'expo-linear-gradient';
import { alignSelf, fontFamily, justifyContent } from 'styled-system';
import Svg, { SvgProps, Path } from "react-native-svg"
const {width, height} = Dimensions.get("window")

export default function TungguVerifikasi() {
  return (
    <View colors={['#FFF', '#FFF']}  style={styles.container}>
      <View colors={['#FFF', '#FFF']}  style={styles.sectionLayout}>
        <Text style={styles.sectionTitle}> Status {"\n"} Pulih </Text>
      </View>
      <View style={styles.statusTree}>
        <View style={styles.circleChecks}>
          <View style={{flexDirection: 'row', alignSelf: 'center'}}>
          <View style={styles.circleLineNull}></View>
            <Svg style={{alignSelf: 'center'}} width="35" height="35" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
              <Path
                d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"
                fill="#c4d600"
              />
            </Svg>
            <View style={styles.circleLineGreen}></View>
          </View>
          <Text style={styles.treeLabel}>Persetujuan {"\n"} Pasien</Text>
        </View>
        <View style={styles.circleChecks}>
          <View style={{flexDirection: 'row', alignSelf: 'center'}}>
            <View style={styles.circleLineGreen}></View>
            <Svg style={{alignSelf: 'center'}} width="35" height="35" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
              <Path
                d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"
                fill="#c4d600"
              />
            </Svg>
            <View style={styles.circleLineGreen}></View>
          </View>
          <Text style={styles.treeLabel}>Tinjau {"\n"} Hasil</Text>
        </View>
        <View style={styles.circleChecks}>
          <View style={{flexDirection: 'row', alignSelf: 'center'}}>
            <View style={styles.circleLineGreen}></View>
            <Svg style={{alignSelf: 'center'}} width="35" height="35" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
              <Path
                d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"
                fill="#c4d600"
              />
            </Svg>
            <View style={styles.circleLineGreen}></View>
          </View>
          <Text style={styles.treeLabel}>Konfirmasi {"\n"} Dokter</Text>
        </View>
        <View style={styles.circleChecks}>
          <View style={{flexDirection: 'row', alignSelf: 'center'}}>
            <View style={styles.circleLineGreen}></View>
            <Svg style={{alignSelf: 'center'}} width="35" height="35" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
              <Path
                d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"
                fill="#c4d600"
              />
            </Svg>
            <View style={styles.circleLineGreen}></View>
          </View>
          <Text style={styles.treeLabel}>Unggah {"\n"} Resep</Text>
        </View>
        <View style={styles.circleChecks}>
          <View style={{flexDirection: 'row', alignSelf: 'center'}}>
            <View style={styles.circleLineGreen}></View>
            <Svg style={{alignSelf: 'center'}} width="35" height="35" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
              <Path
                d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"
                fill="#DFDFDF"
              />
            </Svg>
            <View style={styles.circleLineNull}></View>
          </View>
          <Text style={styles.treeLabel}>Verifikasi {"\n"}</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    height: 100,
    flexDirection: 'row',
    width: width -30,
    marginTop: 70,
    backgroundColor: "#FFF",
    borderRadius: 15,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
    // background color must be set
  },
  sectionTitle: {
    color: "#FFF",
    alignSelf: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    fontFamily: "avenir_next_demibold",
    fontSize: 18,
  },
  sectionLayout: {
    justifyContent: 'center',
    flex: 1,
    borderRadius: 15,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#3c1053',
  },
  statusTree: {
    flex: 3,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  circleLineGreen: {
    height: 1,
    width: 8,
    alignSelf: 'center',
    backgroundColor: "#c4d600"
  },
  circleLineGray: {
    height: 1,
    width: 8,
    alignSelf: 'center',
    backgroundColor: "#DFDFDF"
  },
  circleLineNull: {
    height: 1,
    width: 10,
    alignSelf: 'center',
    backgroundColor: "#FFF"
  },
  circleChecks: {
    alignSelf: 'center',
    flexDirection: 'column',
  },
  treeLabel: {
    textAlign: 'center',
    alignSelf: 'center',
    fontSize: 10,
    marginTop: 10,
    color: 'gray',
    fontFamily: "avenir_next_demibold"
  }
});
