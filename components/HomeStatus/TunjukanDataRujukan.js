import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font'
import { useFonts } from '@use-expo/font';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';
import {LinearGradient} from 'expo-linear-gradient';
import { alignSelf, fontFamily, justifyContent } from 'styled-system';
import Svg, { SvgProps, Path } from "react-native-svg"
const {width, height} = Dimensions.get("window")

export default function TunjukanDataRujukan() {
  return (
    <View colors={['#FFF', '#FFF']}  style={styles.container}>
      <View colors={['#FFF', '#FFF']}  style={styles.sectionLayout}>
        <Text style={styles.sectionTitle}> Status Tes Lab </Text>
      </View>
      <View style={styles.statusTree}>
        <View style={styles.startTree}>
          <View style={{flexDirection: 'row'}}>
            <View style={styles.circleLineWhite}></View>
            <Svg style={{alignSelf: 'center'}} width="40" height="40" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
              <Path
                d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"
                fill="#c4d600"
              />
            </Svg>
            <View style={styles.circleLineGreen}></View>
          </View>
          <Text style={styles.treeLabel}>Pilih {"\n"} Laboratorium</Text>
        </View>
        <View style={styles.endTree}>
          <View style={{flexDirection: 'row'}}>
            <View style={styles.circleLineGreen}></View>
            <Svg style={{alignSelf: 'center'}} width="40" height="40" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
              <Path
                d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"
                fill="#DFDFDF"
              />
            </Svg>
            <View style={styles.circleLineWhite}></View>
          </View>
          <Text style={styles.treeLabel}>Unggah {"\n"} Hasil Tes</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    height: 100,
    flexDirection: 'row',
    width: width -30,
    marginTop: 70,
    backgroundColor: "#FFF",
    borderRadius: 15,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
    // background color must be set
  },
  sectionTitle: {
    color: "#FFF",
    alignSelf: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_next_demibold",
    fontSize: 18,
  },
  sectionLayout: {
    justifyContent: 'center',
    flex: 1,
    borderRadius: 15,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#3c1053',
  },
  statusTree: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  circleLineGray: {
    height: 1,
    width: 20,
    alignSelf: 'center',
    backgroundColor: "#DFDFDF"
  },
  circleLineGreen: {
    height: 1,
    width: 20,
    alignSelf: 'center',
    backgroundColor: "#c4d600"
  },
  circleLineWhite: {
    height: 1,
    width: 20,
    alignSelf: 'center',
    backgroundColor: "#FFF"
  },
  startTree: {
    alignSelf: 'center',
    flexDirection: 'column',
  },
  endTree: {
    alignSelf: 'center',
    flexDirection: 'column',
  },
  treeLabel: {
    textAlign: 'center',
    fontSize: 12,
    marginTop: 10,
    color: 'gray',
    fontFamily: "avenir_next_demibold"
  }
});
