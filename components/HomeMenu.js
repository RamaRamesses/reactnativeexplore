import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect, useRef} from 'react';
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font'
import { useFonts } from '@use-expo/font';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation, useRoute } from '@react-navigation/native';
import Svg, { SvgProps, Path } from "react-native-svg"
import {LinearGradient} from 'expo-linear-gradient';
import { fontFamily, fontSize, marginTop } from 'styled-system';
import { validate } from '../services/api/patient';
const {width, height} = Dimensions.get("window")

export default function HomeMenu({ fragment }) {
  const navigation = useNavigation();
  const ref = useRef();

  const testLabStatus = async () => {
    const testLabId = await AsyncStorage.getItem('@TES_LAB_ID')
    if (testLabId != null && testLabId != "") {
      navigation.navigate("TestLabStatusSelesai")
    } else {
      navigation.navigate("PilihTesLaboratorium")
    }
  }

  const checkAccountValidation = async (intent) => {
    try {
      const patientId = await AsyncStorage.getItem('@UUID_PATIENT')
      if (patientId != null) {
        const res = await validate(patientId)
        if (res.data) {
          if (res.data.status == "valid") {
            if (intent == "TestLabStatus") {
              testLabStatus()
            } else if (intent == "ProgramStatus") {
              porgramStatus()
            } else if (intent == "ProgramNavigation") {
              programNavigation()
            } else if (intent == "PilihTesLaboratorium") {
              navigation.navigate(intent, { type: 'regular' })
            } else {
              navigation.navigate(intent)
            }
          } else {
            navigation.navigate("EditProfile", { redirect: true })
          }
        }
      }
    } catch (e) {
      console.log(e)
    }
  }

  const porgramStatus = async () => {
    console.log(fragment, "fragment")
    if (fragment == "dokumen_lengkap") {
      try {
        await AsyncStorage.removeItem('@PROGRAM_STATUS')
      } catch (e) {
        console.log(e)
      }
    } else if (fragment == "tunggu_verifikasi") {
      try {
        await AsyncStorage.setItem('@PROGRAM_STATUS', "receipt")
        navigation.navigate("PapStatusSetujuiDokumen", { dokumen: "done", teslab: "done", doctor: "done", receipt: "done", verify: "" })
      } catch (e) {
        console.log(e)
      }
    }  else if (fragment == "pindai_qr") {
      try {
        await AsyncStorage.setItem('@PROGRAM_STATUS', "verify")
        navigation.navigate("PapStatusSetujuiDokumen", { dokumen: "done", teslab: "done", doctor: "done", receipt: "done", verify: "done" })
      } catch (e) {
        console.log(e)
      }
    }
  }

  const programNavigation = async () => {
    console.log(fragment, "fragment")
    if (fragment == "dokumen_lengkap") {
      try {
        await AsyncStorage.removeItem('@PROGRAM_STATUS')
      } catch (e) {
        console.log(e)
      }
    } else if (fragment == "tunggu_verifikasi") {
      try {
        await AsyncStorage.setItem('@PROGRAM_STATUS', "receipt")
      } catch (e) {
        console.log(e)
      }
    }
    navigation.navigate("PilihJenisProgram")
  }

  return (
    <View style={styles.container}>
      <View style={{flexDirection: 'row', justifyContent: 'space-between', marginBottom: 20, marginTop: 28, marginHorizontal: 18}}>
        <Text style={styles.sectionTitle}>Menu Utama</Text>
        <Text style={styles.patientCode}>2021JKT001</Text>
      </View>
      <View style={styles.menuContent}>
        <View style={styles.contentRows}>
          <TouchableOpacity  onPress={() => checkAccountValidation("PilihTesLaboratorium")}>
            <View style={styles.imageWrapper}>
              <Image
                style={styles.menuImage}
                source={require('../assets/menu_tes_lab.png')}
              />
              <Text style={styles.menuTitle}>Tes Laboratorium</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity  onPress={() => checkAccountValidation("ProgramNavigation")}>
            <View style={styles.imageWrapper}>
              <Image
                style={styles.menuImage}
                source={require('../assets/program_pap.png')}
              />
              <Text style={styles.menuTitle}>Program PULIH</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate("Profile")}>
            <View style={styles.imageWrapper}>
              <Image
                style={styles.menuImage}
                source={require('../assets/profil.png')}
              />
              <Text style={styles.menuTitle}>Profil</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.contentRows}>
          <TouchableOpacity onPress={() => { checkAccountValidation("TestLabStatus") }}>
            <View style={styles.imageWrapper}>
                <Image
                  style={styles.menuImage}
                  source={require('../assets/status_lab.png')}
                />
                <Text style={styles.menuTitle}>Status Tes Lab</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => checkAccountValidation("ProgramStatus")}>
            <View style={styles.imageWrapper}>
              <Image
                style={styles.menuImage}
                source={require('../assets/status_pap.png')}
              />
              <Text style={styles.menuTitle}>Status Program PULIH</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate("Bantuan")}>
          <View style={styles.imageWrapper}>
            <Image
              style={styles.menuImage}
              source={require('../assets/bantuan.png')}
            />
            <Text style={styles.menuTitle}>Bantuan</Text>
          </View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width,
  },
  sectionTitle: {
    color: "#2f2f2f",
    fontFamily: "avenir_bold",
    fontSize: 22,
  },
  
  menuContent: {
    flexDirection: 'column'
  },
  contentRows: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginHorizontal: 18,
    marginBottom: 12,
  },
  imageWrapper: {
    width: 135,
    height: 100,
  },
  menuImage: {
    width: null,
    height: null,
    marginHorizontal: 5,
    flex: 1,
    resizeMode: 'contain'
  },
  menuTitle: {
    fontFamily: "avenir_bold",
    color: "#000000",
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: 12,
  },
  patientCode: {
    color: "gray",
    fontSize: 18,
  }
});
