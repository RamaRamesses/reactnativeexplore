import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font'
import { useFonts } from '@use-expo/font';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity, Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Svg, { SvgProps, Path } from "react-native-svg"
import { useIsFocused, useNavigation } from '@react-navigation/native';
import {LinearGradient} from 'expo-linear-gradient';
import { alignSelf, flexDirection, fontFamily, fontSize, justifyContent, marginTop } from 'styled-system';
import { getAll } from '../services/api/article';
import { baseUrl } from '../config';
const {width, height} = Dimensions.get("window")

export default function HomePillsTracker() {

  const [articles, setArticles] = useState([])
  const isFocused = useIsFocused()
  const navigation = useNavigation()

  useEffect(() => {
    loadArticle()
  }, [isFocused])

  const loadArticle = async () => {
    try {
      const token = await AsyncStorage.getItem('@token')
      const res = await getAll(token)
      if (res.data) {
        setArticles(res.data)
      } else {
        console.log(res)
        Alert.alert(res)
      }
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <View style={styles.container}>
      <View style={{flexDirection: 'row', justifyContent: 'space-between', marginBottom: 20, marginTop: 28, marginHorizontal: 18}}>
        <Text style={styles.sectionTitle}>Obat Dikonsumsi</Text>
        <Text onPress={() => navigation.navigate("ArticleMenu")} style={styles.articleLink}>Riwayat Minum Obat {">"}</Text>
      </View>
      <ScrollView horizontal={true}>
        <View style={styles.menuContent}>
          <View style={styles.card}>
            <LinearGradient style={styles.imageWrapper} colors={['#2D948F', '#89A9C8']} start={{ x: 1, y: 0 }}>
              <Image
                style={{
                  width: "50%",
                  height: "50%",
                }}
                resizeMode="contain"
                source={require('../assets/alarm_clock.png')}
              />
              </LinearGradient>
              <View style={styles.pillsWrapper}>
                <Image
                  style={{
                    width: "40%",
                    height: "40%",
                    marginHorizontal: 25,
                  }}
                  resizeMode="contain"
                  source={require('../assets/green_pills.png')}
                />
              </View>
              <View style={styles.textWrapper}>
                <Text style={styles.title}  numberOfLines={2}>Tagrisso</Text>
                <Text style={styles.description} numberOfLines={3} >1 Tablet, 08:00</Text>
              </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width,
  },
  card: {
    flex: 1,
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    height: 100,
    width: width - 30,
    backgroundColor: "#FFF",
    borderRadius: 2,
    marginHorizontal: 15,
    marginBottom: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
    // background color must be set
  },
  imageWrapper: {
    borderRadius: 2,
    flex: 1.5,
    height: "100%",
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "#390752"
  },
  pillsWrapper: {
    flex: 1,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  textWrapper: {
    flex: 2,
    height: "100%",
    flexDirection: 'column',
    alignSelf: 'center',
    justifyContent: 'center'
  },
  sectionTitle: {
    color: "#2f2f2f",
    fontFamily: "avenir_bold",
    fontSize: 22,
  },
  articleLink: {
    color: "#4e125f",
    fontFamily: "avenir_next_demibold",
    fontSize: 14,
  },
  title: {
    fontFamily: "avenir_bold",
    fontSize: 22,
    color: "#000",
    marginBottom: 5,
  },
  description: {
    fontFamily: "avenir_bold",
    fontSize: 18,
    color: "#000",
  },
});
