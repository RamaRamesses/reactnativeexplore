import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font'
import { useFonts } from '@use-expo/font';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity, Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Svg, { SvgProps, Path } from "react-native-svg"
import { useIsFocused, useNavigation } from '@react-navigation/native';
import {LinearGradient} from 'expo-linear-gradient';
import { alignSelf, flexDirection, fontFamily, fontSize, justifyContent, marginTop } from 'styled-system';
import { getAll } from '../services/api/article';
import { baseUrl } from '../config';
const {width, height} = Dimensions.get("window")

export default function HomeArticle() {

  const [articles, setArticles] = useState([])
  const isFocused = useIsFocused()
  const navigation = useNavigation()

  useEffect(() => {
    loadArticle()
  }, [isFocused])

  const loadArticle = async () => {
    try {
      const token = await AsyncStorage.getItem('@token')
      const res = await getAll(token)
      if (res.data) {
        setArticles(res.data)
      } else {
        console.log(res)
        Alert.alert(res)
      }
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <View style={styles.container}>
      <View style={{flexDirection: 'row', justifyContent: 'space-between', marginBottom: 20, marginTop: 28, marginHorizontal: 18}}>
        <Text style={styles.sectionTitle}>Artikel</Text>
        <Text onPress={() => navigation.navigate("ArticleMenu")} style={styles.articleLink}>Lihat semua {">"}</Text>
      </View>
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        <View style={styles.menuContent}>
          {
            articles.map((article, i) => (
              <TouchableOpacity key={i} style={styles.imageWrapper} onPress={() => navigation.navigate("IsiArtikel", { articleId: article.id })}>
                <Image
                  style={styles.menuImage}
                  source={
                    {
                      uri: baseUrl + article.thumbnailImage
                    }
                  }
                />
                <LinearGradient colors={['#3c10534D', '#830051']}  style={styles.articleGradient}>
                <Text style={styles.articleText}>{article.title}</Text>
                </LinearGradient>
            </TouchableOpacity>
            ))
          }
          {/* <View style={styles.imageWrapper}>
              <Image
                style={styles.menuImage}
                source={require('../assets/doctors_unsplash.png')}
              />
              <LinearGradient colors={['#3c10534D', '#830051']}  style={styles.articleGradient}>
              <Text style={styles.articleText}>Menjaga Asupan Nutrisi selama Pandemi COVID-19</Text>
              </LinearGradient>
          </View> */}
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width,
  },
  articleText: {
    color: "#FFF",
    alignSelf: 'flex-end',
    textAlign: 'left',
    fontFamily: "avenir_bold",
    fontSize: 12,
    flex: 1,
    lineHeight: 15,
  },
  sectionTitle: {
    color: "#2f2f2f",
    fontFamily: "avenir_bold",
    fontSize: 22,
  },
  articleLink: {
    color: "#4e125f",
    fontFamily: "avenir_next_demibold",
    fontSize: 14,
  },
  menuContent: {
    flexDirection: 'row',
    marginBottom: 12,
    marginLeft: 10,
    justifyContent: 'space-evenly'
  },
  articleGradient: {
    position: "absolute",
    width: "100%",
    height: "100%",
    flexDirection: 'row',
    paddingLeft: 20,
    paddingBottom: 10,
  },
  imageWrapper: {
    width: 190,
    height: 100,
    marginHorizontal: 5,
  },
  menuImage: {
    width: "100%",
    height: "100%",
  },
  menuTitle: {
    fontFamily: "avenir_bold",
    color: "#000000",
    alignSelf: 'center',
    fontSize: 13,
  },
});
