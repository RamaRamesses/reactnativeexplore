import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font'
import { useFonts } from '@use-expo/font';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';
import {LinearGradient} from 'expo-linear-gradient';
import { alignSelf, fontFamily, justifyContent } from 'styled-system';
import Svg, { SvgProps, Path } from "react-native-svg"
import PilihTesLab from './HomeStatus/PilihTesLab';
import TinjauHasilTes from './HomeStatus/TinjauHasilTes';
import TungguVerifikasi from './HomeStatus/TungguVerifikasi';
import AturJadwal from './HomeStatus/AturJadwal';
import TinjauHasilSudahTes from './HomeStatus/TinjauHasilSudahTes';
import TunjukanDataRujukan from './HomeStatus/TunjukanDataRujukan';
import DokumenLengkap from './HomeStatus/DokumenLengkap';
import UnggahResep from './HomeStatus/UnggahResep';
const {width, height} = Dimensions.get("window")

export default function HomeStatus({ fragment }) {
  const conditionalRendering = (data) => {
    switch (data) {
      case "dokumen_lengkap":
        return (<DokumenLengkap />)
      case "tunjukan_data_rujukan":
        return (<TunjukanDataRujukan />)
      case "pilih_tes_pasien":
        return (<PilihTesLab />)
      case "tunggu_verifikasi":
          return (<TungguVerifikasi />)
      case "minum_obat":
          return (<AturJadwal />)
      case "pindai_qr":
          return (<AturJadwal />)
      case "perpanjang_pap":
        return (<AturJadwal />)
      case "TinjauHasilTes":
        return (<TinjauHasilTes />)
      case "tinjau_hasil_tes_sudah_tes":
        return (<TinjauHasilSudahTes />)
        case "unggah_resep_dokter":
        return (<UnggahResep />)
      default:
        return (<PilihTesLab />)
    }
  }
  return (
    <>
    {conditionalRendering(fragment)}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    height: 100,
    flexDirection: 'row',
    width: width -30,
    marginTop: 70,
    backgroundColor: "#FFF",
    borderRadius: 15,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
    // background color must be set
  },
  sectionTitle: {
    color: "#FFF",
    alignSelf: 'center',
    justifyContent: 'center',
    fontFamily: "avenir_next_demibold",
    fontSize: 18,
  },
  sectionLayout: {
    justifyContent: 'center',
    flex: 1,
    borderRadius: 15,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#3c1053',
  },
  statusTree: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  circleLineGreen: {
    height: 1,
    width: 20,
    alignSelf: 'center',
    backgroundColor: "#C4D600"
  },
  circleLineWhite: {
    height: 1,
    width: 20,
    alignSelf: 'center',
    backgroundColor: "#FFF"
  },
  startTree: {
    alignSelf: 'center',
    flexDirection: 'column',
  },
  endTree: {
    alignSelf: 'center',
    flexDirection: 'column',
  },
  treeLabel: {
    textAlign: 'center',
    fontSize: 12,
    marginTop: 10,
    color: 'gray',
    fontFamily: "avenir_next_demibold"
  }
});
