import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font'
import { useFonts } from '@use-expo/font';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, Button, TouchableOpacity, Alert, FlatList } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Svg, { SvgProps, Path } from "react-native-svg"
import { useIsFocused, useNavigation } from '@react-navigation/native';
import {LinearGradient} from 'expo-linear-gradient';
import { alignSelf, flexDirection, fontFamily, fontSize, justifyContent, marginTop } from 'styled-system';
import { getAll, getAllByProgram } from '../services/api/article';
import { baseUrl } from '../config';
const {width, height} = Dimensions.get("window")

export default function HomeSuggestedArticle() {

  const [articles, setArticles] = useState([])
  const [visibility, setVisibility] = useState("false")
  const isFocused = useIsFocused()
  const navigation = useNavigation()

  useEffect(() => {
    loadArticle()
  }, [isFocused])

  const loadArticle = async () => {
    setVisibility("false")
    try {
      const token = await AsyncStorage.getItem('@token')
      const programType = await AsyncStorage.getItem('@NAMA_PROGRAM_TYPE')
      console.log(programType, "programtype")
      const res = await getAllByProgram(token, programType)
      if (res.data) {
        if (res.data.length > 0) {
          setVisibility("true")
        }
        console.log(visibility)
        var arrays = [], size = 2;
        while (res.data.length > 0) {
          arrays.push(res.data.splice(0, size))
        }
        console.log(arrays)
        setArticles(arrays)
      } else {
        console.log(res)
        Alert.alert(res)
      }
    } catch (e) {
      console.log(e)
    }
  }

  function renderRow(articles) {
    return (
        <View style={styles.menuContent}>
            <TouchableOpacity key={articles[0].id} style={styles.imageWrapper} onPress={() => navigation.navigate("IsiArtikel", { articleId: articles[0].id })}>
                  <Image
                    style={styles.menuImage}
                    source={
                      {
                        uri: baseUrl + articles[0].thumbnailImage
                      }
                    }
                  />
                  <LinearGradient colors={['#3c10534D', '#830051']}  style={styles.articleGradient}>
                  <Text style={styles.articleText}>{articles[0].title}</Text>
                  </LinearGradient>
              </TouchableOpacity>
            {articles.length > 1 ?
              <TouchableOpacity key={articles[1].id} style={styles.imageWrapper} onPress={() => navigation.navigate("IsiArtikel", { articleId: articles[1].id })}>
              <Image
                style={styles.menuImage}
                source={
                  {
                    uri: baseUrl + articles[1].thumbnailImage
                  }
                }
              />
              <LinearGradient colors={['#3c10534D', '#830051']}  style={styles.articleGradient}>
              <Text style={styles.articleText}>{articles[1].title}</Text>
              </LinearGradient>
          </TouchableOpacity> : null
            }
        </View>
    );
  }

  return (
    <View style={styles.container}>
      <View style={{flexDirection: 'row', justifyContent: 'space-between', marginBottom: 20, marginTop: 28, marginHorizontal: 18}}>
        <Text style={styles.sectionTitle}>Artikel Khusus Untukmu</Text>
      </View>
      {
        visibility == "true" ? (
          <FlatList
          contentContainerStyle={{alignSelf: 'flex-start'}}
          horizontal={true}
          style={{ marginLeft: 10 }}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          data={articles}
          renderItem={({ item, index }) => renderRow(item)}
        />
        ) : (
          <>
            <View style={styles.card}>
              <LinearGradient style={styles.emptyImageWrapper} colors={['#94809F', '#94809F']} start={{ x: 1, y: 0 }}>
                <Image
                  style={{
                    width: "70%",
                    height: "70%",
                    opacity: 0.6,
                  }}
                  resizeMode="contain"
                  source={require('../assets/empty_article.png')}
                />
                </LinearGradient>
                <View style={styles.textWrapper}>
                  <View style={styles.headerRoundedRectangle}></View>
                  <View style={styles.roundedRectangle}></View>
                  <View style={styles.roundedRectangle}></View>
                  <View style={styles.roundedRectangle}></View>
                </View>
            </View>
            <Text style={styles.textDescription}>Maaf, belum ada artikel khusus yang {"\n"}tersedia untukmu</Text>
          </>
        )
      }
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width,
  },
  articleText: {
    color: "#FFF",
    alignSelf: 'flex-end',
    textAlign: 'left',
    fontFamily: "avenir_bold",
    fontSize: 12,
    flex: 1,
    lineHeight: 15,
  },
  card: {
    flex: 1,
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    height: 100,
    width: width - 30,
    backgroundColor: "#FFF",
    borderRadius: 12,
    marginHorizontal: 15,
    marginBottom: 20,
    shadowColor: "#000",
    backgroundColor: "#EFEFEF",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
    // background color must be set
  },
  headerRoundedRectangle: {
    width: "85%",
    alignSelf: 'center',
    borderRadius: 5,
    marginBottom: 15,
    height: 13,
    backgroundColor: "#94809F",
  },
  roundedRectangle: {
    width: "85%",
    alignSelf: 'center',
    borderRadius: 10,
    height: 6,
    backgroundColor: "#94809F",
    marginVertical: 3,
  },
  textWrapper: {
    flex: 2,
    height: "100%",
    flexDirection: 'column',
    backgroundColor: "#EFEFEF",
    alignSelf: 'center',
    justifyContent: 'center'
  },
  textDescription: {
    alignSelf: 'center',
    textAlign: 'center',
    fontFamily: 'avenir_bold',
    color: "#511361",
    marginTop: 18,
    fontSize: 16,
    marginBottom: 30,
  },
  emptyImageWrapper: {
    borderRadius: 12,
    flex: 2,
    height: "100%",
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "#390752"
  },
  sectionTitle: {
    color: "#2f2f2f",
    fontFamily: "avenir_bold",
    fontSize: 22,
  },
  articleLink: {
    color: "#4e125f",
    fontFamily: "avenir_next_demibold",
    fontSize: 14,
  },
  menuContent: {
    flexDirection: 'column',
    marginBottom: 15,
    justifyContent: 'flex-start'
  },
  articleGradient: {
    position: "absolute",
    width: "100%",
    height: "100%",
    flexDirection: 'row',
    paddingLeft: 20,
    paddingBottom: 10,
  },
  imageWrapper: {
    width: 190,
    height: 100,
    marginBottom: 10,
    marginHorizontal: 5,
  },
  menuImage: {
    width: "100%",
    height: "100%",
  },
  menuTitle: {
    fontFamily: "avenir_bold",
    color: "#000000",
    alignSelf: 'center',
    fontSize: 13,
  },
});
